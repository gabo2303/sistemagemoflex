dataSource
{
	pooled = true
	driverClassName = "com.mysql.jdbc.Driver"
	//username = "root"
	//password = "gp2014"
	username = "gemo"
	password = "gemo2015"
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
	
//	properties
//	{
//		maxActive = 100
//		minEvictableIdleTimeMillis = 1800000 
//		timeBetweenEvictionRunsMillis = 1800000 
//		numTestsPerEvictionRun = 3 
//		testOnBorrow = true 
//		testWhileIdle = true 
//		testOnReturn = true 
//		validationQuery = "SELECT 1"
//		removeAbandoned = "true"
//		removeAbandonedTimeout = "240"			    
//	}	
}

hibernate 
{
	cache.use_second_level_cache = false
	cache.use_query_cache = false
	
	//cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
	
	cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
	singleSession = true // configure OSIV singleSession mode
	flush.mode = 'manual' // OSIV session flush mode outside of transactional context
}

// environment specific settings
environments 
{
	development 
	{
		dataSource 
		{			
			dbCreate = "update"
			url = "jdbc:mysql://190.107.177.111:3306/gemoflex?autoReconnect = true"
		}
	}
	test 
	{
		dataSource 
		{			
			dbCreate = "update"
			url = "jdbc:mysql://localhost/gemoflex?autoReconnect = true"	
		}
	}
	production 
	{		
		dataSource 
		{			
			dbCreate = "update"
			url = "jdbc:mysql://190.107.177.111:3306/gemoflex?autoReconnect = true"
						
			properties
			{
				jmxEnabled = true
				initialSize = 5
				maxActive = 50
				minIdle = 5
				maxIdle = 25
				maxWait = 10000
				maxAge = 10 * 60000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				validationInterval = 15000
               	testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				jdbcInterceptors = "ConnectionState"
				defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
			}
		}
	}
}