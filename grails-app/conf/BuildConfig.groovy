grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6

//grails.project.war.file = "target/${appName}-${appVersion}.war"
//grails.tomcat.jvmArgs = ["-Xms1024m", "-Xmx2048m", "-XX:PermSize=512m", "-XX:MaxPermSize=1024m"]

grails.project.fork = 
[
	// configure settings for the test-app JVM, uses the daemon by default
    test: [maxMemory: 1024, minMemory: 64, debug: false, maxPerm: 512, daemon:true],
    // configure settings for the run-app JVM
    run: [maxMemory: 1024, minMemory: 64, debug: false, maxPerm: 512, forkReserve:false],
    // configure settings for the run-war JVM
    war: [maxMemory: 1024, minMemory: 64, debug: false, maxPerm: 512, forkReserve:false],
    
	// configure settings for the Console UI JVM
    console: [maxMemory: 1024, minMemory: 64, debug: false, maxPerm: 512]
]

grails.project.dependency.resolver = "maven" 
grails.project.dependency.resolution = 
{
    inherits("global") {  }
    
	log "error" 
    checksums true 
    legacyResolve false 

    repositories 
	{
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
    }

    dependencies 
	{
        runtime 'mysql:mysql-connector-java:5.1.29'
		runtime 'net.sourceforge.jexcelapi:jxl:2.6.12'
		
        test "org.grails:grails-datastore-test-support:1.0-grails-2.4"		
    }

    plugins 
	{
		build ":tomcat:7.0.55"		

        // plugins for the compile step				
        compile ":scaffolding:2.1.2"
        compile ':cache:1.1.7'
        compile ":asset-pipeline:1.9.6"
		compile ":spring-security-core:2.0-RC4"
		compile ":famfamfam:1.0.1"
		compile ":mail:1.0.7"
		compile ":jasper:1.11.0"
		compile ":tooltip:0.8"
		compile "org.grails.plugins:google-visualization:1.0.2"		
		compile ':excel-import:1.1.0.BUILD-SNAPSHOT'
		compile "org.grails.plugins:jxl:0.54"
		compile "org.grails.plugins:excel-export:0.1.9"

        // plugins needed at runtime but not for compilation
        runtime ":hibernate4:4.3.5.5" // or ":hibernate:3.6.10.17"
        runtime ":database-migration:1.4.0"
        runtime ":jquery:1.11.1"
		runtime ':jasper:latest'
    }
}