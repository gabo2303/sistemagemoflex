import Maestros.*
import seguridad.*

class BootStrap 
{
    def init = 
	{ 
		servletContext ->
		
		//Creacion super Usuario
		def superAdministrador = Permiso.findByAuthority('ROLE_SUPERADMINISTRADOR') ?: new Permiso(authority:'ROLE_SUPERADMINISTRADOR').save(failOnError:true)
		
		def usuarioSuperAdministrador = Usuario.findByUsername('gabriel.gutierrez')?: new Usuario(username:'gabriel.gutierrez', password:'1234', nombre:'Gabriel', apellido:'Gutierrez', cliente: 1, estado:true).save(failOnError:true)
		
		if(!usuarioSuperAdministrador.authorities.contains(superAdministrador)) { UsuarioPermiso.create usuarioSuperAdministrador,superAdministrador }	
		
		def roles = UsuarioPermiso.findAllByUsuario(usuarioSuperAdministrador)
    }
    
	def destroy = {  }
}