//Función que pinta el número de hoja en la que uno esta posicionado identificando donde estamos

function identificadorHoja()
{		
	var p = document.getElementById('hoja').value;
	var label = document.getElementsByClassName('label'), i;
	var etiqueta = document.getElementById('etiqueta');		
					
	for (var i = 0; i < label.length; i++) 
	{
		if(p == i)
		{				
			label[i].style.color = 'white';			    
			label[i].style.backgroundColor = '#383836';
			label[i].style.padding = '5px 5px 5px 5px';
			label[i].style.marginTop = '-100px';
		}
		else
		{
			label[i].style.color = 'black';
		}				
	}
}

//Función que convierte los datos pertenecientes a los correos de un array a un String y los muestra en pantalla

function arrayToString(correo, correoC, correoF)
{	
	var email = document.getElementById(correo)
	var emailC = document.getElementById(correoC)
	var correoFin = document.getElementById(correoF)
	
	var a = email.value.substring(1) //quita el primer caracter de la cadena
	var tama = a.length;//recupera el tamaño de la cadena "a"
	var cFinal = a.substring(0, tama - 1 )//quita el ultimo caracter de la cadena "a"
	email.setAttribute('value',cFinal );//Asigna la cadena el input correo

	var aC = emailC.value.substring(1)
	var tamaC = aC.length;
	var cFinalC = aC.substring(0, tamaC - 1 )
	emailC.setAttribute('value',cFinalC);
	
	var correos = cFinalC+','+cFinal
	correoFin.setAttribute('value',correos)	
}

//Función que muestra la imagen de un ticket si es que se ha enviado el correo

function confir(elemento, v)
{	
	var verifica = document.getElementById(v).value;
	
	if(verifica == 1)
	{
		document.getElementById(elemento).style.display = 'block';		
	} 
}

//Función que muestra el combo de selección para cambiar estado

function mostrar(va)
{
	document.getElementById(va).style.display = 'block';	
	document.getElementById("se").style.display = 'block';	
}

//Función que muestra el botón exportar siempre que el estado de la inspeccion se 'APROBADO'

function ocultar(va,est,exportar,oculta,td2,elemento)
{
	document.getElementById(va).style.display = 'none';
	
	var x = document.getElementById(est);	
	
	if(x.value == "APROBADO")
	{
		document.getElementById(exportar).style.display = 'table-cell';	
		document.getElementById(exportar).style.position = 'relative';	
		document.getElementById(exportar).style.top = '-26px';
		x.style.backgroundColor = 'green';
		x.style.color = 'white';				
		document.getElementById(oculta).style.display = 'none';
	}
	else if(x.value == "RECHAZADO")
	{
		document.getElementById(exportar).style.display = 'none';		
		document.getElementById(td2).style.display = 'none';
		x.style.backgroundColor = 'red';
		x.style.color = 'white';
		document.getElementById(oculta).style.display = 'block';
		document.getElementById(elemento).style.display = 'none';		    
	}
	else if(x.value == "NULO")
	{		
		document.getElementById(exportar).style.display = 'none';
		document.getElementById(td2).style.display = 'none';		
		x.style.backgroundColor = 'black';
		x.style.color = 'white';
		document.getElementById(oculta).style.display = 'block';
		document.getElementById(elemento).style.display = 'none';
	}
	else
	{
		document.getElementById(td2).style.display = 'none';
		document.getElementById(exportar).style.display = 'none';		
		x.style.backgroundColor = 'white';
		x.style.color = 'black';
		document.getElementById(oculta).style.display = 'block';
		document.getElementById(elemento).style.display = 'none';
	}
}

//Función que impide que un inspector pueda modificar el estado de una inspección

function ocultarLabel(mostrar, guardar, exportar, td1, td2, oculta)
{
	var j = document.getElementById("role").value;	
	
	if(j == "[ROLE_SUPERADMINISTRADOR]" || j == "[ROLE_ADMINISTRADOR]" || j == "[ROLE_SUPERVISOR]")
	{
		document.getElementById(mostrar).style.display = 'block';
		document.getElementById(guardar).style.display = 'block';			
		document.getElementById(td1).style.display = 'none';
		document.getElementById(td2).style.display = 'none';
	}
	else
	{			
		document.getElementById(mostrar).style.display = 'none';
		document.getElementById(guardar).style.display = 'none';
		document.getElementById(exportar).style.display = 'none';
		document.getElementById(oculta).style.display = 'none';								
	}	
}

//Función que oculta la cabecera 'SELECCIONAR ESTADO'

function ocultarCabecera()
{
	document.getElementById("se").style.display = 'none';	
}

//Función que pasa el valor del estado seleccionado

function select(estado,cambio)
{
	var elvalor = document.getElementById(estado).value;	
	var x = document.getElementById(cambio);
	x.setAttribute("value",elvalor);	
}

function abrirPestana()
{
	alert("TEST");
}