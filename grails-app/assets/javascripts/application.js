// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}

(function($) {

	  $.fn.menumaker = function(options) {
	      
	      var cssmenu = $(this), settings = $.extend({
	        title: "Menu",
	        format: "dropdown",
	        sticky: false
	      }, options);

	      return this.each(function() {
	        cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
	        $(this).find("#menu-button").on('click', function(){
	          $(this).toggleClass('menu-opened');
	          var mainmenu = $(this).next('ul');
	          if (mainmenu.hasClass('open')) { 
	            mainmenu.hide().removeClass('open');
	          }
	          else {
	            mainmenu.show().addClass('open');
	            if (settings.format === "dropdown") {
	              mainmenu.find('ul').show();
	            }
	          }
	        });

	        cssmenu.find('li ul').parent().addClass('has-sub');

	        multiTg = function() {
	          cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
	          cssmenu.find('.submenu-button').on('click', function() {
	            $(this).toggleClass('submenu-opened');
	            if ($(this).siblings('ul').hasClass('open')) {
	              $(this).siblings('ul').removeClass('open').hide();
	            }
	            else {
	              $(this).siblings('ul').addClass('open').show();
	            }
	          });
	        };

	        if (settings.format === 'multitoggle') multiTg();
	        else cssmenu.addClass('dropdown');

	        if (settings.sticky === true) cssmenu.css('position', 'fixed');

	        resizeFix = function() {
	          if ($( window ).width() > 768) {
	            cssmenu.find('ul').show();
	          }

	          if ($(window).width() <= 768) {
	            cssmenu.find('ul').hide().removeClass('open');
	          }
	        };
	        resizeFix();
	        return $(window).on('resize', resizeFix);

	      });
	  };
	})(jQuery);

	(function($){
	$(document).ready(function(){

	$("#cssmenu").menumaker({
	   title: "Menu",
	   format: "multitoggle"
	});


	});
	})(jQuery);
	
	
