<%@ page import = "Maestros.MaestroClasificacion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Editando Clasificación </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Clasificaciones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Clasificación </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editando Clasificación </h1>
			
			<g:form url = "[resource:maestroClasificacionInstance, action:'update']" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${maestroClasificacionInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>