<%@ page import = "Maestros.MaestroClasificacion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Clasificaciones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Clasificación </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1 align = "center"> Listado de Clasificaciones </h1> <br/>
			
			<table style = "width:40%; margin-left:30%">
				
				<thead> <tr> <th> Id </th> <th> Descripción </th> </tr> </thead>
				
				<tbody>
					
					<g:each in = "${clasificaciones}" status = "i" var = "clasificacion">
					
						<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
							<td> <g:link action = "show" id = "${clasificacion.id}"> ${clasificacion.id} </g:link> </td> <td> ${clasificacion.descripcion} </td> 
							
						</tr>
					
					</g:each>
				
				</tbody>
				
			</table>
			
			<div class = "paginations" style = "width:39.5%; margin-left:30%"> <g:paginate total = "${clasificaciones.totalCount ?: 0}" /> </div>
			
		</div>
		
	</body>
	
</html>