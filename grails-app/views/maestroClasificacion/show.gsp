<%@ page import="Maestros.MaestroClasificacion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Mostrando Clasificación </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Clasificaciones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Clasificación </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-maestroClasificacion" class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Clasificación</h1>
			
			<ol class = "property-list maestroClasificacion">
						
				<li class = "fieldcontain"> <span class = "property-label"> Descripción </span> <span class = "property-value"> ${clasificacion.descripcion} </span> </li>
				
			</ol>
			
			<g:form resource = "${clasificacion}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${clasificacion}"> Editar </g:link>
					
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>