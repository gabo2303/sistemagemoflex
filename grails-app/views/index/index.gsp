<!DOCTYPE html>
<!-- Muestra la pagina principal con el menú las imagenes, etc... -->
<html>

	<head>
	
		<meta name = "layout" content = "main"/>
		
		<title> Sistema de Inspecciones Gemoflex </title>
		
		<gvisualization:apiImport/>
		
		<style type = "text/css" media = "screen">
			
			#page-body { margin-left: 2%; font-size: 13px; }

			h2 { margin-top: 1em; margin-bottom: 0.3em; font-size: 1em; }

			p { line-height: 1.5; margin: 0.25em 0; font-size:14px; }

			#controller-list ul { list-style-position: inside; }
			
			#datos { font-weight:lighter; font-size:18px;  }
			
			#controller-list li { line-height: 1.3; list-style-position: inside; margin: 0.25em 0; }

			@media screen and (max-width: 480px) 
			{				
				#page-body 
				{
					background: white;
					margin: 0 1em 1em;
					font-size: 0.5em;
					margin-bottom: -2px;
					width: 100%;
					margin-left: -1px;
				}

				#page-body h1 
				{
					font-size: 15px;
					font-weight: 500; 
					color: #737b8a;
					text-align: center;
					margin-top: 0;
				}
			}
			
		</style>
		
	</head>
	
	<body>	
	
		<g:if test = "${cliente?.razonSocial == 'Copec'}"> 
			
			<sec:ifAnyGranted roles = "ROLE_ADMINISTRADOR, ROLE_SUPERADMINISTRADOR, ROLE_SUPERVISOR, ROLE_CLIENTE">
			
				<div id = "page-body" role = "main">		
					
					<br/> <h1 style = "font-size: 22px; text-align: center"> Bienvenido al Sistema de Inspecciones Gemoflex </h1> <br/>
									
					<g:form action = "index">
					
						<h1 style = "font-size: 19px; text-align: center"> 
											
							Inspecciones Mensuales para la Planta: ${params.plantaSeleccionada ?: 'Todas'}, Transportista: ${params.transportistaSeleccionado ?: 'Todos'}, 
							Período: <g:datePicker name = "periodo" precision = "month" value = "${params.periodo}"/> <input style = "margin-left: 1%" type = "submit" value = "Seleccionar" class = "btn btn-gemo">
							
						</h1> <br/>
								
					</g:form>
							
					<div class = "container">
					
						<div class = "row">
							
							<div class = "col-sm-1"> </div>
							
							<div class = "col-sm-3"> <label> Inspecciones Destacadas </label> </div>
							
							<div class = "col-sm-4"> </div>
							
							<div class = "col-sm-4"> <label> Otras Inspecciones </label> </div>
							
						</div>
						
					</div>
					
					<br/>
					 
					<div class = "container">
					
						<div class = "row">
							
							<div class = "col-sm-4 buttons">
								
								<div class = "container">
									
									<br/> <div class = "row">
										
										<div class = "col-sm-2"> </div>
										<div class = "col-sm-8"> <label> Total: ${cantidades[0] + cantidades[6]} </label> </div>
										<div class = "col-sm-2"> </div>
										
									</div> <br/>
									
									<div class = "row">
											
										<div class = "col-sm-2"> </div>
										<div class = "col-sm-8"> <label style = "color:#BD2020"> Liviano / P.Combustible: ${cantidades[0]} </label> </div>
										<div class = "col-sm-2"> </div>
											
									</div> <br/>
								
									<div class = "row">
										
										<div class = "col-sm-2"> </div>																				
										<div class = "col-sm-8"> <label style = "color:#A80D7C"> Vehículo en Taller: ${cantidades[6]} </label> </div>
										<div class = "col-sm-2"> </div>																			
																			
									</div> <br/>
									
								</div>
								
							</div>
							
							<div class = "col-sm-1"> </div>
							
							<div class = "col-sm-7 buttons">
							
								<div class = "container">
									
									<br/> <div class = "row">
										
										<div class = "col-sm-2"> </div>
										<div class = "col-sm-5"> <label style = "color:#59A0F7"> Gráfica: ${cantidades[1]} </label> </div>
										<div class = "col-sm-5"> <label style = "color:#C5881F"> Lubricantes Quintero: ${cantidades[2]} </label> </div>
										
									</div> <br/>
									
									<div class = "row">
											
										<div class = "col-sm-2"> </div>
										<div class = "col-sm-5"> <label style = "color:#1C9424"> Red Tae: ${cantidades[3]} </label> </div>
										<div class = "col-sm-5"> <label style = "color:#5d5d5d"> Gráfica Lubricantes Quintero: ${cantidades[4]} </label> </div>
											
									</div> <br/>
								
									<div class = "row">
										
										<div class = "col-sm-2"> </div>																				
										<div class = "col-sm-8"> <label style = "color:#55179C"> CD Lubricantes: ${cantidades[5]} </label> </div>
										<div class = "col-sm-2"> </div>																			
																			
									</div> <br/>
									
								</div>
								 	
							</div>
						
						</div>
						
					</div> <br/>
					
					<gvisualization:columnChart elementId = "grafico" 
					columns = "${[['string', 'Mes'], ['number','Gráfica'], ['number','Liviano-Petróleo'], ['number','Lubricantes Quintero'], 
					['number','Red Tae'], ['number','Grafica Lubricantes Quintero'], ['number','CD Lubricantes'], ['number', 'Vehículo en Taller']]}"
					height = "${300}" data = "${data}" is3D = "${true}" dynamicLoading = "${true}"/>
			
					<div id = "grafico" style = "width: 98%"> </div>
					
					<g:form action = "index">
									
						<table style = "width:88%; margin-left:5%; margin-top:2%;" class = "buttons">
							
							<tbody>
								
								<tr class = "noclass">
									
									<td> <p style = "text-align: center"> Seleccionar Planta:  <g:select name = "plantaSeleccionada" 
									value = "${params.plantaSeleccionada}" from = "${plantas}" noSelection = "['':'Todas Las Plantas']"/> </p> </td> 
									
									<td> <p style = "text-align: center"> Seleccionar Transportista: <g:select name = "transportistaSeleccionado" 
									value = "${params.transportistaSeleccionado}" from = "${transportistas}" noSelection = "['':'Todos Los Transportistas']"/> </p> </td>
									
								</tr>
								
							</tbody>
							
						</table>
						
						<fieldset class = "buttons" style = "margin-top: 20px; width: 8%; margin-left:45%"> <g:submitButton class = "edit" name = "Consultar"/> </fieldset>
						
					</g:form>
					
					<div style = "text-align: center; margin-top:4%">
								
						<h1> Contacto </h1>
									
						<p style = "font-size: 18px"> Correo: inspecciones@gemoflex.cl <br/> Teléfono: +56 2 25563120 </p>
							
					</div>
							
				</div>
				
			</sec:ifAnyGranted>
			
			<sec:ifAnyGranted roles = "ROLE_TRANSPORTISTA">
			
				<div id = "page-body" role = "main">		
					
					<br/> <h1 style = "font-size: 20px; text-align: center"> Bienvenido al Sistema de Inspecciones Gemoflex </h1> <br/>
					
					<gvisualization:columnChart elementId = "grafico" columns = "${columnas}" height = "${300}" data = "${data}" is3D = "${true}" dynamicLoading = "${true}"/>
					
					<div id = "grafico" style = "width: 98%"> </div>
					
					<g:if test = "${plantas?.size >= 1 || transportistas?.size >= 1}">
						
						<g:form action = "index">
										
							<table style = "width:88%; margin-left:5%; margin-top:2%;" class = "buttons">
								
								<tbody>
									
									<tr class = "noclass">
										
										<g:if test = "${plantas?.size > 1}">
										
											<td> <p style = "text-align: center"> Seleccionar Planta:  <g:select name = "plantaSeleccionada" 
											value = "${params.plantaSeleccionada}" from = "${plantas}" noSelection = "['':'Todas Las Plantas']"/> </p> </td>
											 
										</g:if>
										
										<g:if test = "${transportistas?.size > 1}">
										
											<td> <p style = "text-align: center"> Seleccionar Transportista: <g:select name = "transportistaSeleccionado" 
											value = "${params.transportistaSeleccionado}" from = "${transportistas}" noSelection = "['':'Todos Los Transportistas']"/> </p> </td>
											
										</g:if>
										
									</tr>
									
								</tbody>
								
							</table>
							
							<fieldset class = "buttons" style = "margin-top: 20px; width: 8%; margin-left:45%;"> <g:submitButton class = "edit" name = "Consultar"/> </fieldset>
							
						</g:form>
						
					</g:if>
					
					<div style = "text-align: center; margin-top:4%">
								
						<h1> Contacto </h1> <p style = "font-size: 18px"> Correo: inspecciones@gemoflex.cl <br/> Teléfono: +56 2 25563120 </p>
							
					</div>
							
				</div>
			
			</sec:ifAnyGranted>		
		
			<sec:ifAnyGranted roles = "ROLE_ANONYMOUS, ROLE_INSPECTOR">
			
				<div id = "page-body" role = "main">		
				
					<br/> <h1> Bienvenido al Sistema de Inspecciones Gemoflex </h1> <p id = "p"> Para comenzar a utilizar la aplicación es preciso utilizar el menú. </p>	
					
					<br/> <h1> Contacto </h1> <p id = "p"> Correo: inspecciones@gemoflex.cl <br/> Teléfono: +56 2 25563120 </p>	
								
				</div>
					
			</sec:ifAnyGranted>
		
		</g:if>
		
		<g:else>

			<div id = "page-body" role = "main">		
				
				<br/> <h1> Bienvenido al Sistema de Inspecciones Gemoflex </h1> <p id = "p"> Para comenzar a utilizar la aplicación es preciso utilizar el menú. </p>	
				
				<br/> <h1> Contacto </h1> <p id = "p"> Correo: inspecciones@gemoflex.cl <br/> Teléfono: +56 2 25563120 </p>	
							
			</div>
		
		</g:else>
			
	</body>
	
</html>