<%@ page import = "Maestros.CorreoCliente" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Asociaciones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Asociación </g:link> </li> </ul> </div>
		
		<div id = "list-correoCliente" class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1> Listado de Asociaciones </h1> <br/>
			
			<g:form action = "index">
			
				<table style = "margin-left: 2.5%">
					
					<thead> <tr> <th> Id </th> <th> Cliente </th> <th> Correo </th> <th> Planta </th> <th> Tipo de Inspección </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass"> 
							
							<td> </td> 
							<td> <g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.cliente}" /> </td>
							<td> <g:select name = "correo" from = "${Maestros.MaestroCorreo.list().sort { it.correo }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.correo?.toInteger()}" /> </td>
							<td> <g:select name = "planta" from = "${Maestros.MaestroPlanta.list().sort { it.descripcion }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.planta?.toInteger()}" /> </td>
							<td> <g:select name = "tipoInspeccion" from = "${Maestros.MaestroTipoInspeccion.findAllByActivo(true).sort { it.descripcion }}" optionValue = "descripcion" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.tipoInspeccion}" /> </td>
							
						</tr>
						
						<g:each in = "${correosClientes}" status = "i" var = "correoCliente">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> <g:link action = "show" id = "${correoCliente.id}"> ${correoCliente.id} </g:link> </td>
								
								<td> ${correoCliente.cliente} </td> <td> ${correoCliente.email} </td> <td> ${correoCliente.planta} </td>
							
								<td> ${correoCliente.tipoInspeccion.descripcion} </td>
							
							</tr>
							
						</g:each>
					
					</tbody>
				
				</table>
				
				<div class = "paginations"> <g:submitButton name = "Buscar" class = "botonBuscador"/> <g:paginate total = "${correosClientes.totalCount ?: 0}" params = "${params}" /> </div>
			
			</g:form>
			
		</div>
		
	</body>
	
</html>