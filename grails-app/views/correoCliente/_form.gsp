<%@ page import = "Maestros.CorreoCliente" %>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label>
	
	<g:select name = "cliente.id" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" required = "" value = "${correoClienteInstance?.cliente?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "email"> Email <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "correo" name = "email.id" from = "${Maestros.MaestroCorreo.list(sort:'correo', order:'asc')}" optionKey = "id" required = "" value = "${correoClienteInstance?.email?.id}" />

</div>

<div class = "fieldcontain">

	<label for = "planta"> Planta <span class = "required-indicator">*</span> </label>
	
	<g:select name = "planta.id" from = "${Maestros.MaestroPlanta.list(sort:'descripcion',order:'asc')}" optionKey = "id" required = "" value = "${correoClienteInstance?.planta?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "tipoInspeccion"> Tipo de Inspección <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" name = "tipoInspeccion.id" from = "${Maestros.MaestroTipoInspeccion.findAllByActivo(true).sort { it.descripcion }}" optionKey = "id" required = "" value = "${correoClienteInstance?.tipoInspeccion?.id}" />

</div>