<%@ page import = "Maestros.CorreoCliente" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Correo Cliente </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class = "list" action = "index"> Listado Correo Cliente </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Correo Cliente </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Correo-Cliente </h1>
			
			<ol class = "property-list correoCliente">
			
				<li class = "fieldcontain">
					
					<span class = "property-label"> Cliente </span> <span class = "property-value"> ${correoClienteInstance?.cliente} </span>
					
				</li>
			
				<li class = "fieldcontain">
				
					<span class = "property-label"> Email </span> <span class = "property-value"> ${correoClienteInstance?.email?.correo} </span>
					
				</li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Planta </span> <span class = "property-value"> ${correoClienteInstance?.planta} </span> </li>
				
				<li class = "fieldcontain">
					
					<span class = "property-label"> Tipo de Inspección </span> <span class = "property-value"> ${correoClienteInstance?.tipoInspeccion?.descripcion} </span>
					
				</li>
			
			</ol>
			
			<g:form resource = "${correoClienteInstance}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${correoClienteInstance}"> Editar </g:link>
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('¿Esta usted seguro?');" />
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>