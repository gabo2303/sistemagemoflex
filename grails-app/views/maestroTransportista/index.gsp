<%@ page import = "Maestros.MaestroTransportista" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Listado de Transportistas </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation"> 
		
			<ul> 
				<li> <g:link class = "create" action = "create"> Crear Transportista </g:link> </li> 
				<li> <g:link class = "excel" action = "descargarExcel"> Descargar Transportistas sin Rut </g:link> </li>
			</ul> 
		
		</div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1> Listado de Transportistas </h1> <br/>
			
			<g:form action = "index">
				
				<table style = "margin-left: 2.5%">
			
					<thead> <tr> <th> Razón Social </th> <th> Rut </th> <th> Cliente </th> <th> Descripción </th> <th> Activo </th> </tr> </thead>
						
					<tbody>
						
						<tr class = "noclass">
							
							<td> <input size = 17 name = "razonSocial" value = "${params.razonSocial}"> </td>
							<td> <input size = 17 name = "rut" value = "${params.rut}"> </td>																		
							<td> <g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" 	noSelection = "['':'Seleccione un Cliente']" optionKey = "id" value = "${params.cliente}"/> </td>
							<td> <input size = 17 name = "descripcion" value = "${params.descripcion}"> </td>							
							<td> <g:select name = "activo" from = "${['Verdadero', 'Falso']}" noSelection = "['':'Seleccione una Opción']" value = "${params.activo}"/> </td>	
																							
						</tr>
						
						<g:each in = "${transportistas}" status = "i" var = "transportista">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> <g:link action = "show" id = "${transportista.id}"> ${transportista.razonSocial} </g:link> </td>
								
								<td> ${transportista.rut} </td> <td> ${transportista.cliente} </td> <td> ${transportista.descripcion} </td>
							
								<td> <g:formatBoolean boolean = "${transportista.activo}" true = "Si" false = "No" /> </td>				
						
							</tr>
							
						</g:each>
						
					</tbody>
						
				</table>
				
				<div class = "paginations"> 
				
					<tooltip:tip value = "Presione para filtrar los datos."> <g:submitButton name = "Buscar" class = "botonBuscador"/> </tooltip:tip> 
					<g:paginate total = "${transportistas.totalCount ?: 0}"/> </div>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>