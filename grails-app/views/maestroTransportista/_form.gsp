<%@ page import = "Maestros.MaestroTransportista" %>

<div class = "fieldcontain">
	
	<label for = "rut"> Rut <span class = "required-indicator">*</span> </label> 
	
	<g:textField  name = "rut" required = "" placeholder = "Formato: 11111111-1" value = "${maestroTransportistaInstance?.rut}" pattern = "[0-9Kk-]{8,10}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "razonSocial"> Razón Social <span class = "required-indicator">*</span> </label> <g:textField name = "razonSocial" required = "" value = "${maestroTransportistaInstance?.razonSocial}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label>
	
	<g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" required = "" value = "${maestroTransportistaInstance?.cliente?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "descripcion"> Descripción <span class = "required-indicator">*</span> </label> <g:textField name = "descripcion" required = "" value = "${maestroTransportistaInstance?.descripcion}"/>

</div>