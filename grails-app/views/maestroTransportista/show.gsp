<%@ page import = "Maestros.MaestroTransportista" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Mostrando Transportista: ${transportista.descripcion} </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class = "list" action = "index"> Listado de Transportistas </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Transportista </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-maestroTransportista" class = "content scaffold-show" role = "main">
			
			<br/> 
			
			<g:if test = "${flash.message}">
				
				<div class = "message" role = "status"> ${flash.message} </div>
				
			</g:if>
			
			<h1> Mostrando Transportista: ${transportista.descripcion} </h1>
						
			<ol class = "property-list maestroTransportista">
			
				<li class = "fieldcontain"> <span class = "property-label"> Rut </span> <span class = "property-value"> ${transportista.rut} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Razón Social </span> <span class = "property-value"> ${transportista.razonSocial} </span> </li>
					
				<li class = "fieldcontain"> <span class = "property-label"> Cliente </span> <span class = "property-value"> ${transportista?.cliente} </span> </li>
		
				<li class = "fieldcontain"> <span class = "property-label"> Descripción </span> <span class = "property-value"> ${transportista.descripcion} </span> </li>			
				
				<li class = "fieldcontain"> 
				
					<span class = "property-label"> Activo </span> <span class = "property-value"> <g:formatBoolean boolean = "${transportista.activo}" false = "No" true = "Si" /> </span>					
					
				</li>
							
			</ol>
			
			<g:form resource = "${transportista}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons"> 
					
					<g:link class = "edit" action = "edit" resource = "${transportista}"> Editar </g:link> 
				
					<g:if test = "${transportista.activo}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar el Transportista?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<div hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </div>
					
					</g:if>
					
					<g:else>
						
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
						
						<script>
							
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar el Transportista?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
						
						</script>
						
						<div hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </div>
					
					</g:else>
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>