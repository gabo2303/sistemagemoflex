<!-- Template que se muestra al seleccionar el tipo de inspección y muestra los campos que permiten el filtrado -->

<g:formRemote name = "frmConsultaCliente" url = "[controller:'transportistaConsulta', action:'listadoPorItems']" update = "divCC">
		
	<input type = "hidden" value = "${tInspeccion}" name = "nInspeccion"> 
	<input type = "hidden" value = "${plantasTotales}" name = "plantas"> 
	<input type = "hidden" value = "${transportistas}" name = "transportistas">
	
	<table class = "table-form">
		
		<thead>
			
			<tr>
				<td class = "trhead" colspan = "2">Seleccionador de Item</td>
			</tr>
			
		</thead>
		
		<tbody>
		
			<tr class = "noclass">  	
			 
  	  			<td style = "width: 100px">Número Tanque: </td>
				
				<td> 
					<g:select id = "ntanque" name = "tanque" from = "${numero}" noSelection = "['':'Todos los Tanques']"
					class = "many-to-one"/>                      			  	                       
				</td>	
												 	  		 
  	  		</tr>
  	  		
			<tr class = "noclass">
			  	  
  	  			<td>Items: </td>
				
				<td> 
					<g:select id = "items" name = "items" from = "${items}" optionKey = "" optionValue = "" noSelection = "['':'Todos los Items']"
					class = "many-to-one"/>                      			  	                       
				</td> 		 
												
			</tr>
			
			<tr class = "noclass">
			
		    	<td>Planta/Localidad:</td>
		        
		        <td> <g:select name = "planta" from = "${plantas}" noSelection = "['':'Todas las plantas']" class = "many-to-one"/> </td>
		                 				           
  	  		</tr>
  	  
  	  		<tr class = "noclass">
  	  		
  	  			<td>Fecha Inicio:</td>
  	  			
  	  			<td> <g:datePicker name = "fechaInicial" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>
  	  									 			 	  		
  	  		</tr>
  	  		
  	  		<tr class = "noclass">
  	  		
  	  			<td>Fecha Fin:</td>
  	  			
  	  			<td> <g:datePicker name = "fechaFinal" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>
  	  										 			  	  		
  	  		</tr>
  	  		  	  		
  	  		<tr class = "noclass">
  	  		
  	  			<td> Buscar </td>
  	  			
  	  			<td> <input type = "image" src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" class = firstbutton name = "btnBuscar" 
  	  			value = "Buscar"> </td>
  	  	
			</tr>
  		
  		</tbody>
  	
  	</table>	  

</g:formRemote>  	

<div id = "divCC"> </div>