<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		<meta name = "layout" content = "main"/>
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/>
		<title> Consulta Última Inspección </title>
		
	</head>

	<body>
  
  		<div class = "body">
  			
  			<br/><br/>	  
  			
  			<input hidden = "" id = "transportista" value = "${transportistas}"/>
  			
	  		<table class = "table-form">
	  				
	  			<thead> <tr> <td  class = "trhead" colspan = 2> Consulta por Última Inspección para el(los) Transportista(s) ${transportistas} </td> </tr> </thead>
	  	  			  	  			
	  	  		<tbody>
				 
					<tr class = "noclass">
					  	  	  			 
						<td width = "180px"> Tipo de Inspección: </td>
							
						<td> 
							<g:select id = "tipoInspeccionId" name = "numeroInspeccion" from = "${tipoInspeccion}" optionKey = "id" required = "" optionValue = "descripcion" 
							noSelection = "['':'Seleccione una Inspección']" onchange = "${remoteFunction(controller:'transportistaConsulta', action:'resultadoUltimaInspeccion',
							params:'\'numeroInspeccion=\' + this.value + \'&transportista=\' + transportista.value', update: 'resultado')}"	/>                      			  	                       
						</td>	
										
					</tr>	
												
				</tbody>
				
			</table>
			
		</div> 
  		 
  		<div id = "resultado"> </div>
    	
	</body>
	
</html>