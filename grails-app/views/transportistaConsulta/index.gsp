<%@ page contentType = "text/html;charset=UTF-8" %>

<!-- Muestra el listado de los tipos de inspecciones, fechas y listado de plantas para realizar la consulta del transportista -->
<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		<meta name = "layout" content = "main"/>
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/>
		<title> Consulta Transportista </title>

	</head>

	<body>

		<g:formRemote name = "frmConsultaavanzada" url = "[controller:'transportistaConsulta', action:'consultaTransportista']" 
		update = "consultaHistoricaTransportista">
	
  			<div class = "body">
  	
  				<br/>
  	  			<br/>	  

  				<table class = "table-form">
				  	
				  	<thead>
				  		
				  		<tr>
				  			<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
				  				
				  				<td class = "trhead" colspan = "2"> Consulta Histórica con el(los) Transportista(s) Asociados ${descripcionTransportista} </td>
				  			
				  			</sec:ifAnyGranted>
				  			
				  			<sec:ifAnyGranted roles = "ROLE_TRANSPORTISTA">
				  				
				  				<td class = "trhead" colspan = "2"> Consulta Histórica para el(los) Transportista(s) ${descripcionTransportista}</td>
				  		
				  			</sec:ifAnyGranted>
				  			
				  			<sec:ifAnyGranted roles = "ROLE_CLIENTE">
				  				
				  				<td class = "trhead" colspan = "2"> Consulta Histórica para el(los) Transportista(s) ${descripcionTransportista}</td>
				  		
				  			</sec:ifAnyGranted>
				  		</tr>
				  		
				  	</thead>
				  	
				  	<tbody>
			 			
			 			<tr class = "noclass">
			 	  			<td style = "width: 100px">Tipo Inspección: </td>
						
							<td>
								<g:select id = "tipoInspeccionId" name = "nInspeccion" from = "${tipoInspeccion}" optionKey = "id" required = ""
								optionValue = "descripcion" noSelection = "['':'Seleccione Una Inspeccion']" class = "many-to-one"/>
							</td>							 			 
  	  					</tr>
  	  	
  	  					<tr class = "noclass">
  	  						<td>Fecha Inicio:</td>
  	  						
  	  						<td> <g:datePicker name = "fechaInicial" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>							
  	  					</tr>
  	  		
  	  					<tr class = "noclass">
  	  						<td>Fecha Fin:</td>
  	  				
  	  						<td> <g:datePicker name = "fechaFinal" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>														
  	  					</tr>
  	  		
  	  					<tr class = "noclass">
		        			
		        			<td> Planta/Localidad: </td>
		 		
		 					<td> <g:select name = "planta"  from = "${planta}" noSelection = "['':'Todas Las Plantas']" /> </td>
		        
		        			<td hidden = ""> <input type = "text" value = "${name}" name = "nameUser"> </td>		
		        				     
					     </tr>
					     
			  	  		 <tr class = "noclass">  
			  	  		 	
			  	  		 	<td hidden = ""> 
			  	  		 	
			  	  		 		<iframe onload = "arrayToString(), tipoInspeccionSinSeleccion()" src = "${resource(dir: 'images/skin', file: 'guardar.png')}"> </iframe> 
			  	  		 	
			  	  		 	</td>             
  	  					</tr>
  	  					
  	  					<tr class = "noclass">
  	  					
  	  						<td>Buscar</td>
  	  						
  	  						<td>
  	  							<input type = "image"  src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" class = firstbutton name = "btnBuscar" 
  	  							value = "Buscar">
  	  						</td>
  	  									  	  			
  	  					</tr>  	  	
					
					</tbody>
				
				</table>
				
				<input type = "hidden" name = "transportista" value = "${descripcionTransportista}">
				<input type = "hidden" name = "plantasTotales" value = "${descripcionPlanta}">
				
			</div>  
  		
  		</g:formRemote>

  		<div id = "consultaHistoricaTransportista"> </div>  
	
	</body>
	
</html>