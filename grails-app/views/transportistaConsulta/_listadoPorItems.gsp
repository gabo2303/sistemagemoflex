<asset:javascript src = "filtro.js"/>

<table class = "table-form">
	
	<thead> <tr> <td class = "trhead" colspan = "2"> <label> Buscador y Descarga de Inspecciones </label> </td> </tr> </thead>
	
	<tbody>
		
		<tr> 
				
			<td width = "250px"> <input name = "filter" onkeyup = "filter2(this, 'listaInspecciones')" placeholder = "Ingrese dato a buscar"> </td>
			
			<td> <button class = "descargarExcel" id = "descargaExcel"> Descargar Excel </button> </td>
			 
		</tr>
	
	</tbody>
	
</table>

<br/>

<div hidden = "">

	<g:form action = "bajadaExcelPorItem">
						
		<g:submitButton  name = "Descargar Reporte (XLS)" id = "descargaExcelFinal"/>
		
		<g:hiddenField name = "inspeccion" value = "${params.nInspeccion}" />
		<g:hiddenField name = "plantaSeleccionada" value = "${plantaSeleccionada}" />
		<g:hiddenField name = "plantas" value = "${params.plantas}" />
		<g:hiddenField name = "item" value = "${item}" />
		<g:hiddenField name = "numeroTanque" value = "${numeroTanque}" />
		<g:hiddenField name = "transportistas" value = "${params.transportistas}" />
		
		<div hidden = ""> <g:datePicker name = "fechaInicial" value = "${params.fechaInicial}" precision = "day" /> </div>
		<div hidden = ""> <g:datePicker name = "fechaFinal" value = "${params.fechaFinal}" precision = "day" /> </div>
		
	</g:form>

</div>

<script> 
				
	$(document).ready(function(){ $('#descargaExcel').on('click', function(e){ $(document).ready(function(){$('#descargaExcelFinal').trigger('click'); }); });  });

</script>

<table class = "table-form" id = "listaInspecciones">
	
	<thead>
		
		<tr> <td colspan = "8" class = "trhead"> ${consultapor} </td> </tr>
	  	
	  	<tr>	
	  		<td class = "trhead"> Fecha Inspección </td> <td class = "trhead"> Item </td> <td class = "trhead"> Observación </td>	  			
	  		
	  		<td class = "trhead"> Planta/Localidad </td> <td class = "trhead"> Número Tanque </td> <td class = "trhead"> Transportista </td>
	  		
	  		<td class = "trhead"> Evaluación </td> <td class = "trhead"> Opción </td>
	  	</tr>
	  	
	</thead>	
	
	<tbody>
		
		<g:each in = "${listadoItemsPorFalla}" status = "i" var = "var" >
			
			<tr>		
				<td> <g:formatDate format = "dd-MM-yyyy" date = "${var[0]}" /> </td>
				
				<td> ${var[1]} </td> <td> ${var[2]} </td> <td> ${var[3]} </td> <td> ${var[4]} </td> <td> ${var[5]} </td>
				
				<td> <g:if test = "${var[6] == 0}"> Malo </g:if> <g:else> Crítico </g:else> </td>
				
				<td> 
					<g:link target = "_blank" controller = "inspeccion" params = "[tipoInspeccion:var[7], id:var[9]]" action = "show">
						
						<img src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" alt = "Grails"/> 
					
					</g:link>
				</td>							
			</tr>
								 														
		</g:each>	
			
	</tbody>  		
	  		
</table> 	