<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		<meta name = "layout" content = "main"/>
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/>
		<title>Resumen Inspecciones</title>
		
	</head>
	
	<body>
  		
  		<div class = "body">
  	  		
  	  		<br/>	  
  	  		
  			<g:if test = "${flash.message}">
								 
				<div id = "mensaje" style = "width: 230px; border-color: red; background-color: #FFC6C6; 
				box-shadow: inset 0px 0px 3px 0.5px red; border-radius: 3px; padding: 1.5px 0px 1.5px 10px; border-style:ridge; 
				border-width:2.5px; margin-left: 5%; color: red">${flash.message}</div>
				
				<br/>
					
			</g:if>
							
			<g:if test = "${flash.message2}">
						
				<div id = "mensaje" style = "width: 180px; border-color: red; background-color: #FFC6C6; 
				box-shadow: inset 0px 0px 3px 0.5px red; border-radius: 3px; padding: 1.5px 0px 1.5px 10px; border-style:ridge; 
				border-width:2.5px; margin-left: 5%; color: red">${flash.message2}</div>
				
				<br/>
				
			</g:if>
			
  			<table class = "table-form">
  					
  				<thead>
  						
  					<tr>
  						<td class = "trhead" colspan = "2">Resumen de Inspecciones para ${descripcionTransportista}</td>
  					</tr>
  					
  				</thead>
  	  			
  	  			<tbody>  	 
  	  				
			 		<tr class = "noclass">
  	  
  	  			 		<td width = "150px">Tipo Inspección: </td>
						
						<td>
							<g:select id = "tipoInspeccionId" name = "nInspeccion" from = "${tipoInspeccion}" optionKey = "id" required = ""
							optionValue = "descripcion" noSelection = "['':'Seleccione Una Inspeccion']" onchange = "pasarValores(),ocultarDiv()"
							class = "many-to-one"/>													                      			  	                       
						</td>	
					</tr>
  	  		
  	  				<tr class = "noclass">
  	  				
		            	<td>Planta/Localidad: </td>
		                
		                <td> 
		                	<g:select name = "planta" id = "planta" from = "${planta}" noSelection = "['':'Seleccione un Planta']" 
							onchange = "pasarValores()" class = "many-to-one" />  
						</td>
		         	</tr>
				
				</tbody>
			
			</table>			 
		 		  	
  			<table class = "table-form" id = "xls">		 
				
				<tbody>
  	  		 		
  	  		 		<tr class = "noclass">	
  	  		 				 
			 			<td colspan = "2">
			 				
			 				<g:jasperReport controller = "transportistaConsulta" id = "resumen" name = "Resumen Inspeccion" 
			 				jasper = "ResumenCabecera.jrxml" format = "XLS" action = "cabeceraResumen" bodyBefore = " " 
			 				buttonPosition = "below" delimiter = " " description = "Exportar Excel">
	  					
			  					<tr class = "noclass">
			  					
			  						<td hidden = ""> <input type = "text" id = "plantasTotales" name = "plantasTotales" value = "${descripcionPlanta}"> </td>
			  			  			<td hidden = ""> <input type = "text" id = "inspeccionx" name = "inspeccion" required = "required"> </td>
			  			  			<td hidden = ""> <input type = "text" id = "plantaSx" name = "plantaS" required = "required"> </td>
			  						<td hidden = ""> <input type = "text" id = "descripcionTransportista" name = "descripcionTransportista" value = "${descripcionTransportista}"> </td>
			  						
			  						<td width = "160px"> &nbsp;&nbsp;&nbsp;Fecha Inicio:</td>	
			  						
			  						<td> 
			  							<g:datePicker  name = "nfecha1" id = "fecha1" value = "${new Date()}" precision = "day" 
			  							years = "${1900..2999}"/>
			  						</td>
			  						
			  					</tr>	  				
			  					
			  					<tr class = "noclass">
			  					
			  						<td>Fecha Fin:</td>
			  						<td>  
			  							<g:datePicker name = "nfecha2" id = "fecha2" value = "${new Date()}" precision = "day" 
			  							years = "${1900..2999}"/>
			  						</td>
			  						
			  					</tr>
							
							</g:jasperReport>
							
						</td>
						
					</tr>			 		 	
				
				</tbody>		  	
  			
  			</table>   			
  		
  		</div>  
  
  		<script type = "text/javascript">

			function pasarValores()
			{
				var numeroI = document.getElementById('tipoInspeccionId').value;				
				var recibeIx = document.getElementById('inspeccionx')				
				recibeIx.setAttribute('value',numeroI);

				var plantaR = document.getElementById('planta').value;				
				var recibePx = document.getElementById('plantaSx')				
				recibePx.setAttribute('value',plantaR);			
			}

			function ocultarDiv()
			{
				$('#mensaje').hide()
				$('#mensaje2').hide()
			}  		

		</script>
	
	</body>
	
</html>