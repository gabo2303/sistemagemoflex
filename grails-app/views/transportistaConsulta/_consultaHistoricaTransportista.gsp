<!-- Template que muestra la lista con las inspecciones realizadas del transportista -->

<asset:javascript src = "filtro.js"/>

<table class = "table-form">
	
	<thead> 
		
		<tr>
			<td class = "trhead"> <label>Buscador de inspecciones</label> </td>
		</tr>
	
	</thead>
	
	<tbody>
		
		<tr class = "noclass">
		
			<td> <input name = "filter" onkeyup = "filter2(this, 'listaInspecciones')" placeholder = "Ingrese dato a buscar"> </td>
		
		</tr>
	
	</tbody>
	
</table>

<br/>

<table class = "table-form" id = "listaInspecciones">
	
	<thead>
		
		<tr class = "noclass">
		
	  		<td class = "trhead">Id </td>
	  		
	  		<td class = "trhead">Fecha Inspección</td>
	  		
	  		<td class = "trhead">Numero Tanque</td>
	  		
	  		<td class = "trhead">Marca</td>  			
	  		
	  		<td class = "trhead">Planta/Localidad</td>
	  		
	  		<td class = "trhead">Transportista</td>
	  		
	  		<td class = "trhead">Críticos</td>
	  		
	  		<td class = "trhead">Ver</td>  		
	  			  			
		</tr>
	  
	</thead>	
	  
	<tbody>
	  		
		<g:each in = "${cabecera}" status = "i" var = "var" >
			
			<tr>		
				<td>${var[0]}</td>
				
				<td><g:formatDate format = "dd-MM-yyyy" date = "${var[1]}"/></td>					
				
				<td style = "margin-left: auto;">${var[2]} </td>
				
				<td> ${var[3]} </td>				
				
				<td> ${var[4]} </td>
				
				<td> ${var[5]} </td>
				
				<td> ${var[6]} </td>
				
				<td>
					<g:link target = "_blank" controller = "inspeccion" params = "[tipoInspeccion:var[7], id:var[0]]" action = "show"> 
					
						<img src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" alt = "Grails"/> 
					
					</g:link>
				</td>							
			</tr>					 														
		
		</g:each>	
	
	</tbody>  		
	  		
</table>  	