<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		<meta name = "layout" content = "main"/>

		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/>

		<title> Consulta Por Fallas </title>
		
	</head>

	<body>
  
  		<div class = "body">
  
  			<br/> <br/>	  
			
			<input type = "hidden" id = "transportistas" value = "${transportistas}">
			<input type = "hidden" id = "plantas" value = "${plantas}">
			
  			<table class = "table-form">
  	
			  	<thead>
				  		
			  		<tr>
			  			<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
			  				
			  				<td class = "trhead" colspan = "2"> Consulta Por Item Malo Transportista/Concesionario Gemoflex</td>
			  			
			  			</sec:ifAnyGranted>
			  			
			  			<sec:ifAnyGranted roles = "ROLE_TRANSPORTISTA">
			  				
			  				<td class = "trhead" colspan = "2"> Consulta Por Item Malo Transportista/Concesionario ${transportista}</td>
			  		
			  			</sec:ifAnyGranted>
			  			
			  			<sec:ifAnyGranted roles = "ROLE_CLIENTE">
			  				
			  				<td class = "trhead" colspan = "2"> Consulta Por Item Malo Transportista/Concesionario ${cliente}</td>
			  		
			  			</sec:ifAnyGranted>
			  		</tr>
			  		
			  	</thead>
			  	
			  	<tbody>
			 
			 		<tr class = "noclass">  	  
  	  			 
						<td style = "width: 180px"> Tipo Inspección: </td>
						
						<td> 
							<g:select id = "tipoInspeccionId" name = "nInspeccion" from = "${tipoInspeccion}" optionKey = "id" required = "" optionValue = "descripcion" 
							noSelection = "['':'Seleccione Una Inspeccion']" onchange = "${remoteFunction(controller:'transportistaConsulta', action:'listaItem', 
							params:'\'id=\'+ this.value + \'&transportistas=\' + transportistas.value + \'&plantas=\' + plantas.value', update: 'itemDiv')}"/>                      			  	                       
						</td>		 
 	 				
 	 				</tr>	
  	  			
  	  			</tbody>
		
			</table>
		
		</div> 
  		
  		<div id = "itemDiv"> </div>
    	
	</body>
	
</html>