<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Creación de Imagen </title> </head>
	
	<body>
		
		<div class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Creación de Imagen </h1>
			
			<g:form resource = "${image}" action = "save" enctype = "multipart/form-data">
				
				<div class = "fieldcontain"> <label> Foto <input style = "margin-left:65%; margin-top:5%" type = "file" placeholder = "Seleccione Foto" name = "foto" required = "required"/> </label> </div>
				
				<div class = "fieldcontain">
	
					<g:hiddenField name = "idCabecera" value = "${folio}" /> 	
					
					<table class = "table-form" style = "margin-left:2.5%">
						
						<thead> <tr> <th colspan = "${imagenes.size}"> Foto </th> </tr> </thead>
						
						<tbody>
							
							<tr class = "even">								
								
								<g:each in = "${imagenes}" status = "i" var = "imagen">
									
									<td> <img width = "100" height = "50" alt = "Img" src = "/SistemaGemoflex/image/showImage/${imagen.id}"> </td>								
									
								</g:each>
								
							</tr>
						
						</tbody>
								
					</table> <br/>
					
				</div>				
				
				<fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Crear"/> </fieldset>
				
			</g:form>
					
		</div>
		
	</body>
	
</html>