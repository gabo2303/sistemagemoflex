<!-- MUESTRA EL LISTADO CON LOS TIPOS DE INSPECCIONES SEGUN EL CLIENTE -->

<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		<meta name = "layout" content = "main"/>

		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css">

		<title> Listado de Inspecciones </title>
	
	</head>

	<body>
  		
  		<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status" style = "font-size: 14px"> ${flash.message} </div> </g:if>
		
		<div class = "content scaffold-list">	
			
			<h1> Listado de Inspecciones </h1> <br/>
				
			<g:formRemote name = "tablaUsuario" url = "[controller:'inspeccion', action:'verInspeccion']" update = "body">
			
				<table style = "margin-left: 1.5%">
					
					<thead>
	
						<tr>
							<g:sortableColumn property = "id" title = "Folio" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
							numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
							
							<g:sortableColumn property = "fechaInspeccion" title = "Fecha Inspección" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, 
							transportista:params.transportista, numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>							
						
							<g:sortableColumn property = "planta" title = "Planta/Localidad" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
							numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
													
							<g:sortableColumn property = "transportista" title = "Transportista" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
							numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
						
							<g:sortableColumn property = "numero" title = "Número" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
							numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
							
							<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
							
								<g:sortableColumn property = "usuario" title = "Inspector" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
								numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
								
							</sec:ifAnyGranted>
							
							<g:sortableColumn property = "estado" title = "Estado" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
							numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>	
							
							<g:sortableColumn property = "idInspeccion" title = "Tipo Inspección" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
							numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
							
							<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
							
								<g:sortableColumn property = "enviado" title = "Env" params = "[folio:params.folio, fecha:params.folio, planta:params.planta, transportista:params.transportista, 
								numero:params.numero, inspector:params.inspector, estado:params.estado, nuevoMax:params.max]"/>
								
							</sec:ifAnyGranted>													
						</tr>
						
					</thead>
				
					<tbody>
					
						<tr class = "noclass">
							
							<td> <input pattern = "[0-9]{1,9}" size = 2 name = "folio" id = "folio" value = "${params.folio}"> </td>
							
							<td> <input type = "date" name = "fecha" id = "fecha" value = "${params.fecha}"> </td>
								
							<td> <input size = 10 name = "planta" id = "planta" value = "${params.planta}"> </td>
															
							<td> <input size = 20 name = "transportista" id = "transportista" value = "${params.transportista}"> </td>
							
							<td> <input size = 5 name = "numero" id = "numero" value = "${params.numero}"> </td>
							
							<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR"> 
							
								<td> <input size = 10 name = "usuario" id = "usuario" value = "${params.usuario}"> </td> 
							
							</sec:ifAnyGranted>
							
							<td> <input size = 7 name = "estado" id = "estado" value = "${params.estado}"> </td>
							
							<td> <input size = 10 name = "tipoInspeccion" id = "tipoInspeccion" value = "${params.tipoInspeccion}"> </td>
							
							<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR"> <td> </td> </sec:ifAnyGranted>
						</tr>
						
						<g:each in = "${listaEncontrados}" status = "i" var = "inspeccion">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
								
								<td> 
									<g:link target = "_blank" action = "show" id = "${inspeccion.id}"
									params = "[tipoInspeccion:inspeccion.idInspeccion, role:permiso, estado:inspeccion.estado]">
									 ${inspeccion.id} </g:link> 
								</td>
																
								<td> <g:formatDate date = "${inspeccion.fechaInspeccion}" format = "dd-MM-yyyy"/> </td>
															
								<td> ${inspeccion.planta} </td>
																	
								<td> ${inspeccion.transportista} </td>
								
								<td> ${inspeccion.numero} </td>
								
								<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
								
									<td> ${inspeccion.usuario} </td>
									
								</sec:ifAnyGranted>
								
								<g:if test = "${inspeccion.estado == 'APROBADO'}"> <td bgcolor = "C0F2BC"> ${inspeccion.estado} </td> </g:if>
								<g:elseif test = "${inspeccion.estado == 'RECHAZADO'}"> <td bgcolor = "F7788F"> ${inspeccion.estado} </td> </g:elseif>
								<g:elseif test = "${inspeccion.estado == 'NULO'}"> <td bgcolor = "787F77"> ${inspeccion.estado} </td> </g:elseif>
								<g:else> <td> ${inspeccion.estado} </td> </g:else>
																								
								<td> ${Maestros.MaestroTipoInspeccion.get(inspeccion.idInspeccion).descripcion.drop(15)} </td>	
								
								<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
								
									<td> <g:formatBoolean boolean = "${inspeccion.enviado}" false = "No" true = "Si"/> </td>
								
								</sec:ifAnyGranted>						
							</tr>
							
						</g:each>
					
					</tbody>
				
				</table>
				
				<div class = "paginations">
				
					<g:submitButton class = "botonBuscador" name = "Buscar"/>
																
					<g:paginate total = "${listaEncontradosTotal ?: 0}" params = "${params}"/>
						
					<g:select id = "comboPaginacion" name = "tablaPrincipalSelect" from = "${['Registros por página','5','10','20','30','40','50']}" 
					value = "${params.max}"	onchange = "${remoteFunction(controller: 'inspeccion', 
					params:'\'nuevoMax=\' + this.value + \'&folio=\' + folio.value + \'&fecha=\' + fecha.value + \'&planta=\' + planta.value + \'&transportista=\' + transportista.value + \'&numero=\' + numero.value + \'&usuario=\' + usuario.value + \'&estado=\' + estado.value + \'&tipoInspeccion=\' + tipoInspeccion.value', 
					action: 'verInspeccion', update: 'body')}"/>	
							
				</div>
						
			</g:formRemote>
			
     	</div>

	</body>
	
</html>