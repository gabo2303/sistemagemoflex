<!-- TEMPLATE QUE MUESTRA LISTADO DE TRANSPORTISTAS, ESTACIONES DE SERVICIO Y METER SEGUN SEA EL CASO -->
		
<table class = "table-form" >
		
	<g:if test = "${tipoInspeccion == '6'}">
					
		<tr class = "noclass">
					
			<td> Número de Estación de Servicio: <g:select name = "eServicio" from = "${estaciones}" optionKey = "codigoEstacion" noSelection = "['':'Seleccione Una Estacion de Servicio']"/> </td>
		
		</tr>
	
	</g:if>
	
	<tr class = "noclass">
		                    
		<td align = "right"> Transportista/Concesionario: 
		
			<g:select name = "transportista" from = "${transportistas.sort { it.descripcion }}" required = "" optionKey = "id" optionValue = "descripcion" noSelection = "['':'Seleccione Un Transportista']" 
			onchange = "${remoteFunction(controller:'inspeccion', action:'transportista', params:'\'transportista=\'+ this.value +\'&nombrePlanta=\'+ nombrePlanta.value', update: 'transpDiv')}"/> 
		
		</td>	
		
	</tr>
						
</table>

<g:hiddenField name = "nombrePlanta" id = "nombrePlanta" value = "${nombrePlanta}" />