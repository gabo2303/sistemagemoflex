<html>
	
	<head>

		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/> <meta name = "layout" content = "main"/>
		<meta name = "viewport" content = "width=device-width, initial-scale=0.5"/>

		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/>

		<title> Ingreso de Inspección </title>
		
	</head>

	<body>
		
		<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_INSPECTOR">
	  		
	  		<div class = "body">
	  			
	  			<g:if test = "${flash.message}"> <div class = "message" role = "status" style = "font-size: 14px"> ${flash.message} </div> </g:if>
	  			
	  			<g:elseif test = "${flash.warning}"> <div class = "errors" style = "font-size: 14px"> ${flash.warning} </div> </g:elseif>
	  			
	  			<g:form name = "formCabecera" url = "[action:'save']">
	  
	 				<g:render template = "form" />
	 			
	   	 			<br/> <fieldset class = "buttons" style = "margin-left: 46%; width:7%"> <input class = "save" type = "submit" value = "Guardar"> </fieldset>    
	    
	    		</g:form>
	     
			</div> 	
			
		</sec:ifAnyGranted>
		
		<sec:ifNotGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_INSPECTOR">
			
			<br/> <div class = "errors" style = "font-size: 14px"> Lo sentimos, no esta autorizado para acceder a esta página. </div>		
			
		</sec:ifNotGranted>
		
	</body>
	
</html>