<!-- TEMPLATE QUE MUESTRA CHECKLIST -->

<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css">

<g:if test = "${tipoInspeccion?.id != 13}">
	
	<g:hiddenField name = "nombreInspeccion" id = "nombreInspeccion"  value = "${tipoInspeccion.descripcion}" />
	
	<table class = "table-form">
			
		<thead> <tr> <th> Preguntas </th> <th colspan = "2"> Bueno </th> <th colspan = "2"> Malo </th> <th align="right"> Observación </th> </tr> </thead>
		
		<tbody>
							
			<g:each in = "${preguntas}" status = "i" var = "var" >
				
				<tr>	
					<g:if test = "${var.pregunta == 'Revision de 4 senaleticas de seguridad Rombo y NU'}">
						
						<td> ${var.pregunta} <img style  = "margin-left: 2%" alt = "Imagen" src = "${resource(dir: 'images', file: 'gas_comprimido.png')}" height = "42" width = "42"> 
						<img style = "margin-left: 2%" alt = "Imagen" src = "${resource(dir: 'images', file: 'nu_1075.png')}" height = "32" width = "82"> </td> 
					
					</g:if>
					
					<g:elseif test = "${var.pregunta == 'Cartel sobre Cabina del Vehiculo'}">
						
						<td> ${var.pregunta} <img style  = "margin-left: 2%" alt = "Imagen" src = "${resource(dir: 'images', file: 'gas_licuado.png')}" height = "32" width = "152"> </td> 
					
					</g:elseif> 
					
					<g:else> <td> ${var.pregunta} </td> </g:else>
					 				
					<td> ${var.bueno} </td> <td> <g:radio name = "p${var.id}" value = "Bueno" checked = "${1}"/> </td> 			
					<td> ${var.malo} </td> <td> <g:radio name = "p${var.id}" value = "Malo" checked = "${0}"/> </td> 										
					<td> <input type = "text" size = "70" name = "o${var.id}"> </td>
				</tr>							
			
			</g:each>	
					
		</tbody>
		
	</table>
	
</g:if>	
		
<g:if test = "${tipoInspeccion.id == 6}">
				 
	<table class = "table-form">		
	
		<tbody>
			
			<tr class = "noclass"> 
				
				<td width = 170px> Producto de Carga: </td> <td> <input size = "40" type = "text" name = "pCarga" > </td> 
			
				<td> Tipo de Carguio: </td> <td> <input size = "40" type = "text" name = "tCarguio" > </td> 
			
			</tr>
			
			<tr class = "noclass"> 
				
				<td> Succión Propia: </td> <td> <input size = "40" type = "text" name = "sPropia" > </td> 
			
				<td> Carga por otro Medio: </td> <td> <input size = "40" type = "text" name = "cOMedio" > </td> 
			
			</tr>
			
			<tr class = "noclass"> 
			
				<td> Fabricante Meter: </td> <td> <input size = "40" type = "text" name = "fMeter" > </td>
			
				<td> Modelo Meter: </td> <td> <input size = "40" type = "text" name = "mMeter" > </td>
				 
			</tr>
			
			<tr class = "noclass"> 
				
				<td> Serie Meter: </td> <td> <input size = "40" type = "text" name = "sMeter" > </td> 
			
				<td> Ticket Printer Meter: </td> <td> <input size = "40" type = "text" name = "tPrintMeter" > </td> 
				
			</tr>
		    				        
    	</tbody>						
    	
	</table>

</g:if>	

<table class = "table-form">
	
	<tbody>	<tr class = "noclass"> <td> <label> Comentarios: </label> </td> <td> <input name = "comentarios" size = "130"> </td> </tr> </tbody>
   
 </table>     