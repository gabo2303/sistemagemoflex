<!-- Muestra la parte de la cabecera de la inspección y si la inspección es de tipo red tae muestra la parte del meter -->					
<%@page import = "Maestros.MaestroTransportista"%>

<table class = "table-form">

	<thead> <tr> <th colspan = "4"> Ingreso de Inspección </th> </tr> </thead>
	
	<tr class = "noclass">
	
		<td align = "right"> Inspector: </td> <td> ${usuario.username} </td> 
				                    
	    <td align = "right"> Cliente: </td> <td> ${usuario.cliente} </td>
	    		    
		<g:hiddenField name = "usuario" value = "${usuario.username}" />			
		<g:hiddenField name = "estado" value = "PENDIENTE" />
		<g:hiddenField name = "cliente" value = "${usuario.cliente}" id = "cliente" />
	    						 
	</tr>
	
	<tr class = "noclass">
		                    
	   	<td> Fecha: </td> <td> <g:datePicker name = "fecha" value = "${new Date()}" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR] + 5}"/> </td>
		
		<td> Tipo Inspección: </td>
		
		<td>
			<g:select id = "tipoInspeccionId" name = "nInspeccion" from = "${tipoInspeccion.sort { it.descripcion }}" optionKey = "id" required = "" optionValue = "descripcion"									 
			noSelection = "['':'Seleccione Una Inspección']" onchange = "${remoteFunction(controller:'inspeccion', action:'items', 
			params:'\'id=\'+ this.value', update: 'itemDiv')}"/>          			  	                       
		</td>		 
	</tr> 	
						 	
	<tr class = "noclass">
	
		<td> Planta/Localidad :</td>
	    
	    <td>
	    	<g:select name = "planta"  from = "${planta.sort { it.descripcion }}" required = "" noSelection = "['':'Seleccione Una Planta']" onchange = "${remoteFunction(
			controller:'inspeccion', action:'transPlanta', params:'\'planta=\'+ this.value +\'&cliente=\'+ cliente.value +\'&tipoInspeccion=\'+ tipoInspeccionId.value', update: 'transp')}"/> 
		</td>                    
												
		<td align = "right"> Nombre Conductor </td>
		
		<td> <input type = "text" name = "nombreConductor" id = "nombreConductor" size = "55"  autofocus required = "required"/>
	</tr>		  
	                    
</table>
	             
<div id = "transp"> </div>
<div id = "transpDiv"> </div>	
<div id = "itemDiv"> </div>				