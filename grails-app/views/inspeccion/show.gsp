<%@ page contentType = "text/html;charset=UTF-8" %>
<!-- MUESTRA LA INSPECCION REALIZADA CON LA OPCION DE MODIFICAR -->

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/> <meta name = "layout" content = "main"/> <link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css">
		
		<title> Folio N° ${cabecera.id} </title> <tooltip:resources/>	
		
	</head>

	<body> 		
 				
		<g:set var = "nombreInspector" value = "${seguridad.Usuario.findByUsername(cabecera.usuario)}"/>
 		<g:set var = "cliente" value = "${Maestros.MaestroTipoInspeccion.get(cabecera.idInspeccion).cliente.razonSocial}" />
 		
 		<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status" style = "width: 90%; margin-left:5%"> ${flash.message} </div> </g:if>
		<g:elseif test = "${flash.warning}"> <div class = "errors" style = "width: 90%; margin-left:5%"> ${flash.warning} </div> </g:elseif>
		
 		<h1 style = "margin-left: 5%"> Folio ${cabecera.id} (${descripcion}) </h1>
 		
		<g:if test = "${cabecera.estado == 'APROBADO' && !cabecera.enviado && cliente != 'Lipigas'}"> 
			
			<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">	
				
				<br/>		
			
				<table class = "table-form">
					
					<thead> <tr> <th colspan = "2"> Correos </th> </tr> </thead>
					
					<tbody>
					
						<tr class = "noclass"> 
							
							<td> Correos de Cliente: </td> 
								
							<td> 							
								<input name = "correoCliente" size = 115 readonly = "readonly" placeholder = "Este campo no puede estar vacío para el envío de correo. Favor asociar."
								value = "${correoCliente.toString().substring(1, correoCliente.toString().length() - 1)}" />						
							</td>			
											 
						</tr>
						
						<tr class = "noclass"> 
							
							<td> Correos de Transportistas: </td> 
						
							<td>
								<input size = 115 name = "correoTransportista" readonly = "readonly" placeholder = "Este campo no puede estar vacío para el envío de correo. Favor asociar."
								value = "${correoTransportista.toString().substring(1, correoTransportista.toString().length() - 1)}"/>							 
							</td> 
						</tr>
									
					</tbody>
					
				</table>
						
				<fieldset id = "areaReporte" class = "buttons" style = "margin-left: 45%; width: 142px;">
					
					<g:jasperForm action = "reporte" update = "body" controller = "inspeccion" jasper = "Informe.jrxml" 
					name = "${cabecera.numero} - ${cabecera.planta} - [${g.formatDate(date: cabecera.fechaInspeccion, format: 'dd-MM-yyyy')}] - ${descripcion}" class = "botonReporte">											
						
	            		<input type = "hidden" name = "cabecera" value = "${cabecera.id}">	          					
	            				            			
	            		<input type = "hidden" name = "correoFinal" value = "${correoCliente << correoTransportista.toString().substring(1, correoTransportista.toString().length() - 1)}">	
	            						
						<g:jasperButton class = "enviarCorreo" format = "XLS" text = "Enviar Correo"/>					
						
<%--						<script> $(".enviarCorreo").click(function() { setTimeout(function() { location.reload(); }, 20000); }); </script> --%>
			  			  						
					</g:jasperForm>
					
				</fieldset>
			
			</sec:ifAnyGranted>
			
		</g:if>

 		<br/>
 		
 		<table class = "table-form">
  
			<thead> <tr> <th colspan = "4"> Datos </th> </tr> </thead>
			
			<tbody>
								
				<tr class = "noclass">		                    
					
					<td width = "60px"> Inspector: </td> <td width = "300px"> ${cabecera.usuario} </td>
				            							  	                    
					<td width = "95px"> Fecha: </td> <td> <g:formatDate date = "${cabecera.fechaInspeccion}" format = "dd-MM-yyyy"/> </td>
								
				</tr>
			
				<tr class = "noclass"> <td> Planta/Localidad: </td> <td> ${cabecera.planta} </td> <td> Conductor: </td> <td> ${cabecera.conductor} </td> </tr>
			
				<tr class = "noclass">
					
					<td> Transportista: </td> <td> ${cabecera.transportista} </td>
					
					<g:if test = "${cabecera.nEstacionServicio}"> <td> E. de Servicio: </td> <td> ${cabecera.nEstacionServicio} </td> </g:if>
					
					<g:else> <td colspan = "2"> </td> </g:else>		
					
				</tr>
				                    
				<tr class = "noclass">
				
				  	<g:if test = "${cabecera.cEstanque != 'No aplica'}"> 
				  	
				  		<td> Camion Tanque: </td> <td colspan = "3"> ${cabecera.cEstanque} </td> 
				  	
				  	</g:if>			  	
				  	
				  	<g:else> <td> Tracto: </td> <td> ${cabecera.tracto} </td> <td> Tanque: </td> <td> ${cabecera.tanque} </td> </g:else>
				</tr>
				
			</tbody>
			
		</table>
		
		<br/>
		
		<g:if test = "${cabecera.idInspeccion != 13}">      
			
			<table class = "table-form">    
		      
				<thead> <tr> <th> Items </th> <th> P. Obtenido </th> <th> P. Máximo </th> <th> Evaluación Item </th> <th> Observación </th> </tr> </thead>
				            
				<g:each in = "${detalle}" status = "i" var = "var">
						                    
					<g:if test = "${var.critico == 1 && var.evaluacionItemBm == 'Malo'}">  <tr class = "nostyle" bgcolor = "F7788F"> </g:if> 
						
					<g:else> <tr> </g:else>			
					
					   	<td width = "500px"> 
					   		
					   		<g:if test = "${var.pregunta == 'Revision de 4 senaleticas de seguridad Rombo y NU'}">
						
								${var.pregunta} <img style  = "margin-left: 2%" alt = "Imagen" src = "${resource(dir: 'images', file: 'gas_comprimido.png')}" height = "42" width = "42"> 
								<img style = "margin-left: 2%" alt = "Imagen" src = "${resource(dir: 'images', file: 'nu_1075.png')}" height = "32" width = "82"> 
							
							</g:if>
							
							<g:elseif test = "${var.pregunta == 'Cartel sobre Cabina del Vehiculo'}">
								
								${var.pregunta} <img style  = "margin-left: 2%" alt = "Imagen" src = "${resource(dir: 'images', file: 'gas_licuado.png')}" height = "32" width = "152">  
							
							</g:elseif> 
							
							<g:else> ${var.pregunta} </g:else> 
						
						</td>
					   	                  	
					   	<td> ${var.evaluacionItem} </td>
					   	
					   	<td> ${var.puntajeItemBueno} </td>
					
					   	<td> ${var.evaluacionItemBm} </td>
						
						<g:if test = "${var.observacion}">
					   		
					   		<td onmouseover = "tooltip.show('${var.observacion}');" onmouseout = "tooltip.hide();"> <input size = "50" value = "${var.observacion}" disabled = "disabled"> </td>
					   		
						</g:if>
						
						<g:else> <td> <input size = "50" value = "${var.observacion}" disabled = "disabled"> </td> </g:else>
					</tr>		                    
				
				</g:each>
			  
			</table>
  		
  		</g:if>
  		
  		<g:else> <h1 align = "center" style = "color: red;"> Vehículo Temporalmente Fuera de Servicio (Sin Evaluación) </h1> <br/> </g:else>
  		
  		<br/>
  		
  		<g:if test = "${fotos}">
	  		  		
	  		<table class = "table-form">
				
				<thead> <tr> <th colspan = "${fotos.size}"> Fotos </th> </tr> </thead>
				
				<tbody>
					
					<tr class = "noclass">
						
						<g:each in = "${fotos}" status = "i" var = "imageInstance">					
														
							<td>							
								<a target = "_blank" href = "/SistemaGemoflex/image/showImage/${imageInstance.id}"> 
									
									<img width = "100" height = "50" alt = "Img" src = "/SistemaGemoflex/image/showImage/${imageInstance.id}"/>
								
								</a> 
							</td>	
																
						</g:each>
						
					</tr>	
					
				</tbody>
									
			</table>
	  		
	  		<br/>
  		
  		</g:if>
  		
  		<g:if test = "${cabecera.idInspeccion == 6}">
  		
			<table class = "table-form" style = "width: 40%; margin-left: 30%">		
			
				<tbody>
					
					<tr> <td width = 170px> Producto de Carga: </td> <td> <input value = "${cabecera.productoCarga}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Tipo de carguio: </td> <td> <input value = "${cabecera.tipoCarguio}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Succión propia: </td> <td> <input value = "${cabecera.succionPropia}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Carga por otro medio: </td> <td> <input value = "${cabecera.cargaOtroMedio}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Fabricante Meter: </td> <td> <input value = "${cabecera.fabricanteMeter}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Modelo Meter: </td> <td> <input value = "${cabecera.modeloMeter}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Serie Meter: </td> <td> <input value = "${cabecera.serieMeter}" size = "32" readonly = "readonly"/> </td> </tr>
					
					<tr> <td> Ticket Printer Meter: </td> <td> <input value = "${cabecera.ticketPrinterMeter}" size = "32" readonly = "readonly"/> </td> </tr>
				    				        
		    	</tbody>				
		    		    			
			</table>
		  	
		  	<br/>
	  	
	  	</g:if>
	  						
		<table class = "table-form" style = "width: 40%; margin-left: 30%">
		
			<tbody>		
				
				<tr> <td> Total Buenos: </td> <td> ${cabecera.totalBueno} </td> </tr>							
				
				<tr> <td> Total Malos: </td> <td> ${cabecera.totalMalo} </td> </tr>						
				
				<tr> <td> Puntaje Total: </td> <td> ${cabecera.puntajeTotal} </td> </tr>						
				
				<tr> <td> Índice Operación: </td> <td> <g:formatNumber number = "${cabecera.indiceOperacion}" format = "###,###.###"/> % </td> </tr>						
				
				<tr> <td> Cantidad de Críticos: </td> <td> ${cabecera.totalCriticos} </td> </tr>						
				
				<tr> <td> Cantidad de Items Evaluados: </td> <td> ${detalle.size} </td> </tr>						
				
				<tr> <td> Comentarios: </td> <td> <textarea cols = "30" rows = "3" maxlength = "250" readonly = "readonly"> ${cabecera.comentario} </textarea> </td> </tr>											                        
			
			</tbody>              
		
		</table>		
		
	
		<g:if test = "${cabecera.estado == 'RECHAZADO' || cabecera.estado == 'PENDIENTE'}" >
			
			<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_INSPECTOR">			 
				
				<br/> <fieldset class = "buttons" style = "margin-left: 45%; width: 90px;">
						
					<g:link action = "edit" controller = "inspeccion" params = "[tipoInspeccion:cabecera.idInspeccion, esta:cabecera.estado, id:cabecera.id]" class = "edit"> Editar </g:link>
				
				</fieldset>
									
			</sec:ifAnyGranted>
		
		</g:if>
		
		
		<g:set var = "role" value = "${seguridad.Usuario.findByUsername(sec.username()).getAuthorities().authority.any { it == 'ROLE_ADMINISTRADOR' || it == 'ROLE_SUPERADMINISTRADOR' }}" />
		
		<g:if test = "${cabecera.estado == 'APROBADO'}">
										
			<br/> <g:if test = "${cliente == 'Lipigas' || (cliente != 'Lipigas' && role && cabecera.enviado)}"> <fieldset class = "buttons" style = "width: 285px; margin-left: 40%"> </g:if> <g:else> <fieldset class = "buttons" style = "width: 180px; margin-left: 43%"> </g:else>
					
				<g:if test = "${(cliente == 'Copec' && cabecera.enviado) || cliente != 'Copec'}"> <input type = "submit" id = "reporte" class = "descargarExcel" value = "Descargar Reporte"/> </g:if>					
				<g:if test = "${cliente == 'Lipigas'}"> <input type = "submit" id = "anular" class = "delete" value = "Anular"> </g:if>						 						
				<g:if test = "${cliente != 'Lipigas' && role}"> <input type = "submit" id = "editar" class = "edit" value = "Editar"/> </g:if>				
								
			</fieldset>
										
		</g:if>
		
		<g:if test = "${cabecera.estado == 'NULO'}">
										
			<br/> <g:if test = "${cliente != 'Lipigas' && role}"> 
				
				<fieldset class = "buttons" style = "width: 180px; margin-left: 43%"> 
					
					<input type = "submit" id = "editar" class = "edit" value = "Editar"/> 
				
				</fieldset>		
			
			</g:if>			
							
		</g:if>
		
		<script> 
				
			$(document).ready(function(){ $('#reporte').on('click', function(e){ $(document).ready(function(){$('#reporteFinal').trigger('click'); }); });  }); 
			$(document).ready(function(){ $('#anular').on('click', function(e){ $(document).ready(function(){$('#anularFinal').trigger('click'); }); });  }); 
			$(document).ready(function(){ $('#editar').on('click', function(e){ $(document).ready(function(){$('#editarFinal').trigger('click'); }); });  });

		</script>
		
		
		<div hidden = ""> 
			
			<g:jasperForm action = "reporte" update = "body" controller = "inspeccion" jasper = "Informe.jrxml" 
			name = "${cabecera.numero} - ${cabecera.planta} - [${g.formatDate(date: cabecera.fechaInspeccion, format: 'dd-MM-yyyy')}] - ${descripcion}">											
				
           		<g:hiddenField name = "cabecera" value = "${cabecera.id}" /> <input type = "submit" id = "reporteFinal" /> 					
								 						
			</g:jasperForm>
			
			<g:form action = "anularInforme"> <g:hiddenField name = "cabecera" value = "${cabecera.id}" /> <input type = "submit" id = "anularFinal" onclick = "return confirm('¿Esta seguro de Anular el informe?')"> </g:form> 
			
			<g:form action = "edit" params = "[tipoInspeccion:cabecera.idInspeccion, esta:cabecera.estado, id:cabecera.id]" class = "edit"> <input type = "submit" id = "editarFinal"> </g:form>
			
		</div>
		
	</body>

</html>