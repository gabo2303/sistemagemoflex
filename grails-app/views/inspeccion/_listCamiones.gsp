<!-- TEMPLATE QUE MUESTRA LISTADO DE CAMIONES, TRACTO Y TANQUE -->

<table class = "table-form" >	       
		                  
	<g:if test = "${cliente.razonSocial == 'Lipigas'}">
		
		<tr class = "noclass">		                    
			
			<td align = "right"> Camion: </td>
			<td colspan = 3> <g:select optionKey = "id"  optionValue="${{it.numero +' | '+it.patente +' | '+it.marca+' | '+it.capacidad}}" name = "camionTanque" noSelection = "['':'Seleccione']" from = "${camiones}" required = ""/> </td>
		   	<g:hiddenField value = "${transport.id}" name = "transporte" id = "transporte" />
		   	
		</tr>
	
	</g:if>
	
	<g:else>	       
		       
		<tr class = "noclass">		                    
			
			<td align = "right"> Camion: </td>
			
			<td colspan = 3>
				
				<g:select optionKey = "id" optionValue = "${{it.numero +' | '+it.patente +' | '+it.marca+' | '+it.capacidad}}" name = "camionTanque" required = ""
				id = "cEstanque" noSelection = "['':'Seleccione']" from = "${camiones.findAll { it.tipoCamion == 'Camion Tanque' }}"  onchange = "desahabilitarTractoyEstanque(this.value)" />
			
			</td>    
		    
		</tr>
		
		<tr class = "noclass">
		
			<td align = "right"> N°Tracto: </td>
			
			<td> <g:select optionKey = "id" optionValue = "${{it.numero +' | ' + it.patente +' | ' + it.marca}}" name = "tracto" id = "nTracto" noSelection = "['':'Seleccione']" 
			from = "${camiones.findAll { it.tipoCamion == 'Tracto' }}" onchange = "desahabilitarCamionEstanque(this.value)" required = ""/>  </td>
		    
		    <td align = "right"> N° de Tanque: </td>
			
			<td> <g:select optionKey = "id" optionValue = "${{it.numero +' | '+it.patente +' | '+it.marca+' | '+it.capacidad}}" name = "tanque" id = "nEstanque" required = "" 
			from = "${camiones.findAll { it.tipoCamion == 'Tanque' }}" noSelection = "['':'Seleccione']" onchange = "desahabilitarCamionEstanque(this.value)" /> </td>
		    
		    <g:hiddenField value = "${transport.id}" name = "transporte" id = "transporte" />
		      
		</tr>

	</g:else>

</table>
					
<script type = "text/javascript">

	//Función que permite  desahabilitar las opciones de tracto y tanque cuando se selecciona un camión.
	function desahabilitarTractoyEstanque(valor)
	{			
			var nTracto = document.getElementById("nTracto")
			var nEstanque = document.getElementById("nEstanque")		
			nTracto.disabled = true;
			nEstanque.disabled = true;	
	}
	
	//Funcion que permite  desahabilitar la opcion camion cuanndo se selecciona un tracto o un tanque.
	function desahabilitarCamionEstanque(value)
	{	
		var camionEstanque = document.getElementById("cEstanque")		
		camionEstanque.disabled = true;
	}	
	
</script>	