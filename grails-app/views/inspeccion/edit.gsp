<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		
		<meta name = "layout" content = "main"/>
		
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css">
		
		<title> Folio N° ${folio.id} </title> <tooltip:resources/>
	
	</head>

	<body>

		<g:form name = "update" controller = "inspeccion" action = "update" method = "PUT" params = "[folio:folio.id]">
			    
		    <h1 style = "margin-left: 5%"> Editando Checklist del Folio ${folio.id} (${descripcion}) </h1>
		    
		    <br/> <br/>
		    
		    <table class = "table-form"> 
		    	
		    	<thead> <tr> <th colspan = "2"> Datos </th> </tr> </thead>
		    	
		    	<tbody>
		    		
		    		<tr class = "noclass">		    		
		    			
		    			<td width = "400"> Fecha: <g:datePicker name = "fecha" value = "${folio.fechaInspeccion}" precision = "day"/> </td>
		    			
		    			<td> Chofer: <input name = "chofer" value = "${folio.conductor}"> </td>
		    			
		    		</tr>
		    	
		    	</tbody>
		    	
		    </table>
		    
		    <br/> <g:if test = "${folio.idInspeccion != 13}">
		      
				<table class = "table-form">    
						      
					<thead> <tr> <th> Items </th> <th colspan = "2"> Bueno </th> <th colspan = "2"> Malo </th> <th> Observación </th> </tr> </thead>
					     
					<g:each in = "${det}" status = "i" var = "detalle">
						               
						<g:if test = "${detalle.critico == 1 && detalle.evaluacionItemBm == 'Malo'}"> <tr class = "nostyle" bgcolor = "F7788F"> </g:if> <g:else> <tr> </g:else>
								
						   	<td> ${detalle.pregunta} </td>
						
						   	<td> ${detalle.bueno} </td> 
						   	
						   	<td>	
						   		<g:if test = "${detalle.evaluacionItemBm == 'Bueno'}"> <g:radio value = "1" name = "${detalle.id}" checked = "${1}"/> </g:if> 
						   		
						   		<g:else> <g:radio value = "1" name = "${detalle.id}" checked = "${0}"/> </g:else> 
						   	</td>
						
						   	<td> ${detalle.malo} </td> 
						   		
						   	<td>	
						   		<g:if test = "${detalle.evaluacionItemBm == 'Malo'}"> <g:radio value = "0" name = "${detalle.id}" checked = "${1}"/> </g:if> 
						   		
						   		<g:else> <g:radio value = "0" name = "${detalle.id}" checked = "${0}"/> </g:else> 
						   	</td>
							
							<g:if test = "${detalle.observacion}">
						   		
						   		<td id = "observacion${i}"> <input onmouseover = "" class = "observa" size = "50" name = "o${detalle.id}" 
						   		value = "${detalle.observacion}" onmouseleave = "" maxlength = "250">  </td>
						   		
							</g:if>
							
							<g:else>
								
								<td id = "observacion${i}"> <input class = "observa" size = "50" name = "o${detalle.id}" maxlength = "250" 
								value = "${detalle.observacion}"> </td>
								
							</g:else>
						</tr>		                    
					
					</g:each>
				  
				</table>
			
			</g:if>
			
			<g:if test = "${folio.idInspeccion == 6}">
			
				<table class = "table-form">		
				
					<tbody>
					
						<tr> <td width = 170px> Producto de Carga: </td> <td> <input name = "productoCarga" id = "pCarga" value = "${folio.productoCarga}"> </td> </tr>
						
						<tr> <td> Tipo de carguio: </td> <td> <input name = "tipoCarguio" value = "${folio.tipoCarguio}"> </td> </tr>
						
						<tr> <td> Succión Propia: </td> <td> <input name = "succionPropia" value = "${folio.succionPropia}"> </td> </tr>
						
						<tr> <td> Carga por otro Medio: </td> <td> <input name = "cargaOtroMedio" value = "${folio.cargaOtroMedio}"> </td> </tr>
						
						<tr> <td> Fabricante Meter: </td> <td> <input name = "fabricanteMeter" value = "${folio.fabricanteMeter}"> </td> </tr>
						
						<tr> <td> Modelo Meter: </td> <td> <input name = "modeloMeter" value = "${folio.modeloMeter}"> </td> </tr>
						
						<tr> <td> Serie Meter: </td> <td> <input name = "serieMeter" value = "${folio.serieMeter}"> </td> </tr>
						
						<tr> <td> Ticket Printer Meter: </td> <td> <input name = "ticketPrinterMeter" value = "${folio.ticketPrinterMeter}"> </td> </tr>
					    				        
			    	</tbody>
			    							
				</table>
				
			</g:if>    
			  						
			<table class = "table-form">
				
				<thead> <tr> <th colspan = "4"> Adicionales </th> </tr> </thead>	
				
				<tbody>
					
					<tr class = "noclass">
					
						<td width = "500"> 
							
							Comentarios: <textarea name = "comentario" rows = "2" cols = "50" wrap = "soft" maxlength = "250">${folio.comentario}</textarea> 
						
						</td>
											
						<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">
						
							<td width = "300"> 
								
								Estado: <g:select name = "estado" from = "${['PENDIENTE','APROBADO','NULO', 'RECHAZADO']}" value = "${folio.estado}"/>
							
							</td>
							
						</sec:ifAnyGranted>
						
						<sec:ifAnyGranted roles = "ROLE_INSPECTOR"> 
							
							<td> <input name = "estado" value = "${folio.estado}" hidden = ""/> </td> 
						
						</sec:ifAnyGranted>
						
						<td width = "150px"> Agregar Imagen: </td>
						
						<td>	
							<g:link target = "_blank" controller = "image" params = "[cabecera:folio.id]" action = "create"> 
								
								<g:img style = "margin-left:-5%" dir = "images/skin" file = "camera_icon.png"/> 
							
							</g:link>
						</td>
											
					</tr>
					          
				</tbody>
				
			</table>	
			
			<table class = "table-form">
				
				<tr class = "noclass">
				
					<g:each in = "${fotos}" status = "i" var = "imageInstance">					
					
						<td id = "img" class = "noclass">			
						
							<a target = "_blank" href = "/SistemaGemoflex/image/showImage/${imageInstance.id}">
						 
					  			<img width = "100" height = "50" alt = "Img" src = "/SistemaGemoflex/image/showImage/${imageInstance.id}"/>
					  	
					  		</a>
				   		
				   			<input type = "checkbox" class = "eliminar" name = "i${imageInstance.id}">
					
						</td>
								
					</g:each>
				
				</tr>
									
			</table>
			 
			<br/> <fieldset class = "buttons" style = "margin-left: 45%; width: 120px;"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
						
		</g:form>
		
	</body>

</html>
