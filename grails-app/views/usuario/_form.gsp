<%@ page import = "seguridad.Usuario" %>

<div class = "fieldcontain">
	
	<label for = "username"> Username <span class = "required-indicator">*</span> </label> <g:textField name = "username" required = "" value = "${usuarioInstance?.username}"/>

</div>

<div class = "fieldcontain">

	<label for = "password"> Contraseña <span class = "required-indicator">*</span> </label> <g:textField name = "password" required = "" value = "${usuarioInstance?.password}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "nombre"> Nombre <span class="required-indicator">*</span> </label> <g:textField name = "nombre" maxlength = "150" required = "" value = "${usuarioInstance?.nombre}"/>

</div>

<div class = "fieldcontain">

	<label for = "apellido"> Apellido <span class = "required-indicator">*</span> </label> <g:textField name = "apellido" maxlength = "150" required = "" value = "${usuarioInstance?.apellido}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label> 
	
	<g:select name = "cliente.id" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" required = "" value = "${usuarioInstance?.cliente?.id}" />

</div>