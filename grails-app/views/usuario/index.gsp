<%@ page import = "seguridad.Usuario" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Usuarios </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Usuario </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
		
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<g:form action = "index">
			
				<h1> Listado de Usuarios </h1> <br/>
				
				<table style = "margin-left: 2.5%">
					
					<thead> <tr> <th> Username </th> <th> Nombre </th> <th> Apellido </th> <th> Cliente </th> <th> Activo </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass">
						 
							<td> <input name = "username" value = "${params.username}" />  </td>
							<td> <input name = "nombre" value = "${params.nombre}" /> </td>
							<td> <input name = "apellido" value = "${params.apellido}" /> </td>
							<td> <g:select name = "cliente" from = "${Maestros.MaestroCliente.list().sort { it.razonSocial }}" noSelection = "['':'Seleccione']" optionKey = "id" value = "${params.cliente}"/> </td>
							<td> <g:select name = "activo" from = "${['Si', 'No']}" noSelection = "['':'Seleccione']" value = "${params.activo}"/> </td>
						
						</tr>
						
						<g:each in = "${usuarios}" status = "i" var = "usuario">
							
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td> <g:link action = "show" id = "${usuario.id}"> ${usuario.username} </g:link> </td>
								
								<td> ${usuario.nombre} </td> <td> ${usuario.apellido}</td> <td> ${usuario.cliente}</td>
							
								<td> <g:formatBoolean boolean = "${usuario.accountLocked}" false = "Si" true = "No"/> </td>
							
							</tr>
							
						</g:each>
						
					</tbody>
					
				</table>
				
				<div class = "paginations"> <g:submitButton name = "Filtrar" class = "botonBuscador"/> <g:paginate total = "${usuarios.totalCount ?: 0}" /> </div>
			
			</g:form>
			
		</div>
		
	</body>
	
</html>