<%@ page import = "seguridad.Usuario" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Mostrando Usuario </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
		
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Usuarios </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Usuario </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Usuario </h1>
			
			<ol class = "property-list usuario">
			
				<li class = "fieldcontain"> <span class = "property-label"> Nombre </span> <span class = "property-value"> ${usuario.nombre} </span> </li>

				<li class = "fieldcontain"> <span class = "property-label"> Apellido </span> <span class = "property-value"> ${usuario.apellido} </span> </li>

				<li class = "fieldcontain"> <span class = "property-label"> Username </span> <span class = "property-value"> ${usuario.username} </span> </li>
		
				<li class = "fieldcontain"> <span class = "property-label"> Cliente </span> <span class = "property-value"> ${usuario?.cliente} </span> </li>
		
				<li class = "fieldcontain"> 
					
					<span class = "property-label"> Activo </span> 
					
					<span class = "property-value"> <g:formatBoolean boolean = "${usuario?.accountLocked}" false = "Si" true = "No" /> </span>
					
				</li>

			</ol>
			
			<g:form resource = "${usuario}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
				
					<g:link class = "edit" action = "edit" resource = "${usuario}"> Editar </g:link>
					
					<g:if test = "${!usuario.accountLocked}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar el Usuario?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<div hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </div>
					
					</g:if>
					
					<g:else>
						
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
						
						<script>
							
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar el Usuario?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
						
						</script>
						
						<div hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </div>
					
					</g:else>
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>