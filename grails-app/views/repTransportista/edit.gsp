<%@ page import = "Maestros.RepTransportista" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Editando Usuario-Transportista </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class = "list" action = "index"> Listado Usuario-Transportista </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Usuario-Transportista </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editar Usuario Transportista </h1>
			
			<g:form resource = "repTransportistaInstance" action = "update" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${repTransportistaInstance?.version}" />
				
				<fieldset class = "form">
					
					<div class = "fieldcontain"> <label for = "cliente"> Cliente </label> ${repTransportistaInstance?.transportista?.cliente} </div>
					
					<div class = "fieldcontain"> <label for = "usuario"> Usuario </label> ${repTransportistaInstance.usuario.username} </div>
					
					<div class = "fieldcontain">					
						
						<label for = "transportista"> Transportista </label>
					
						<g:if test = "${repTransportistaInstance.transportista}">							
						
							<g:select optionValue = "descripcion" id = "transportista" name = "transportista.id" from = "${transportistaList}" optionKey = "id" 
							requiered = "" value = "${repTransportistaInstance?.transportista?.id}"/>
																					
						</g:if>
						
					</div>
					
				</fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>