<%@ page import = "Maestros.RepTransportista" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Asociaciones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Asociación </g:link> </li> </ul> </div> 
		
		<div id = "list-repTransportista" class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if> 
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1 align = "center"> Listado de Asociaciones </h1> <br/>
			
			<g:form action = 'index'>
			
				<table style = "margin-left: 25%; width: 50%"> 
					
					<thead> <tr> <th> Id </th> <th> Transportista </th> <th> Usuario </th> </tr> </thead> 
					
					<tbody>
						
						<tr class = "noclass"> 
							<td> </td>
							<td> <g:select name = "transportista" from = "${Maestros.MaestroTransportista.list().sort { it.descripcion }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.transportista}" /> </td>
							<td> <g:select name = "usuario" from = "${seguridad.Usuario.list().sort { it.username }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.usuario}" /> </td>
						</tr>
						
						<g:each in = "${usuariosTransportistas}" status = "i" var = "repTransportista">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> <g:link action = "show" id = "${repTransportista.id}"> ${repTransportista.id} </g:link> </td>
						
								<td> ${repTransportista.transportista} </td>
								
								<td> ${repTransportista.usuario} </td>
							
							</tr>
						
						</g:each>
						
					</tbody>
					
				</table>
			
				<div class = "paginations" style = "margin-left: 25%; width: 49.5%"> 
				
					<g:submitButton name = "Buscar" class = "botonBuscador" /> <g:paginate total = "${usuariosTransportistas.totalCount ?: 0}" /> 
				
				</div>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>