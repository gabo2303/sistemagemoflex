<%@ page import = "Maestros.RepTransportista" %>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label>
	
	<g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" required = "" noSelection = "['':'Seleccione Cliente']"
	onchange = "${remoteFunction(controller:'repTransportista', action:'create', params:'\'cliente=\'+ this.value', update: 'body')}" 
	value = "${repTransportistaInstance?.usuario?.cliente?.id ?: params.cliente}" />
	
</div>

<g:if test = "${cliente}">
	
	<div class ="fieldcontain">
		
		<label for = "usuario"> Usuario <span class = "required-indicator">*</span> </label>
	
		<g:select name = "usuario" from = "${usuarios}" optionKey = "id" required = "" noSelection = "['':'Seleccione Usuario']" value = "${repTransportistaInstance?.usuario?.id}" />
	
	</div>
	
	<div class ="fieldcontain">
		
		<label for = "transportista"> Transportista <span class = "required-indicator">*</span> </label>
	
		<g:select name = "transportista" from = "${transportistas}" optionKey = "id" required = "" noSelection = "['':'Seleccione Transportista']" value = "${repTransportistaInstance?.transportista?.id}" />
	
	</div>
	
</g:if>