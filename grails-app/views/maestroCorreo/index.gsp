<%@ page import = "Maestros.MaestroCorreo" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Correos </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Correo </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1 align = "center"> Listado de Correos </h1> <br/>
			
			<g:form action = "index">
				
				<table style = "margin-left: 25%; width:50%">
				
					<thead> <tr> <g:sortableColumn property = "id" title = "Id"/> <g:sortableColumn property = "correo" title = "Correo" />  <th> Activo </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass"> 
							
							<td> </td> <td> <input type = "text" name = "correo" value = "${params.correo}"> </td>
							
							<td> <g:select name = "activo" from = "${['Si', 'No']}" value = "${params.activo}" noSelection = "['':'Todos']" /> </td> 
						
						</tr>
								
						<g:each in = "${correos}" status = "i" var = "correo">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}"> 	
								
								<td> <g:link action = "show" id = "${correo.id}"> ${correo.id} </g:link> <td> ${correo.correo} </td>
								
								<td> <g:formatBoolean boolean = "${correo.activo}" true = "Si" false = "No" /> </td>
						
							</tr>
							
						</g:each>
						
					</tbody>
					
				</table>
							
				<div class = "paginations" style = "margin-left: 25%; width: 49.5%"> 
					
					<g:submitButton name = "Buscar" class = "botonBuscador"/> <g:paginate total = "${correos.totalCount ?: 0}" /> 
				
				</div>
			
			</g:form>
			
		</div>
		
	</body>
	
</html>