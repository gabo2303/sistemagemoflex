<%@ page import = "Maestros.MaestroCorreo" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Mostrando Correo </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Correos </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Correo </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Correo </h1>
			
			<ol class = "property-list maestroCorreo">
			
				<li class = "fieldcontain"> <span class = "property-label"> Correo </span> <span class = "property-value"> ${correo.correo} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Activo </span> <span class = "property-value"> <g:formatBoolean boolean = "${correo.activo}" false = "No" true = "Si" /> </span> </li>
				
			</ol>
			
			<g:form resource = "${correo}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class =  "edit" action = "edit" resource = "${correo}"> Editar </g:link>
					
					<g:if test = "${correo.activo}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar el Correo?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<div hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </div>
					
					</g:if>
					
					<g:else>
						
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
						
						<script>
							
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar el Correo?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
						
						</script>
						
						<div hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </div>
					
					</g:else>
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>