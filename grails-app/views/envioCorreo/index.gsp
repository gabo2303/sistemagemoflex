<%@ page import="transaccionales.EnvioCorreo" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Envío de Correo </title> </head>
	
	<body>
	
		<div id = "list-envioCorreo" class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if> 
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif> 
			
			<h1> Registro de Correos Enviados </h1> <br/>
			
			<table style = "margin-left: 2.5%; width:95%" class = "buttons"> 
			
				<tbody>  	 
  		  			
  		  			<g:form action = "index">
  		  				
						<tr class = "noclass">
		  	  											
							<td> Fecha Inicio: </td> 
							
							<td> <g:datePicker name = "fechaInicio" value = "${new Date()}" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR]}"/> </td>
							
							<td> Fecha Final: </td> 
							
							<td> <g:datePicker name = "fechaFinal" value = "${new Date()}" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR]}"/> </td>
							
							<td> <input id = "env" class = "botonGuardarPersonalizado" type = "submit" value = "Enviar"> </td>
							
						</tr>
						
					</g:form>
				
				</tbody>
				
			</table>
			
			<div class = "pagination"> <g:paginate total="${envioCorreoInstanceCount ?: 0}" /> </div>
			
		</div>
		
	</body>
	
</html>