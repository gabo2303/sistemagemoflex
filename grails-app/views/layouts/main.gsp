<!DOCTYPE html>
<%@page import = "java.lang.Object"%>

<html class = "no-js">

	<head>
				
		<asset:stylesheet src = "bootstrap.3.3.7.css"/> 	<asset:stylesheet src = "application.css"/>		
  		<asset:javascript src = "application.js"/>			<asset:javascript src = "bootstrap.min.js"/>	
  		<asset:javascript src = "bootbox.min.js"/>
  		
		<meta http-equiv = "Content-Type" content = "text/html; charset = ISO-8859-1">
		<meta charset = "ISO-8859-1">
		<meta http-equiv = "X-UA-Compatible" content = "IE=edge,chrome=1">
		<meta name = "viewport" content = "width=device-width, initial-scale=1.0">
		
		<title> <g:layoutTitle default = "Sistema de Inspecciones "/> </title>
				
		<link rel = "shortcut icon" href = "${resource(dir: 'images', file: 'upperGemoLogo.png')}"	type = "image/x-icon">
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/mobile.css" type = "text/css" media = "handheld , only screen and (max-width: 480px)"/>
			
		<g:layoutHead /> <tooltip:resources/> <asset:stylesheet src = "bootstrap.css"/>	
		
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/main.css"	type = "text/css" media = "screen and (min-width: 481px)">
		
	</head>
	 
	<body>
		
		<div id = body>
			
			<img id = "imgCabecera" src = "${resource(dir: 'images', file: 'cabecera.png')}" alt = "Sistema Gemoflex"/>	 
	
			<div id = "cssmenu">
			
				<ul>
				
					<li> <a href = "/SistemaGemoflex"> Inicio </a> </li>
			
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR">		
					
						<li class = 'active' style = "postition: relative; z-index: 9999 !important;"> 
							
							<a href = "#"> Administrar Flota </a>
				
							<ul>						
								<li> <a href = "/SistemaGemoflex/maestroRegion"> Administrar Región </a> </li>				
								<li> <a href = "/SistemaGemoflex/maestroPlanta"> Administrar Planta / Localidad </a> </li>
								<li> <a href = "/SistemaGemoflex/estacionServicioRedTae"> Administrar Estación de Servicio </a> </li>
								<li> <a href = "/SistemaGemoflex/maestroTransportista/index"> Administrar Transportista </a> </li>				
								<li> <a href = "/SistemaGemoflex/maestroCamion/index"> Administrar Camión </a> </li>								
							</ul>
							
						</li>
					
					</sec:ifAnyGranted>
				
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR">		
						
						<li class = 'active' style = "postition: relative; z-index: 9999 !important;"> <a href = "#"> Administrar Cliente </a>		
								
							<ul>							
								<li> <g:link controller = "usuario" action = "index"> Administrar Usuario </g:link> </li>
								<li> <g:link controller = "usuarioPermiso" action = "index"> Administrar Usuario Permiso </g:link> </li>
								<li> <g:link controller = "maestroCliente" action = "index"> Administrar Cliente </g:link> </li>				
								<li> <g:link controller = "maestroTipoInspeccion" action = "index"> Administrar Tipo De Inspección </g:link> </li>				
								<li> <g:link controller = "maestroClasificacion" action = "index"> Administrar Clasificación </g:link> </li>
								<li> <g:link controller = "checkList" action = "index"> Creación de Preguntas </g:link> </li>
								<li> <g:link controller = "maestroCorreo" action = "index"> Administrar Correo </g:link> </li>						
							</ul>
							
						</li>
					
					</sec:ifAnyGranted>
					
					<sec:ifAnyGranted roles = "ROLE_ADMINISTRADOR">
						
						<li class = 'active'> <a href = "#"> Administrar Cliente </a>		
								
							<ul> <li> <a href = "/SistemaGemoflex/maestroCorreo"> Administrar Correo </a> </li> </ul>
							
						</li>	
					
					</sec:ifAnyGranted>
					
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR">				
						
						<li class = 'active' style = "postition:relative; z-index:9999 !important;"> <a href = "#"> Asociaciones </a>
							
							<ul>						
								<li> <g:link controller = "usuarioPlanta" action = "index"> Asociar Planta Usuario  </g:link> </li>
								<li> <g:link controller = "repTransportista" action = "index"> Asociar Transportista Usuario </g:link> </li>
								<li> <g:link controller = "transportistaPlanta" action = "index"> Asociar Transportista Planta/Localidad </g:link> </li>				
								<li> <g:link controller = "correoCliente" action = "index"> Asociar Correo Cliente </g:link> </li>
								<li> <g:link controller = "correoTransportista" action = "index"> Asociar Correo Transportista </g:link> </li>													
							</ul>
							
						</li>
					
					</sec:ifAnyGranted>
	
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_INSPECTOR">
						
						<li class = 'active'> <a href = "#"> Inspección </a> 
													
							<ul>
								<li> <g:link controller = "inspeccion" action = "index"> Ingresar Inspección </g:link> </li>
								<li> <g:link controller = "inspeccion" action = "verInspeccion"> Listado de Inspecciones </g:link> </li>												
							</ul>
							
						</li>
					
					</sec:ifAnyGranted>		
	
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_CLIENTE">
						
						<li class = 'active'> <a href = "#"> Cliente </a>
				
							<ul>			
								<li> <g:link controller = "ClienteConsulta" action = "index" params = "[cliente:'cliente']"> Consulta Histórica </g:link> </li>
								<li> <a href = "/SistemaGemoflex/Consultas/consultarPorItem"> Consulta Por Item Malo </a> </li>
								
								<g:if test = "${seguridad.Usuario.findByUsername(sec.username()).cliente.razonSocial != 'Lipigas'}">
									
									<li> <g:link controller = "consultaCriticos" action = "consulta"> Consulta Malos / Críticos </g:link> </li>
																	
								</g:if>
								
								<g:if test = "${seguridad.Usuario.findByUsername(sec.username()).cliente.razonSocial == 'Copec'}"> 
									
									<li> <g:link controller = "consultas" action = "consultaReiterativos"> Consulta Malos Reiterativos </g:link> </li>									
									<li> <a href = "/SistemaGemoflex/Consultas"> Consulta Red Tae </a> </li> 
								
								</g:if>
								
								<li> <a href = "/SistemaGemoflex/Consultas/resumenInspeccion"> Resumen de Inspecciones </a> </li>
								<li> <g:link controller = "clienteConsulta" action = "ultimaInspeccion" params = "[cliente:'cliente']"> Última Inspección </g:link> </li>				
								
								<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR"> 
								
									<li> <g:link controller = "envioCorreo" action = "index"> Consulta Envío de Correos </g:link> </li>
									
								</sec:ifAnyGranted>
							</ul>		
							
						</li>
			
					</sec:ifAnyGranted>		
					
					<sec:ifAnyGranted roles = "ROLE_INSPECTOR"> 
					
						<li> <g:link controller = "ClienteConsulta" action = "index"> Consulta Histórica </g:link> </li>
						
						<g:if test = "${seguridad.Usuario.findByUsername(sec.username()).cliente.razonSocial == 'Copec'}">
						
							<li> <g:link controller = "consultas" action = "consultaReiterativos"> Consulta Malos Reiterativos </g:link> </li>
						
						</g:if> 
					
					</sec:ifAnyGranted>
					
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_TRANSPORTISTA">
			
						<li class = 'active'> <a href = "#"> Transportista </a>
				
							<ul>						
								<li> <g:link controller = "ClienteConsulta" action = "index"> Consulta Histórica </g:link> </li>
								<li> <a href = "/SistemaGemoflex/transportistaConsulta/transportistaConsultaItem"> Consulta Por Item Malo </a> </li>
								
								<g:if test = "${seguridad.Usuario.findByUsername(sec.username()).cliente.razonSocial != 'Lipigas'}">
									
									<li> <a href = "/SistemaGemoflex/consultaCriticos/consulta"> Consulta Malos / Críticos </a> </li>
								
								</g:if>
								
								<li> <a href = "/SistemaGemoflex/transportistaConsulta/resumenInspeccion"> Resumen de Inspecciones </a> </li>								
								<li> <g:link controller = "clienteConsulta" action = "ultimaInspeccion"> Última Inspección </g:link> </li>												
							</ul>	
						</li>
			
					</sec:ifAnyGranted>
					
					<li>  
						<sec:ifLoggedIn> <g:link controller = 'logout'> Salir del Sistema </g:link> </sec:ifLoggedIn>			
				
						<sec:ifNotLoggedIn> 
	
							<g:link controller = "login" action = "auth"> Ingresar al Sistema </g:link> 
						
						</sec:ifNotLoggedIn>			
					</li>
			
					<sec:ifLoggedIn> <li class = 'active'> <a> Conectado: <sec:username/> </a> </li> </sec:ifLoggedIn>
			
				</ul>	
			
			</div>		
					
			<g:layoutBody/>
			
			<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			
			<div class = "footer" role = "contentinfo"> </div>
		
			<div id = "footer"> <g:render template = "/common/footer"/> </div>
		
		</div>
		
	</body>
	
</html>