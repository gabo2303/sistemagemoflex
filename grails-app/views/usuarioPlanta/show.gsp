<%@ page import = "Maestros.UsuarioPlanta" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Usuario Planta </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Asociaciones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Asociación </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-usuarioPlanta" class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Asociación </h1> <br/>
			
			<ol class = "property-list usuarioPlanta">
			
				<li class = "fieldcontain">
					
					<span id = "planta-label" class = "property-label"> Planta </span>
				
					<span class = "property-value"> ${usuarioPlantaInstance?.planta?.encodeAsHTML()} </span>
				
				</li>
				
				<li class = "fieldcontain">
					
					<span id = "usuario-label" class = "property-label"> Usuario </span>
				
					<span class = "property-value"> ${usuarioPlantaInstance?.usuario?.encodeAsHTML()} </span>
				
				</li>
			
			</ol>
			
			<g:form url = "[resource:usuarioPlantaInstance, action:'delete']" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${usuarioPlantaInstance}"> Editar </g:link>
					
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('¿Esta seguro?');" />
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>