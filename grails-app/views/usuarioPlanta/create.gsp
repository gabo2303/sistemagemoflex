<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Usuario Planta </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "list" action = "index"> Listado de Asociaciones </g:link> </li> </ul> </div>
		
		<div id = "create-usuarioPlanta" class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1> Crear Usuario Planta </h1> <br/>
			
			<g:form url = "[resource:usuarioPlantaInstance, action:'save']" >
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>

				<fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Crear" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>