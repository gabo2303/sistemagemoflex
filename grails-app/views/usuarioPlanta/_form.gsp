<%@ page import = "Maestros.UsuarioPlanta" %>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label>
	
	<g:if test = "${!usuarioPlantaInstance.usuario}"> 
		
		<g:select name = "cliente.id" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" required = "" noSelection = "['':'Seleccione']" 
		onchange = "${remoteFunction(controller:'usuarioPlanta', action:'create', params:'\'cliente=\'+ this.value', update: 'body')}" value = "${usuarioPlantaInstance?.usuario?.id ?: params.cliente}"/>
	
	</g:if>
	
	<g:else> ${usuarioPlantaInstance.usuario.cliente.razonSocial} </g:else>
	
</div>

<g:if test = "${usuarioPlantaInstance.planta || plantas}"> 
	
	<div class = "fieldcontain">
	
		<label for = "planta"> Planta <span class = "required-indicator">*</span> </label>
	
		<g:select name = "planta.id" from = "${plantas.sort { it.descripcion }}" optionKey = "id" value = "${usuarioPlantaInstance.planta}"/>
	
	</div>
	
</g:if>
	
<g:if test = "${usuarioPlantaInstance.usuario || usuarios}">	
	
	<div class = "fieldcontain">
	
		<label for = "usuarios"> Usuario <span class = "required-indicator">*</span> </label>
	
		<g:select name = "usuario.id" from = "${usuarios.sort { it.username }}" optionKey = "id" value = "${usuarioPlantaInstance.usuario}"/>
	
	</div>
	
</g:if>