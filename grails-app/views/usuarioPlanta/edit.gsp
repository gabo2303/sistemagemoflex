<%@page import = "Maestros.MaestroPlanta"%>
<%@ page import = "Maestros.UsuarioPlanta" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Usuario Planta </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Asociaciones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Asociación </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "edit-usuarioPlanta" class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editar Usuario Planta </h1> <br/>
			
			<g:form url = "[resource:usuarioPlantaInstance, action:'update']" method = "PUT" >
					
				<g:hiddenField name = "version" value = "${usuarioPlantaInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form" /> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>