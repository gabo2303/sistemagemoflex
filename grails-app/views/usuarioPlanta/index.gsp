<%@ page import = "Maestros.UsuarioPlanta" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Asociaciones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Asociación </g:link> </li> </ul> </div>
		
		<div id = "list-usuarioPlanta" class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1 align = "center"> Listado de Asociaciones </h1> <br/>
			
			<g:form action = 'index'>
			
				<table style = "margin-left: 25%; width: 50%">
					
					<thead> <tr> <th> Id </th> <th> Planta </th> <th> Usuario </th> </tr> </thead> 
					
					<tbody>
						
						<tr class = "noclass"> 
							
							<td> </td>
							<td> <g:select name = "planta" from = "${Maestros.MaestroPlanta.list().sort { it.descripcion }}" noSelection = "['':'Seleccione Planta']" value = "${params.planta?.toInteger()}" optionKey = "id"/> </td>
							<td> <g:select name = "usuario" from = "${seguridad.Usuario.list().sort { it.username }}" noSelection = "['':'Seleccione Usuario']" value = "${params.usuario?.toInteger()}" optionKey = "id"/> </td> 
						
						</tr>
						
						<g:each in = "${usuariosPlantas}" status = "i" var = "usuarioPlanta">
							
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> <g:link action = "show" id = "${usuarioPlanta.id}"> ${usuarioPlanta.id} </g:link> </td>
						
								<td> ${usuarioPlanta.planta} </td> <td> ${usuarioPlanta.usuario} </td>
						
							</tr>
							
						</g:each>
						
					</tbody>
				
				</table>
				
				<div class = "paginations" style = "margin-left: 25%; width: 49.5%"> 
					
					<g:submitButton name = "Buscar" class = "botonBuscador"/> <g:paginate total = "${usuariosPlantas.totalCount ?: 0}" /> 
				
				</div>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>