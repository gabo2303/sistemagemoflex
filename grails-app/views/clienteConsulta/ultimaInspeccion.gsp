<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head> <meta name = "layout" content = "main"/> <link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/> <title> Consulta Última Inspección </title> </head>

	<body>
  
  		<div class = "content scaffold-list">
  			
  			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "main"> ${flash.message} </div> </g:if> 
  			<g:if test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:if>
  			
  			<h1> Consulta Última Inspección </h1> 
  			
  			<g:form action = "ultimaInspeccion">
  				
  				<g:hiddenField name = "cliente" value = "${params.cliente}" />
  				
		  		<table style = "margin-left:1.5%">
		  				
		  			<thead> <tr> <th> Número </th> <th> Planta </th> <th> Transportista </th> <th> Tipo Inspección </th> <th> Ultima Inspección </th> <th> Filtrar/Ver </th> </tr> </thead>
		  	  			
		  	  		<tbody>
					 	
					 	<tr class = "noclass">
					 						 		
					 		<td> <input name = "numero" value = "${params.numero}" > </td>
					 		<td> <g:select name = "planta" from = "${plantas.sort { it.descripcion }}" value = "${params.planta ? params.planta.toInteger() : ''}" noSelection = "['':'Todas']" 
					 		optionKey = "id" optionValue = "descripcion" /> </td>
					 		<td> <g:select name = "transportista" from = "${transportistas.sort { it.descripcion }}" value = "${params.transportista}" noSelection = "['':'Todos']" optionKey = "id" optionValue = "descripcion" /> </td>
					 		<td> <g:select name = "tipoInspeccion" from = "${usuario.cliente.tipoInspeccion.sort { it.descripcion }}" value = "${params.tipoInspeccion}" noSelection = "['':'Todas']" 
					 		optionKey = "id" optionValue = "descripcion" /> </td>
					 		<td> </td> <td> <g:submitButton name = "Filtrar" class = "botonBuscador" /> </td>
					 		
					 	</tr>
					 	
					 	<g:each var = "inspeccion" in = "${inspecciones}" status = "i">
							
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}"> 
											
								<td> ${inspeccion[0]} </td> <td> ${inspeccion[2]} </td> <td> ${inspeccion[3]} </td> <td> ${Maestros.MaestroTipoInspeccion.get(inspeccion[4]).descripcion.drop(15)} </td> 
							
								<td> <g:formatDate date = "${inspeccion[1]}" format = "dd-MM-yyyy" /> </td>
								
								<td>
									<g:link target = "_blank" controller = "inspeccion" params = "[tipoInspeccion:inspeccion[4], id:inspeccion[5]]" action = "show"> 
									
										<img src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" alt = "Grails"/> 
									
									</g:link>
								</td>	
								
							</tr>	
						
						</g:each>
													
					</tbody>
					
				</table>
			
			</g:form>	
			
			<g:if test = "${inspeccionesTotales > 50}"> <div class = "paginations"> <g:paginate total = "${inspeccionesTotales ?: 0}" params = "${params}" /> </div> </g:if>
			
		</div>    
    	   	    	
	</body>
	
</html>