<html>
	
	<head>
	
		<meta name = "layout" content = "main">
		
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css">
		
		<title> Contador Mensual </title>
	
	</head>
	
	<body>
		
		<br/> 
		
		<g:formRemote name = "formContadorMensual" url = "[controller:'clienteConsulta', action:'resultadoContadorMensual']" update = "resultado">
		
			<table class = "table-form">
							
				<thead> <tr> <th colspan = "4"> Contador Mensual </th> </tr> </thead>
				
				<tbody> 
					
					<tr class = "noclass"> 
						
						<td width = "120px"> Tipo Inspección: </td> 
						<td> <g:select name = "tipoInspeccion" id = "tipoInspeccion" from = "${tipoInspeccion}"/> </td> 
										
						<td> Mes: </td> 
						<td> <g:datePicker name = "fecha" id = "fecha" precision = "month"/> </td> 
					
					</tr>
					
					<tr class = "noclass"> 
						
						<td> Transportistas: </td> 
						<td> 
							<g:select name = "transportista" id = "transportista" noSelection = "['':'Todos los Transportistas']"
							from = "${Maestros.MaestroTransportista.findAllByCliente(cliente).descripcion}"/> 
						</td> 
					
						<td> Plantas: </td> 
						<td> 
							<g:select name = "plantas" id = "plantas" noSelection = "['':'Todas las Plantas']"
							from = "${Maestros.MaestroPlanta.findAllByCliente(cliente).descripcion}"/> 
						</td> 
					
					</tr>
					
				</tbody>
					
			</table>	
			
			<br/>
			
			<div id = "resultado"> </div>
			
			<fieldset class = "buttons" style = "margin-left: 45%; width: 90px;">
							
				<g:submitButton class = "save" name = "Consultar"/>
					
			</fieldset>
			
		</g:formRemote>
		
	</body>
	
</html>