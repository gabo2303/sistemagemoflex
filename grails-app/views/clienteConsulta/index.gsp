<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head> <meta name = "layout" content = "main"/> <link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/> <title> Consulta Por Tanque </title> </head>

	<body>
  
  		<div class = "content scaffold-list">
  			
  			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "main"> ${flash.message} </div> </g:if>
  			<g:if test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:if> 
  			
  			<h1> Consulta Histórica por Tanque </h1> 	  

			<g:form action = "index">
				
				<g:hiddenField name = "cliente" value = "${params.cliente}" />
				
				<label style = "margin-left:2%"> Fecha Inicial: </label> 
				
				<g:datePicker name = "fechaInicial" noSelection = "['':'']" default = "none" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR] + 5}" value = "${params.fechaInicial}" /> 
				
				<label style = "margin-left:2%"> Fecha Final: </label> 
				
				<g:datePicker name = "fechaFinal" noSelection = "['':'']" default = "none" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR] + 5}" value = "${params.fechaFinal}" /> 
				
				<g:submitButton style = "margin-left:2%" name = "Filtrar" class = "botonBuscador "/> <br/> <br/>
					
					<div style = "overflow-x:auto; margin-left:1.5%; width:97%"> 
					
			  			<table style = "margin-left:0%">
			  				
			  				<thead> 
			  					
			  					<tr> 
			  						<th> Id / Ver </th> <th> Fecha Inspeccion </th> <th> Número Tanque </th> <th> Planta/Localidad </th> <th> Transportista </th> <th> Tipo de Inspección </th> <th> Total de Críticos / Filtrar </th> 
			  					</tr> 
			  				
			  				</thead>
			  	  			
			  	  			<tbody>
						 		
						 		<tr class = "noclass">		 			
						 			
						 			<td colspan = "2"> </td>
						 			<td> <input name = "numero" value = "${params.numero}"> </td>
						 			<td> <input name = "planta" value = "${params.planta}"> </td>
						 			<td> <g:select name = "transportista" from = "${transportistas.sort { it.descripcion }}" noSelection = "['':'Todos']" optionKey = "id" optionValue = "descripcion" 
						 			value = "${params.transportista}" /> </td>
						 			<td> <g:select name = "tipoInspeccion" from = "${usuario.cliente.tipoInspeccion.sort { it.descripcion }}" noSelection = "['':'Todos']" optionKey = "id" 
						 			optionValue = "descripcion" value = "${params.tipoInspeccion}" /> </td>
						 			<td> <g:submitButton class = "botonBuscador" name = "Filtrar"/> </td> 		 			
						 			
						 		</tr>
						 		
						 		<g:each in = "${inspecciones}" var = "inspeccion" status = "i">
							 		
							 		<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">  	  
				  	  			 
										<td> <tooltip:tip value = "Haga click para ver la Inspección"> 
											
											<g:link controller = "inspeccion"  action = "show" id = "${inspeccion.id}" params = "[tipoInspeccion:inspeccion.idInspeccion]" target = "_blank"> ${inspeccion.id} </g:link> 
										
										</tooltip:tip> </td> 
										
										<td> <g:formatDate date = "${inspeccion.fechaInspeccion}" format = "dd-MM-yyyy" /> </td> 
										
										<td> ${inspeccion.numero} </td> <td> ${inspeccion.planta} </td> <td> ${inspeccion.transportista} </td> 
										
										<td> ${Maestros.MaestroTipoInspeccion.get(inspeccion.idInspeccion).descripcion.drop(15)} </td> <td> ${inspeccion.totalCriticos} </td>							
									
									</tr>	
			  	  				
			  	  				</g:each>
			  	  				
							</tbody>
							
						</table>
						
					</div> <br/>
					
				<g:if test = "${inspecciones.totalCount > 50}"> <div class = "paginations"> <g:paginate total = "${inspecciones.totalCount ?: 0}" params = "${params}"/> </div> </g:if>
				
			</g:form>
		
		</div> 		 
    	
	</body>
	
</html>