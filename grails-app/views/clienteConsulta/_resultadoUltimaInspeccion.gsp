<div class = "body">
  			
	<br/>  
	
	<asset:javascript src = "filtro.js"/>	

	<table class = "table-form">
	
		<thead> 
		
			<tr>
				<td class = "trhead"> <label>Buscador de Datos</label> </td>
			</tr>
	
		</thead>
	
		<tbody>
		
			<tr class = "noclass">
				<td> <input name = "filter" onkeyup = "filter2(this, 'fechaUltimaInspeccion')" placeholder = "Ingrese dato a buscar"> </td>
			</tr>
	
		</tbody>
	
	</table>

	<br/>
	
	<table class = "table-form" id = "fechaUltimaInspeccion">
		
		<thead>
			
			<tr>
				<td class = "trhead">Número</th>
				<td class = "trhead">Ultima Inspección</th>
				<td class = "trhead">Planta</th>
			</tr>
			
		</thead>
		
		<tbody>
		
			<g:each in = "${numeroFecha}" var = "val">
			
				<tr style = "border-color: gray;">
					<td>${val[0]}</td>
					<td><g:formatDate format = "dd-MM-yyyy" date = "${val[1]}"/></td>
					<td>${val[2]}</td>
				</tr>
			
			</g:each>
			
		</tbody>
		
	</table>
			
</div>