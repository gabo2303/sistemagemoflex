<div>

	<input id = "numeroInspeccion" value = "${numeroInspeccion}" name = "numeroInspeccion" hidden = "">
	
	<table class = "table-form">
		
		<tr class = "noclass">  	  			 
			<td width = "150px"> Transportista: </td>
					
			<td> 
				<g:select id = "tipoInspeccionId" name = "nInspeccion" from = "${transportistasPrevios}" required = ""
				noSelection = "['':'Seleccione un Transportista']" 
				onchange = "${remoteFunction(controller:'clienteConsulta', action:'resultadoUltimaInspeccion', 
					                         params:'\'transportista=\'+ this.value + \'&numeroInspeccion=\' + numeroInspeccion.value', update: 'resultado')}"	 			
				class = "many-to-one"/>                      			  	                       
			</td>				
		</tr>
		
	</table>
	
</div>

<div id = "resultado"> </div>