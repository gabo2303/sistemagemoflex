<%@ page import = "Maestros.TransportistaPlanta" %>

<div class = "fieldcontain">
	
	<label for = "planta"> Planta <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" name = "planta" from = "${Maestros.MaestroPlanta.list(sort:'descripcion', order:'asc')}" 
	optionKey = "id" required = "" value = "${transportistaPlantaInstance?.planta?.id ?: params.planta ? params.planta.toInteger() : null}" />

</div>

<div class = "fieldcontain">

	<label for = "transportista"> Transportista <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "razonSocial" name = "transportista" from = "${Maestros.MaestroTransportista.list(sort:'razonSocial',order:'asc')}" 
	optionKey = "id" required = "" value = "${transportistaPlantaInstance?.transportista?.id ?: params.transportista}" />

</div>