<%@ page import = "Maestros.TransportistaPlanta" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Mostrando Transportista-Planta </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class = "list" action = "index"> Listado Transportista Planta </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Transportista Planta </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Asociación Transportista-Planta </h1>
			
			<ol class = "property-list transportistaPlanta">
			
				<li class = "fieldcontain">
					
					<span class = "property-label"> Planta </span> <span class = "property-value"> ${asociacion?.planta} </span>
					
				</li>
			
				<li class = "fieldcontain">
				
					<span class = "property-label"> Transportista </span> <span class = "property-value"> ${asociacion?.transportista.razonSocial} </span>
					
				</li>
			
			</ol>
			
			<g:form resource = "${asociacion}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${asociacion}"> Editar </g:link>
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('¿Esta usted seguro?');" />
				
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>