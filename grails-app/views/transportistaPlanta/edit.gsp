<%@ page import = "Maestros.TransportistaPlanta" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Transportista Planta </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class="list" action="index">Listado Transportista Planta</g:link></li>
				<li> <g:link class="create" action="create">Nuevo Transportista Planta</g:link></li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editar Transportista Planta </h1>
			
			<g:form resource = "${transportistaPlantaInstance}" action = "update" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${transportistaPlantaInstance?.version}" />
				<g:hiddenField name = "asociacion" value = "${transportistaPlantaInstance?.id}" />
				
				<fieldset class = "form"> <g:render template="form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>