<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head> <meta name = "layout" content = "main"/> <link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/> <title> Consulta Por Fallas </title> </head>

	<body>
  
  		<div class = "body">
  			 			
  			<g:set value = "${seguridad.Usuario.findByUsername(sec.username())}" var = "usuario" /> <br/>
  			
  			<g:if test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:if> 
  			 
  			<g:form action = "listadoPorItems">
		  	
		  		<table class = "table-form" style = "width:95%; margin-left:2.5%">
			  	
			  		<thead> <tr> <th colspan = "${usuario.cliente.tipoInspeccion.size() * 2 + 1}"> Consulta Histórica por Item Cliente ${cliente} </th> </tr> </thead>
  	  			  	  			
	  	  			<tbody>
				 	
				 		<tr class = "noclass">
				 		  	  
	  	  			 		<td width = "105px"> Tipo Inspección: </td>
							
							<g:each in = "${usuario.cliente.tipoInspeccion.sort { it.id } -  Maestros.MaestroTipoInspeccion.get(13)}" status = "i" var = "inspeccion">
								
								<td> ${inspeccion.descripcion.drop(15)} </td>
								
								<g:if test = "${inspecciones?.contains(inspeccion.id)}"> <td> <g:checkBox name = "${inspeccion.descripcion.drop(15)}" value = "${true}"/> </td> </g:if> 
								<g:else> <td> <g:checkBox name = "${inspeccion.descripcion.drop(15)}" /> </td> </g:else> 
								
							</g:each>
							
						</tr>
	  	  					  	  				
					</tbody>
				
				</table>
				
				<div align = "center" class = "table-form container" style = "width:95%; margin-left:2.5%; padding-top:0.5%; padding-bottom:0.5%"> 					
				 
	 	  			<span style = "margin-right: 5%"> Fecha Inicio: <g:datePicker name = "nfecha1" value = "${new Date()}" precision = "day" years = "${2015..new Date()[Calendar.YEAR]}" value = "${fecha1}"/> </span>    
			  	  	
			  	  	<span style = "margin-right: 5%"> Fecha Fin: <g:datePicker name = "nfecha2" value = "${new Date()}" precision = "day" years = "${2015..new Date()[Calendar.YEAR]}" value = "${fecha2}"/> </span>  
					
					<span> <input type = "submit" value = "Consultar" class = "botonGuardarPersonalizado"> </span> 
	 				
				</div>
			
			</g:form>
			
  		</div> <br/>
  				
		<g:if test = "${inspecciones}">		
			
			<g:form action = "listadoPorItems">
				
				<g:hiddenField name = "inspecciones" value = "${inspecciones}" /> 

				<div hidden = ""> <g:datePicker name = "nfecha1" precision = "day" value = "${fecha1}"/> </div>
				<div hidden = ""> <g:datePicker name = "nfecha2" precision = "day" value = "${fecha2}"/> </div>
				
				<table class = "table-form" style = "width:95%; margin-left:2.5%">
				  	
				  	<thead>
					  	
					  	<tr> <th> Fecha Inspección </th> <th> Item </th> <th width = "150"> Observación </th> <th> Planta/Localidad </th> <th> Número Tanque </th> 
					  	<th> Transportista </th> <th> Evaluación </th> <th> Ver / Filtrar </th> </tr>
					  	
					</thead>	
				
					<tbody>
						
						<tr bgcolor = "#CBC7C7">
						 
							<td> </td> <td> <input name = "item" value = "${params.item}"/> </td> <td> </td>
							<td> <g:select name = "planta" from = "${plantas.unique()}" value = "${params.planta}" noSelection = "['':'Todos']"/> </td>
							<td> <g:select name = "tanque" from = "${tanques.unique()}" value = "${params.tanque}" noSelection = "['':'Todos']"/> </td>
							<td> <input name = "transportista" value = "${params.transportista}"/> </td>
							<td> <g:select name = "critico" from = "${['Todos', 'Critico', 'Malo']}" value = "${params.critico ?: 'Critico'}" /> </td>
							<td> <input id = "filtrar" type = "submit" value = "Filtrar" class = "botonBuscador"> </td>
							
						</tr>
								
						<g:each in = "${listadoItemsPorFalla}" status = "i" var = "var" >
							
							<tr>		
								<td> <g:formatDate format = "dd-MM-yyyy" date = "${var[0]}"/> </td>
								
								<td> ${var[1]} </td> <td> ${var[2]} </td> <td> ${var[3]} </td> <td> ${var[4]} </td> <td> ${var[5]} </td>
								
								<td> <g:if test = "${var[6] == 0}"> Malo </g:if> <g:else> Crítico </g:else> </td>
								
								<td>
									<g:link target = "_blank" controller = "inspeccion" params = "[tipoInspeccion:var[7], id:var[9]]" action = "show">
										
										<img src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" alt = "Grails"/>	
									
									</g:link>
								</td>							
							</tr>
												 														
						</g:each>	
					
					</tbody>  		
					  		
				</table> <br/>
				
				<div class = "paginations"> 
					
					<g:paginate total = "${listadoItemsPorFallaTotal?.size ?: 0}" params = "${params}" />
					<g:select id = "paginacion" name = "paginacion" from = "${['5', '10', '20', '30', '40', '50']}" value = "${params.max}" onchange = "cambiarPaginacion()" noSelection = "['':'Registros por Página']" />
					
					<script> function cambiarPaginacion() { $(document).ready(function(){$('#filtrar').trigger('click'); }); } </script>
					 
				</div> 
	    	
	    	</g:form>
	    	
	    </g:if>
	    	
	</body>

</html>