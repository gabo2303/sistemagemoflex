<table class = "table-form" id = "xls" style = "width: 20%; margin-left: 40%">		 
				
	<tbody>
  	  		 		
  		<tr class = "noclass">
  				 			
	  		<td colspan = "2" style = "text-align: center">
	  			Exportar Datos  			
		  		<!-- Tag de jasper que sirve para realizar la exportacion de datos a CSV -->
		  		<g:jasperReport controller = "consultas" id = "reporteCSV" name = "Detalle_Inspecciones" jasper = "Modelo XLS.jrxml" 
		  		format = "CSV" action = "reporteCsv" bodyBefore = " " buttonPosition = "below" delimiter = " " description = " ">
		  			
		  			<input hidden = "" name = "plantaString" value = "${params.plantaString}"/>	
		  			<input hidden = "" name = "plantaSeleccionada" value = "${params.plantaSeleccionada}"/>			
					<input hidden = "" name = "fechaInicial" value = "${formatDate(date: params.fechaInicial, format: 'yyyy-MM-dd')}"/>
					<input hidden = "" name = "fechaFinal" value = "${formatDate(date: params.fechaFinal, format: 'yyyy-MM-dd')}"/>
									
				</g:jasperReport>
							
			</td>
		</tr>			 		 	
				
	</tbody>		  	
  			
</table>  