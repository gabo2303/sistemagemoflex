<input id = "inspeccionesTransportista" value = "${inspecciones}" hidden="">
<input id = "plantaSinCeroTransportista" value = "${plantaSinCero}" hidden = "">
<input id = "plantaSeleccionada" value = "${plantaSeleccionada}" hidden = "">
<input id = "transportistaSinCero" value = "${transportistaSinCero}" hidden = "">

<table class = "table-form">		 
				
	<tbody>				 
		
		<tr class = noclass>
		    
		    <td width = "150px"> Transportista: </td>
		               
		    <td> 
		      	<g:select name = "planta" id = "planta" from = "${transportista}" noSelection = "['':'Seleccione una Opción']" 
				onchange = "${remoteFunction(controller:'consultas', action:'resumenInspeccionFechas', 
				params:'\'transportista=\' + this.value + \'&inspeccionesTransportista=\' + inspeccionesTransportista.value + \'&plantaSeleccionada=\' + plantaSeleccionada.value + \'&plantaSinCero=\' + plantaSinCeroTransportista.value + \'&transportistaSinCero=\' + transportistaSinCero.value', 
				update:'fechas')}" required = ""/>  
			</td>
			
		</tr>		
				
	</tbody>		  	
  			
</table>   

<div id = fechas> </div>