<!-- Template que se muestra al seleccionar el tipo de inspección y muestra los campos que permiten el filtrado -->

<g:formRemote name = "frmConsultaCliente" url = "[controller:'consultas', action:'listadoPorItems']" update = "divCC">

	<table class = "table-form">
		
		<thead> <tr> <td class = "trhead" colspan = "2"> Seleccionador de Item </td> </tr> </thead>
		
		<tbody>
			
			<tr class = "noclass">  	  
  	  		
  	  			<td width = "150px"> Número Tanque: </td>
				
				<td> <g:select id = "ntanque" name = "tanque" from = "${ntanque}" optionKey = "" optionValue = "" noSelection = "['':'Todos los Tanques']"/> </td>	
  	  		
  	  		</tr>
  	  		
			<tr class = "noclass">  	  
  	  			<td>Items: </td>
				
				<td> 
					<g:select id = "items" name = "items" from = "${items}" optionKey = "" optionValue = "" noSelection = "['':'Todos los Items']"
					class = "many-to-one"/>                      			  	                       
				</td>	
				
				<td hidden = ""> <input type = "text" value = "${tInspeccion}" name = "nInspeccion"></td>		
			</tr>
			
			<tr class = "noclass">
		    	<td>Planta/Localidad:</td>
		        
		        <td> <g:select name = "planta" from = "${planta}" noSelection = "['':'Todas las plantas']" class = "many-to-one"/> </td>           
  	  		</tr>
  	  
  	  		<tr class = "noclass">
  	  			<td>Fecha Inicio:</td>
  	  			
  	  			<td> <g:datePicker name = "nfecha1" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>
			</tr>
  	  		
  	  		<tr class = "noclass">
  	  			<td>Fecha Fin:</td>
  	  			
  	  			<td> <g:datePicker name = "nfecha2" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>
  	  		</tr>
  	  		
  	  		<tr class = "noclass">
  	  			<td>Buscar</td>
  	  			
  	  			<td>
  	  				<!-- Input submit que envía los datos al controlador para poder realizar la consulta con los datos ingresados por el usuario --> 
  	  				<input type = "image" src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" class = firstbutton name = "btnBuscar" value = "Buscar">
  	  			</td>
			</tr>
  		
  		</tbody>
  	
  	</table>	  

</g:formRemote>  	

<div id = "divCC"> </div>