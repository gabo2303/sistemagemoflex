<asset:javascript src = "filtro.js"/>

<table class = "table-form" id = "listaInspecciones">
  	
  	<thead>
		
		<tr> <th colspan = "8"> ${consultapor} </th> </tr>
	  	
	  	<tr> <th> Fecha Inspección </th> <th> Item </th> <th> Observación </th> <th> Planta/Localidad </th> <th> Número Tanque </th> <th> Transportista </th> <th> Evaluación </th> <th> Ver </th> </tr>
	  	
	</thead>	
	
	<tbody>
		
		<g:each in = "${listadoItemsPorFalla}" status = "i" var = "var" >
			
			<tr>		
				<td> <g:formatDate format = "dd-MM-yyyy" date = "${var[0]}"/> </td>
				
				<td> ${var[1]} </td> <td> ${var[2]} </td> <td> ${var[3]} </td> <td> ${var[4]} </td> <td> ${var[5]} </td>
				
				<td>
					<g:if test = "${var[6] == 0}"> <label>	Malo </label> </g:if>
					
					<g:if test = "${var[6] == 1}"> <label>	Crítico </label> </g:if>
				</td>
				
				<td>
					<g:link target = "_blank" controller = "inspeccion" params = "[tipoInspeccion:var[7], id:var[9]]" action = "show">
						
						<img src = "${resource(dir: 'images/skin', file: 'mostrar.png')}" alt = "Grails"/>	
					
					</g:link>
				</td>							
			</tr>
								 														
		</g:each>	
	
	</tbody>  		
	  		
</table>

<div class = "pagination"> <g:paginate total = "${listadoItemsPorFallaCount}" params = "${params}" /> </div> 	