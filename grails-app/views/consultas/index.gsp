<%@page import = "Maestros.MaestroTipoInspeccion"%>

<html>
	
	<head> <meta name = "layout" content = "main"/> <link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/> <title> Exportar Detalle de Inspección </title> </head>
	
	<body> 		
  		
  		<g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if> <br/> <br/>
  			
  		<table class = "table-form" style = "width: 40%; margin-left: 30%;">
  	 			
  	 		<thead>	<tr> <td class = "trhead" colspan = "2" align = "center"> Exportación Detalle de Inspecciones </td> </tr> </thead>
  	  			
  	  		<tbody>
  	  				
  	  			<g:formRemote name = "consultaResumenRedTae" url = "[controller:'consultas', action:'consultaResumenRedTae']" update = "resultadoConsultaResumen">
  	  					
  	  				<g:hiddenField name = "plantasTotales" value = "${plantas}" />
  	  				<g:hiddenField name = "plantaString" value = "${plantaString}"/>	
  	  				
	  	  			<tr class = "noclass">
									  	  				
			           	<td width = "150px" style = "text-align:right"> <br/> Planta/Localidad: </td> 
			            
			            <td> <br/> <g:select name = "plantaSeleccionada" from = "${planta}" noSelection = "['':'Todas las Plantas']" /> </td>
			        </tr>
				  				  				
					<tr class = "noclass">
					
					  	<td style = "text-align:right"> Fecha Inicio: </td>	
					  					
					  	<td> <g:datePicker name = "fechaInicial" value = "${new Date()}" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR]}"/> </td>
					
					</tr>			
					
					<tr class = "noclass">
					 
						<td style = "text-align:right"> Fecha Fin: </td>
					  			
					  	<td> <g:datePicker name = "fechaFinal" value = "${new Date()}" precision = "day" years = "${new Date()[Calendar.YEAR] - 5..new Date()[Calendar.YEAR]}"/> </td> </tr>
						
						<tr class = "noclass"> <td colspan = "2" style = "text-align: center"> <br/> <g:submitButton name = "Enviar Datos"/> </td> </tr>											
										
					</g:formRemote>
					
				</tbody>		  	
  			
  			</table>  	
  			
  		<div id = "resultadoConsultaResumen"> </div> 	
		
	</body>

</html>