<g:formRemote url = "[controller:'consultas', action:'resumenInspeccionPreReporte']" name = "fechas" update = "reporte"> 
	
	<g:hiddenField name = "inspeccionesFecha" value = "${inspecciones}" />
	<g:hiddenField name = "plantaSeleccionadaFecha" value = "${plantaSeleccionada}" />
	<g:hiddenField name = "plantaSinCeroFecha" value = "${plantaSinCero}" />
	<g:hiddenField name = "transportistaSeleccionado" value = "${transportista}" />
	<g:hiddenField name = "transportistaSinCeroFecha" value = "${transportistaSinCero}" />
		
	<table class = "table-form" id = "xls">		 
					
		<tbody>					
				<tr class = "noclass">
				
					<td width = "160px">Fecha Inicio:</td>	
					  						
					<td> <g:datePicker  name = "fechaInicial" id = "fechaInicio" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>
					
				</tr>	  				
					  					
				<tr class = "noclass">
				
					<td> Fecha Fin: </td>
				
					<td> <g:datePicker name = "fechaFinal" id = "fechaFin" value = "${new Date()}" precision = "day" years = "${1900..2999}"/> </td>
				</tr>		
				
				<tr class = noclass>
					
					<td colspan = 2> <g:submitButton name = "Enviar Datos" class = "botonGuardarPersonalizado"/> </td>
				
				</tr>	
		</tbody>		  	
	  			
	</table>   
	
</g:formRemote>

<div id = "reporte"> </div>