<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_INSPECTOR, ROLE_CLIENTE">

	<table style = "width: 200px; margin-left: 40%" class = "table-form">
	
		<tr class = "noclass">	    
				
			<td style = "text-align: center">	 				
			
				Descargar Reporte
		
				<g:jasperReport jasper = "ResumenCabecera.jrxml" format = "XLS" name = "Resumen de Inspecciones.xls" action = "resumenInspeccionReporte"  controller = "consultas" 
				bodyBefore = " " buttonPosition = "top" delimiter = " " description = " ">
		            		
					<g:hiddenField name = "inspeccionesReporte" value = "${inspecciones}" /> <g:hiddenField name = "plantaSeleccionadaReporte" value = "${plantaSeleccionada}" />	
	            			
			        <g:hiddenField name = "plantaSinCeroReporte" value = "${plantaSinCero}" /> <g:hiddenField name = "transportistaSeleccionadoReporte" value = "${transportistaSeleccionado}" />
	           		
		           	<g:hiddenField name = "transportistaSinCeroReporte" value = "${transportistaSinCero}" /> 
		           	
		           	<g:hiddenField name = "fechaInicial" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaInicial)}" />
		           	
		           	<g:hiddenField name = "fechaFinal" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaFinal)}" />
		           				
		        </g:jasperReport>
							
			</td>
			
			<g:set var = "fechaInicial" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaInicial)}" />
			<g:set var = "fechaFinal" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaFinal)}" />
			
			<td style = "text-align: center">	 				
			
				<g:link controller = "consultas" action = "bajadaExcelResumenInspeccion" 
				params = "[inspeccionesReporte:inspecciones, plantaSeleccionadaReporte:plantaSeleccionada, plantaSinCeroReporte:plantaSinCero,
				transportistaSeleccionadoReporte:transportistaSeleccionado, transportistaSinCeroReporte:transportistaSinCero, fechaInicial:fechaInicial,
				fechaFinal:fechaFinal]"> Descargar Reporte (XLS) </g:link>
				
			</td>
							 		 	
		</tr>
			
	</table>
	
</sec:ifAnyGranted>

<sec:ifAnyGranted roles = "ROLE_ADMINISTRADOR, ROLE_SUPERVISOR">

	<table style = "width: 200px; margin-left: 40%" class = "table-form">
	
		<tr class = "noclass">	    
				
			<td style = "text-align: center">	 				
			
				Descargar Reporte
		
				<g:jasperReport jasper = "ResumenCabeceraCSV.jrxml" format = "CSV" name = "Resumen de Inspecciones.xls" action = "resumenInspeccionReporte"  controller = "consultas" 
				bodyBefore = " " buttonPosition = "top" delimiter = " " description = " ">
		            			
					<g:hiddenField name = "inspeccionesReporte" value = "${inspecciones}" />  <g:hiddenField name = "plantaSeleccionadaReporte" value = "${plantaSeleccionada}" />	
	            			
			        <g:hiddenField name = "plantaSinCeroReporte" value = "${plantaSinCero}" /> <g:hiddenField name = "transportistaSeleccionadoReporte" value = "${transportistaSeleccionado}" />
	           		
		           	<g:hiddenField name = "transportistaSinCeroReporte" value = "${transportistaSinCero}" />            					
	           			
		           	<g:hiddenField name = "fechaInicial" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaInicial)}" />
		           	
		           	<g:hiddenField name = "fechaFinal" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaFinal)}" />
		           				
		        </g:jasperReport>
							
			</td>
			
			<g:set var = "fechaInicial" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaInicial)}" />
			<g:set var = "fechaFinal" value = "${formatDate(format: 'yyyy-MM-dd', date: fechaFinal)}" />
			
			<td style = "text-align: center">	 				
			
				<g:link controller = "consultas" action = "bajadaExcelResumenInspeccion" 
				params = "[inspeccionesReporte:inspecciones, plantaSeleccionadaReporte:plantaSeleccionada, plantaSinCeroReporte:plantaSinCero,
				transportistaSeleccionadoReporte:transportistaSeleccionado, transportistaSinCeroReporte:transportistaSinCero, fechaInicial:fechaInicial,
				fechaFinal:fechaFinal]"> Descargar Reporte (XLS) </g:link>
				
			</td>
							 		 	
		</tr>
			
	</table>

</sec:ifAnyGranted>