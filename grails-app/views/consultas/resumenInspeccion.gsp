<%@ page contentType="text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/>
		<meta name = "layout" content = "main"/>
		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/>
		<title> Resumen Inspecciones </title>
		
	</head>
	
	<body>
  		
  		<input id = "usuarioId" value = "${usuarioId}" hidden = ""> <input id = "plantas" value = "${planta}" hidden = "">
  		
  		<div class = "body">
  	  		
  	  		<br/> <g:if test = "${flash.message}"> <div class = "message"> ${flash.message} </div> </g:if> 						
			<g:if test = "${flash.message2}"> <div class = "errors"> ${flash.message2} </div> </g:if>
			
			<g:if test = "${cliente.razonSocial == 'Copec'}"> <h1 style = "margin-left:2.5%"> Resumen de Inspecciones </h1> <br/> </g:if>
					
  			<table class = "table-form">
  					
  				<thead>
  						
  					<g:if test = "${cliente.razonSocial == 'Copec'}"> <tr> <td class = "trhead" colspan = 220"> Tipo Inspección </td> </tr> </g:if>
  					<g:else> <tr> <td class = "trhead" colspan = "2"> Resumen de Inspecciones </td> </tr> </g:else>
  					
  				</thead>
  	  			
  	  			<tbody>  	 
  	  				
			 		<tr class = "noclass">
  	  											
						<g:if test = "${cliente.razonSocial == 'Copec'}">					
							
							<g:formRemote name = "resumen" update = "planta" url = "[action:'resumenInspeccionPlanta', params:[usuarioId:usuarioId, multi:'multi']]">
							
								<td> Liviano </td> <td> <g:checkBox name = "liviano" /> </td>
								<td> Petróleo-Combustible </td> <td> <g:checkBox name = "petroleo" /> </td>
								<td> Gráfica </td> <td> <g:checkBox name = "grafica" /> </td>
								<td> Gráfica 2017 </td> <td> <g:checkBox name = "grafica2017" /> </td>
								<td> Gráfica Fuel </td> <td> <g:checkBox name = "graficaFuel" /> </td>
								<td> Gráfica Minero </td> <td> <g:checkBox name = "graficaMinero" /> </td>
								<td> Lubricante </td> <td> <g:checkBox name = "lubricante" /> </td>
								<td> Gráfica Lubricante </td> <td> <g:checkBox name = "graficaLubricante" /> </td>
								<td> RedTae </td> <td> <g:checkBox name = "redtae" /> </td>
								<td> CD Lubricante </td> <td> <g:checkBox name = "cdLubricante" /> </td>
								<td> Taller </td> <td> <g:checkBox name = "taller" /> </td>								
								<td> Aditivo Adblue </td> <td> <g:checkBox name = "aditivoAdblue" /> </td>
								<td hidden = ""> <input id = "envFinal" type = "submit" value = "Enviar"> </td>
								
							</g:formRemote>
							
						</g:if>
						
						<g:else>
							
  	  			 			<td width = "120px"> Tipo Inspección: </td>
							
							<td> 
								<g:select id = "tipoInspeccionId" name = "nInspeccion" from = "${tipoInspeccion}" required = "" noSelection = "['':'Seleccione Una Inspeccion']" 
								onclick = "ocultarDiv()" onchange = "${remoteFunction(controller:'consultas', action:'resumenInspeccionPlanta', 
								params:'\'id=\' + this.value + \'&usuarioId=\' + usuarioId.value', update:'planta')}"/>												                      			  	                      
							</td>
														
						</g:else>
							
					</tr>
  	  				 	  				
  	  			</tbody>
  	  						
			</table>
		 	
		 	<g:if test = "${flash.message}"> <br/> <input id = "env" style = "margin-left:45%;" class = "botonGuardarPersonalizado" type = "submit" value = "Enviar"> <br/> <br/> </g:if>
		 	
		 	<div id = "planta"> </div>
		 	  		
  		</div>  
  
  		<script type = "text/javascript">

			function ocultarDiv()
			{
				$('#mensaje').hide()
				$('#mensaje2').hide()
			}

			$(document).ready(function(){ $('#env').on('click', function(e){ $('#envFinal').trigger('click'); }); });
			
		</script>
	
	</body>
	
</html>