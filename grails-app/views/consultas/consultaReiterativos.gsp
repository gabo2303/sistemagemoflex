<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head> <meta name = "layout" content = "main"/> <link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"/> <title> Consulta Por Fallas </title> </head>

	<body>
  
  		<div class = "body">
  			 			
  			<g:set value = "${seguridad.Usuario.findByUsername(sec.username())}" var = "usuario" /> <br/>
  			
  			<g:if test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:if> 
  			 
  			<g:form action = "consultaReiterativos">
		  	
				<div align = "center" class = "table-form container" style = "width:95%; margin-left:2.5%; padding-top:0.5%; padding-bottom:0.5%"> 					
				 
	 	  			<span style = "margin-right: 5%"> Número Camión: <input name = "numero" value = "${params.numero}"> </span>    
			  	  	
			  	  	<span style = "margin-right: 5%"> Tipo de Inspección: 
			  	  	
			  	  	<g:select name = "tipoInspeccion" from = "${Maestros.MaestroTipoInspeccion.findAllByClienteAndActivo(usuario.cliente, true)}" optionValue = "descripcion" optionKey = "id" value = "${params.tipoInspeccion ?: 1}"/> </span>  
					
					<span> <input type = "submit" value = "Consultar" class = "botonGuardarPersonalizado"> </span> 
	 				
				</div>
			
			</g:form>
			
  		</div> <br/>
  				
		<g:if test = "${detalles}">		
			
			<g:form action = "consultaReiterativos">
				
				<table class = "table-form" style = "width:95%; margin-left:2.5%">
				  	
				  	<thead>
					  	
					  	<tr> <th> Número Tanque </th> <th> Fecha Inspección </th> <th> Item </th> <th> Planta/Localidad </th> <th> Repetición </th> </tr>
					  	
					</thead>	
				
					<tbody>
							
						<g:each in = "${detalles}" status = "i" var = "detalle" >
							
							<tr>										
								
								<td> ${detalle.idCabecera.numero} </td> <td> <g:formatDate format = "dd-MM-yyyy" date = "${detalle.idCabecera.fechaInspeccion}"/> </td> 
								
								<td> ${detalle.pregunta} </td> <td> ${detalle.idCabecera.planta} </td> <td> ${detalle.criticoRepeticion} </td>						
							</tr>
												 														
						</g:each>	
					
					</tbody>  		
					 
				</table> <br/>
				
				<g:if test = "${detalles.size > 10}"> <div class = "paginations"> <g:paginate total = "${detalles.size ?: 0}" params = "${params}" /> </div> </g:if> 
	    	
	    	</g:form>
	    	
	    </g:if>
	    	
	</body>

</html>