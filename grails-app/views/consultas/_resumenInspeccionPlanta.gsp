<input id = "inspecciones" value = "${inspecciones}" hidden = "">
<input id = "plantaSinCero" value = "${plantaSinCero}" hidden = ""> 

<table class = "table-form" id = "xls">		 
				
	<tbody>				 
		
		<tr class = noclass>
		    
		    <td width = "150px"> Planta/Localidad: </td>
		               
		    <td> 
		      	<g:select name = "planta" id = "planta" from = "${planta}" noSelection = "['':'Seleccione una Opción']" 
				onchange = "${remoteFunction(controller:'consultas', action:'resumenInspeccionTransportista', 
				params:'\'plantaSeleccionada=\' + this.value + \'&inspecciones=\' + inspecciones.value + \'&plantaSinCero=\' + plantaSinCero.value', 
				update:'transportista')}" required = ""/>  
			</td>
			
		</tr>
				
	</tbody>		  	
  			
</table>   

<div id = "transportista"> </div>