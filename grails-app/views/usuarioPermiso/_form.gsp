<%@ page import = "seguridad.UsuarioPermiso" %>

<div class = "fieldcontain required">

	<label for = "permiso"> Permiso <span class = "required-indicator">*</span> </label>
	
	<g:select name = "permiso.id" from = "${seguridad.Permiso.list()}" optionKey = "id" required = "" value = "${usuarioPermisoInstance?.permiso?.id}" />

</div>

<div class = "fieldcontain required">
	
	<label for = "usuario"> Usuario <span class = "required-indicator">*</span> </label>
	
	<g:select name = "usuario.id" from = "${seguridad.Usuario.list()}" optionKey = "id" required = "" value = "${usuarioPermisoInstance?.usuario?.id}" />

</div>