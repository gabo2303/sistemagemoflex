<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Crear Asociación </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "list" action = "index"> Listado de Asociaciones </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Creando Usuario Permiso </h1> 
			
			<g:form url = "[resource:usuarioPermisoInstance, action:'save']" >
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset> <fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Crear" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>