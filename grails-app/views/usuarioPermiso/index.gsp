<%@ page import = "seguridad.UsuarioPermiso" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Asociaciones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Asociación </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Listado de Usuarios Permiso </h1> <br/> 
			
			<g:form action = "index">
			
				<table style = "margin-left: 2.5%">
					
					<thead> <tr> <th> Permiso </th> <th> Usuario </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass"> <td> <input name = "permiso" value = "${params.permiso}" /> </td> <td> <input name = "usuario" value = "${params.usuario}" /> </td> </tr>
						
						<g:each in = "${asociaciones}" status = "i" var = "asociacion">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> ${asociacion.permiso} </td> <td> ${asociacion.usuario} </td>
						
							</tr>
							
						</g:each>
						
					</tbody>
					
				</table>
				
				<div class = "paginations"> <g:submitButton class = "botonBuscador" name = "Buscar"/> <g:paginate total = "${asociaciones.totalCount ?: 0}" /> </div>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>