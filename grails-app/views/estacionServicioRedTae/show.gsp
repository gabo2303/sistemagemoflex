<%@ page import = "Maestros.EstacionServicioRedTae" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Mostrando Estación de Servicio </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Estaciones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Estación de Servicio </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-estacionServicioRedTae" class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Estación de Servicio </h1>
			
			<ol class = "property-list estacionServicioRedTae">
			
				<li class = "fieldcontain">
						
					<span class = "property-label"> Código de Estación </span> <span class = "property-value"> ${estacionServicioRedTaeInstance.codigoEstacion} </span>
					
				</li>
						
				<li class = "fieldcontain">
					
					<span class = "property-label"> Localidad </span> <span class = "property-value"> ${estacionServicioRedTaeInstance?.localidad} </span>
					
				</li>
			
			</ol>
			
			<g:form resource = "${estacionServicioRedTaeInstance}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${estacionServicioRedTaeInstance}"> Editar </g:link>
					
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('¿Esta seguro?');" />
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>