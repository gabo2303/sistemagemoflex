<%@ page import = "Maestros.EstacionServicioRedTae" %>

<div class = "fieldcontain">
	
	<label for = "codigoEstacion"> Código de Estación <span class = "required-indicator">*</span> </label>
	
	<g:field name = "codigoEstacion" type = "number" value = "${estacionServicioRedTaeInstance.codigoEstacion}" required = ""/>

</div>

<div class = "fieldcontain">
	
	<label for = "localidad"> Localidad <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" id = "localidad" name = "localidad.id" from = "${Maestros.MaestroPlanta.list(sort:'descripcion', order:'asc')}" 
	optionKey = "id" required = "" value = "${estacionServicioRedTaeInstance?.localidad?.id}" class = "many-to-one"/>

</div>