<%@ page import = "Maestros.EstacionServicioRedTae" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Editando Estación de Servicio </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Estaciones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Estación de Servicio </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "edit-estacionServicioRedTae" class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editar Estación de Servicio </h1> <br/>
			
			<g:form resource = "${estacionServicioRedTaeInstance}" action = "update" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${estacionServicioRedTaeInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>