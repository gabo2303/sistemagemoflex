<%@ page import = "Maestros.EstacionServicioRedTae" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Estaciones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Estación </g:link> </li> </ul> </div>
		
		<div id = "list-estacionServicioRedTae" class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1> Listado de Estaciones </h1> <br/>
			
			<g:form action = "index">			
				
				<table style = "margin-left:2.5%">
					
					<thead> <tr> <th> Id </th> <th> Código de Estación </th> <th> Planta </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass">
							
							<td> </td> 
							<td> <input name = "codigoEstacion" value = "${params.codigoEstacion}"> </td> 
							<td> <g:select name = "localidad" from = "${Maestros.MaestroPlanta.list().sort { it.descripcion }}" noSelection = "['':'Seleccione']" value = "${params.localidad?.toInteger()}" optionKey = "id"/> </td>
							
						</tr>
						
						<g:each in = "${estaciones}" status = "i" var = "estacionServicio">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> <tooltip:tip value = "Haga click para Editar / Eliminar la estación."> <g:link action = "show" id = "${estacionServicio.id}"> ${estacionServicio.id} </g:link> </tooltip:tip> </td>
								
								<td> ${estacionServicio.codigoEstacion} </td> <td> ${estacionServicio.localidad} </td>
						
							</tr>
						
						</g:each>
					
					</tbody>
				
				</table>
				
				<div class = "paginations"> 
					
					<tooltip:tip value = "Presione para filtrar los datos."> <g:submitButton name = "Buscar" class = "botonBuscador"/> </tooltip:tip> 
					
					<g:paginate total = "${estaciones.totalCount ?: 0}"/> 
				
				</div>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>