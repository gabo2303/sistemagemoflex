<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Crear Tipo de Inspección </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "list" action = "index"> Listado Tipo de Inspección </g:link> </li> </ul> </div>
		
		<div id = "create-maestroTipoInspeccion" class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Crear Inspección </h1>
			
			<g:form url = "[resource:maestroTipoInspeccionInstance, action:'save']" >
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Crear" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>