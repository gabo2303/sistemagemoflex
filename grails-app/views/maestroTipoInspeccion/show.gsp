<%@ page import = "Maestros.MaestroTipoInspeccion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Mostrando Tipo de Inspección </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado Tipo de Inspección </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Tipo de Inspección </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-maestroTipoInspeccion" class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Tipo de Inspección</h1>
			
			<ol class = "property-list maestroTipoInspeccion">
			
				<li class = "fieldcontain">
						
					<span class = "property-label"> Cliente </span>
				
					<span class = "property-value"> <g:link controller = "maestroCliente" action = "show" id = "${maestroTipoInspeccionInstance?.cliente?.id}"> ${maestroTipoInspeccionInstance?.cliente?.razonSocial} </g:link> </span>
					
				</li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Descripción </span> <span class = "property-value"> ${maestroTipoInspeccionInstance.descripcion} </span> </li>
				
				<g:if test = "${maestroTipoInspeccionInstance?.bloqueable}">
					
					<li class = "fieldcontain">
						
						<span class = "property-label"> Bloqueable </span> <span class = "property-value"> <g:fieldValue bean="${maestroTipoInspeccionInstance}" field="bloqueable"/> </span>
					
					</li>
					
				</g:if>
						
				<li class = "fieldcontain">
						
					<span class = "property-label"> Activo </span> <span class = "property-value"> <g:formatBoolean boolean = "${maestroTipoInspeccionInstance.activo}" true = "Si" false = "No"/> </span>
				
				</li>
				
			</ol>
			
			<g:form resource = "${maestroTipoInspeccionInstance}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${maestroTipoInspeccionInstance}"> Editar </g:link>
					
					<g:if test = "${maestroTipoInspeccionInstance.activo}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar el Tipo de Inspección?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<div hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </div>
					
					</g:if>
					
					<g:else>
						
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
						
						<script>
							
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar el Tipo de Inspección?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
						
						</script>
						
						<div hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </div>
					
					</g:else>	
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>