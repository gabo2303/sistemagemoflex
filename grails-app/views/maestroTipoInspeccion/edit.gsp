<%@ page import = "Maestros.MaestroTipoInspeccion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Editando Tipo de Inpección </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
		
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado Tipo de Inspección </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Tipo de Inspección </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "edit-maestroTipoInspeccion" class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editando Tipo de Inspección </h1>
			
			<g:form url = "[resource:maestroTipoInspeccionInstance, action:'update']" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${maestroTipoInspeccionInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>