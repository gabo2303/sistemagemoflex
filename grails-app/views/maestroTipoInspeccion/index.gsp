<%@ page import = "Maestros.MaestroTipoInspeccion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Tipos de Inspección </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Tipo de Inspección </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Listado de Tipos de Inspección </h1> <br/>
						
			<table style = "margin-left:2.5%">
				
				<thead> <tr> <th> Id </th> <th> Cliente </th> <th> Descripción </th> <th> Activo </th> </tr> </thead>
				
				<tbody>
					
					<g:each in = "${tiposInspeccion}" status = "i" var = "tipoInspeccion">
					
						<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
					
							<td> <g:link action = "show" id = "${tipoInspeccion.id}"> ${tipoInspeccion.id} </g:link> </td>
						
							<td> ${tipoInspeccion.cliente.razonSocial} </td> <td> ${tipoInspeccion.descripcion} </td> <td> <g:formatBoolean boolean = "${tipoInspeccion.activo}" false = "No" true = "Si" /> </td>						
						
						</tr>
						
					</g:each>
					
				</tbody>
				
			</table>
			
			<div class = "paginations"> <g:paginate total = "${tiposInspeccion.totalCount ?: 0}" /> </div>
			
		</div>
		
	</body>
	
</html>