<%@ page import = "Maestros.MaestroTipoInspeccion" %>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label>
	
	<g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" value = "${maestroTipoInspeccionInstance?.cliente?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "descripcion"> Descripción <span class = "required-indicator">*</span> </label> <g:textField name = "descripcion" required = "" value = "${maestroTipoInspeccionInstance?.descripcion}"/>

</div>