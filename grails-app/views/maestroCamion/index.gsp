<%@ page import = "Maestros.MaestroCamion" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Flota de Camiones </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Camión </g:link> </li> </ul> </div>
		
		<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
		<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
		
		<div class = "content scaffold-list" role = "main">
						
			<h1> Listado de Camiones </h1>	<br/> 
			
			<g:form name = "tablaUsuario" action = 'index'>
				
				<table style = "margin-left: 2.5%">
						
					<thead> <tr> <th> Número </th> <th> Cliente </th> <th> Transportista </th> <th> Patente </th> <th> Planta </th> <th> Activo </th> </tr> </thead>
				
					<tbody>
					
						<tr class = "noclass">
						
							<td> <input size = 7 name = "numero" type = "text" value = "${params.numero}"> </td>
							
							<td> <g:select name = "cliente" from = "${Maestros.MaestroCliente.list().sort { it.razonSocial }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.cliente}"/> </td>
								
							<td> <g:select name = "transportista" from = "${Maestros.MaestroTransportista.list().sort { it.descripcion }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.transportista?.toInteger()}"/> </td>
															
							<td> <input size = 17 name = "patente" type = "text" value = "${params.patente}"> </td>
							
							<td> <g:select name = "planta" from = "${Maestros.MaestroPlanta.list().sort { it.descripcion }}" optionKey = "id" noSelection = "['':'Seleccione']" value = "${params.planta?.toInteger()}"/> </td>
							
							<td> <g:select name = "activo" from = "${['Verdadero', 'Falso']}" noSelection = "['':'Seleccione una Opción']" value = "${params.activo}"/> </td>																		
						
						</tr>
						
						<g:each in = "${camiones}" status = "i" var = "camion">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
								
								<td> <g:link action = "show" id = "${camion.id}"> ${camion.numero} </g:link> </td>
																
								<td> ${camion.cliente} </td> <td> ${camion.transportista.descripcion} </td> <td> ${camion.patente} </td>
								
								<td> ${camion.planta} </td> <td> <g:formatBoolean boolean = "${camion.activo}" true = "Si" false = "No" /> </td>
																								
							</tr>
							
						</g:each>
					
					</tbody>
					
				</table>
					
				<div class = "paginations"> <g:submitButton name = "Buscar" class = "botonBuscador"/> <g:paginate total = "${camiones.totalCount ?: 0}"/> </div>
				
			</g:form>
		
		</div>
		
	</body>
	
</html>