<%@ page import = "Maestros.MaestroCamion" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Mostrando Camión </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Camiones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Camión </g:link> </li>
			</ul>
			
		</div>
				
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>		
			
			<h1> Datos de Camión </h1>
			
			<ol class = "property-list maestroCamion">
			
				<li class = "fieldcontain"> <span class = "property-label"> Cliente </span> <span class = "property-value"> ${camion?.cliente} </span> </li>

				<li class = "fieldcontain"> <span class = "property-label"> Planta </span> <span class = "property-value"> ${camion?.planta} </span> </li>
					
				<li class = "fieldcontain"> <span class = "property-label"> Transportista </span> <span class = "property-value"> ${camion?.transportista.descripcion} </span> </li>
					
				<li class = "fieldcontain"> <span class = "property-label"> Patente </span> <span class = "property-value"> ${camion.patente} </span> </li> 

				<li class = "fieldcontain"> <span class = "property-label"> Marca </span> <span class = "property-value"> ${camion.marca} </span> </li>
					
				<li class = "fieldcontain"> <span class = "property-label"> Número </span> <span class = "property-value"> ${camion.numero} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Tipo Camión </span> <span class = "property-value"> ${camion.tipoCamion} </span> </li>
			
				<li class = "fieldcontain"> <span class = "property-label"> Capacidad </span> <span class = "property-value"> ${camion.capacidad} </span> </li>
				
				<li class = "fieldcontain">
						
					<span class = "property-label"> Activo </span> <span class = "property-value"> <g:formatBoolean boolean = "${camion.activo}" false = "No" true = "Si"/> </span>
				
				</li>	
							
			</ol>
			
			<g:form resource = "${camion}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${camion}"> Editar </g:link>
					
					<g:if test = "${camion.activo}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar el Correo?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<label hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </label>
					
					</g:if>
					
					<g:else>
					
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar el Correo?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<label hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </label>
					
					</g:else>								
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>