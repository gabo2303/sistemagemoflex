<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Creación de Camión </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "list" action = "index"> Listado de Camiones </g:link> </li> </ul> </div>
			
		<div class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>	
			<g:if test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:if>
			
			<h1> Creación de Camión </h1> 
			
			<g:form resource = "${maestroCamionInstance}" action = "save" >
							
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Guardar" /> </fieldset>
			
			</g:form>
		
		</div>
	
	</body>

</html>