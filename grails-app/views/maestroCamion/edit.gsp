<%@ page import = "Maestros.MaestroCamion" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Editando Camión </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Camiones</g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Camión</g:link> </li>
			</ul>
			
		</div>
			
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:if test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:if>
			
			<h1> Editar Camión </h1> <br/>
						
			<g:form action = "update" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${maestroCamionInstance?.version}" />
				<g:hiddenField name = "id" value = "${maestroCamionInstance?.id}" />
					
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
					
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
			
			</g:form>
		
		</div>
	
	</body>

</html>