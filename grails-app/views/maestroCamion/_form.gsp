<%@ page import = "Maestros.MaestroCamion" %>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente </label> ${cliente.razonSocial} <g:hiddenField name = "cliente.id" value = "${cliente.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "planta"> Planta <span class = "required-indicator">*</span> </label>
	
	<g:select name = "planta" from = "${plantas.sort { it.descripcion }}" optionKey = "id" value = "${maestroCamionInstance?.planta?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "transportista"> Transportista <span class = "required-indicator">*</span> </label>
	
	<g:select name = "transportista" from = "${transportistas.sort { it.descripcion }}" optionKey = "id" value = "${maestroCamionInstance?.transportista?.id}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "patente"> Patente <span class = "required-indicator">*</span> </label>
	
	<g:textField name = "patente" maxlength = "50" required = "" value = "${maestroCamionInstance?.patente}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "marca"> Marca <span class = "required-indicator">*</span> </label>
	
	<g:textField name = "marca" maxlength = "50" required = "" value = "${maestroCamionInstance?.marca}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "numero"> Número <span class = "required-indicator">*</span> </label>
	
	<g:textField name = "numero" maxlength = "20" required = "" value = "${maestroCamionInstance?.numero}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "tipoCamion"> Tipo Camión <span class = "required-indicator">*</span> </label>
	
	<select name = "tipoCamion" id = "tipo">
	
		<option> Tanque </option>
		<option> Tracto </option>
		<option> Camion Tanque </option>
		<option> Camion Tracto Rampla </option>
		
	</select>

</div>

<div class = "fieldcontain">
	
	<label for = "capacidad"> Capacidad <span class = "required-indicator">*</span> </label>
	
	<g:textField name = "capacidad" maxlength = "50" required = "" value = "${maestroCamionInstance?.capacidad}"/>

</div>