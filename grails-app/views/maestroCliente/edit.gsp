<%@ page import = "Maestros.MaestroCliente" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Cliente </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> 
		
			<ul>
				<li> <g:link class = "list" action = "index"> Lista de Clientes </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Cliente </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editando Cliente </h1>
			
			<g:form resource = "${maestroClienteInstance}" action = "update" method = "PUT" >
			
				<g:hiddenField name = "version" value = "${maestroClienteInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>