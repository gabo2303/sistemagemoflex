<%@ page import = "Maestros.MaestroCliente" %>

<div class = "fieldcontain">

	<label for = "rut"> Rut <span class = "required-indicator">*</span> </label> <g:textField name = "rut" required = "true" value = "${maestroClienteInstance?.rut}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "razonSocial"> Razón Social <span class = "required-indicator">*</span> </label> <g:textField name = "razonSocial" required = "true" value = "${maestroClienteInstance?.razonSocial}"/>
	
</div>