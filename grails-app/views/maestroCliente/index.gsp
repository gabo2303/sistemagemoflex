<%@ page import = "Maestros.MaestroCliente" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Clientes </title> </head>
	
	<body>
		
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Cliente </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
		
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Listado de Clientes </h1> <br/>
			
			<table style = "margin-left: 2.5%">
				
				<thead> <tr> <th> Id </th> <th> Rut </th> <th> Razón Social </th> <th> Activo </th> </tr> </thead>
				
				<tbody>
					
					<g:each in = "${clientes}" status = "i" var = "cliente">
						
						<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
								
							<td> <g:link action = "show" id = "${cliente.id}"> ${cliente.id} </g:link> </td>																	
							<td> ${cliente.razonSocial} </td> <td> ${cliente.rut} </td>	<td> <g:formatBoolean boolean = "${cliente.activo}" false = "No" true = "Si" />
							
						</tr>
						
					</g:each>
					
				</tbody>
				
			</table>
			
			<div class = "pagination"> <g:paginate total = "${clientes.totalCount ?: 0}" /> </div>
			
		</div>
		
	</body>
	
</html>