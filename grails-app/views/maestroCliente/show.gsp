<%@ page import = "Maestros.MaestroCliente" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Mostrando Cliente </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Lista de Clientes </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Cliente </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Cliente </h1>			
			
			<ol class = "property-list maestroCliente">
			
				<li class = "fieldcontain"> <span class = "property-label"> Rut </span> <span class = "property-value"> ${cliente.rut} </span> </li>

				<li class = "fieldcontain"> <span class = "property-label"> Razón Social </span> <span class = "property-value"> ${cliente.razonSocial} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Activo </span> <span class = "property-value"> <g:formatBoolean boolean = "${cliente.activo}" true = "Si" false = "No" /> </span> </li> 
							
			</ol>
			
			<g:form resource = "${cliente}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${cliente}"> Editar </g:link>
					
					<g:if test = "${cliente.activo}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar el Cliente?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<div hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </div>
					
					</g:if>
					
					<g:else>
						
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
						
						<script>
							
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar el Cliente?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
						
						</script>
						
						<div hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </div>
					
					</g:else>
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>