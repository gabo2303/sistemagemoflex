<style> @media screen and (max-width: 480px) { .copyright { background: #383836; text-align: center; color: white; } } </style>

<div class = "copyright">
	
	<p> &copy; ${new Date()[Calendar.YEAR] } Sistema de Inspecciones RCT 2.0</p> <br/>
	
	<p> <a style = "color: white" href = http://www.global-point.cl> By GlobalPoint </a> </p>
	  
</div>