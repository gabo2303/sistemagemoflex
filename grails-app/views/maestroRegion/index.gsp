<%@ page import = "Maestros.MaestroRegion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Regiones </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Región </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
		
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1 align = "center"> Listado de Regiones </h1> <br/>
			
			<table style = "margin-left:30%; width:40%">
			
				<thead> <tr> <th> Id </th> <th> Descripción </th> </tr> </thead>
				
				<tbody>
					
					<g:each in = "${regiones}" status = "i" var = "region">
						
						<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}"> 
							
							<td> <tooltip:tip value = "Haga click para Editar / Eliminar la Región."> <g:link action = "show" id = "${region.id}"> ${region.id} </g:link> </tooltip:tip> </td> 
							<td> ${region.descripcion} </td> 
						
						</tr>
					
					</g:each>
				
				</tbody>
			
			</table>
			
			<div class = "paginations" style = "margin-left:30%; width:39.5%"> <g:paginate total = "${regiones.totalCount ?: 0}" /> </div>
			
		</div>
		
	</body>
	
</html>