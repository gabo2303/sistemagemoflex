<%@ page import = "Maestros.MaestroRegion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Mostrando Región </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Regiones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Región </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-maestroRegion" class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Región </h1> <br/>
			
			<ol class = "property-list maestroRegion">
			
				<li class = "fieldcontain"> <span class = "property-label"> Descripción </span> <span class = "property-value"> ${maestroRegionInstance?.descripcion} </span> </li>			
						
			</ol>
			
			<g:form url = "[resource:maestroRegionInstance, action:'delete']" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${maestroRegionInstance}"> Editar </g:link>
					
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('¿Esta seguro?');" />
					
				</fieldset>
			
			</g:form>
		
		</div>
	
	</body>
	
</html>