<%@ page import = "Maestros.MaestroRegion" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Editando Región </title> </head> 
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Regiones </g:link> </li>
				<li> <g:link class = "create" action = "create"> Creear Región </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editar Región </h1> <br/>
			
			<g:hasErrors bean = "${maestroRegionInstance}">
				
				<ul class = "errors" role = "alert">
					
					<g:eachError bean="${maestroRegionInstance}" var = "error">
						
						<li <g:if test = "${error in org.springframework.validation.FieldError}">data-field-id = "${error.field}"</g:if>> <g:message error = "${error}"/> </li>
					
					</g:eachError>
					
				</ul>
				
			</g:hasErrors>
			
			<g:form url = "[resource:maestroRegionInstance, action:'update']" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${maestroRegionInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>