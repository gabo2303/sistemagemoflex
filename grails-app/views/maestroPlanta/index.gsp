<%@ page import = "Maestros.MaestroPlanta" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Listado de Plantas </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> 
			
			<ul> 
				<li> <g:link class = "create" action = "create"> Crear Planta </g:link> </li>
				<li> <g:link class = "excel" action = "descargarExcel"> Descargar Plantas </g:link> </li> 
			</ul> 
		
		</div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			<g:elseif test = "${flash.warning}"> <div class = "errors"> ${flash.warning} </div> </g:elseif>
			
			<h1> Listado de Plantas </h1> <br/>
			
			<g:form action = "index">	
					
				<table style = "margin-left:2.5%">
					
					<thead> <tr> <th> Id </th> <th> Cliente </th> <th> Descripción </th> <th> Región </th> <th> Código Copec </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass">
							
							<td> </td>
							<td> <g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" noSelection = "['':'Seleccione']" value = "${params.cliente}" optionKey = "id"/> </td>
							<td> <input name = "descripcion" value = "${params.descripcion}"> </td>
							<td> <g:select name = "region" from = "${Maestros.MaestroRegion.list().sort { it.descripcion }}" noSelection = "['':'Seleccione']" value = "${params.region}" optionKey = "id"/> </td>
							<td> </td>
							
						</tr>
						
						<g:each in = "${plantas}" status = "i" var = "maestroPlanta">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
							
								<td> <tooltip:tip value = "Haga click para Editar / Eliminar la Planta."> <g:link action = "show" id = "${maestroPlanta.id}"> ${maestroPlanta.id} </g:link> </tooltip:tip> </td> 
								
								<td> ${maestroPlanta.cliente} </td> <td> ${maestroPlanta.descripcion} </td> <td> ${maestroPlanta.region} </td>
								
								<td> ${maestroPlanta.codigoCopec} </td>
							
							</tr>
						
						</g:each>
						
					</tbody>
					
				</table>
				
				<div class = "paginations"> 
					
					<tooltip:tip value = "Presione para filtrar los datos."> <g:submitButton name = "Buscar" class = "botonBuscador"/> </tooltip:tip>
					
					<g:paginate total = "${plantas.totalCount ?: 0}" /> 
				
				</div>
			
			</g:form>
			 
		</div>
		
	</body>
	
</html>