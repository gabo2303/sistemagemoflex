<%@ page import = "Maestros.MaestroPlanta" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Editando Planta </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>			
				<li> <g:link class = "list" action = "index"> Listado de Plantas </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Planta </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status">${flash.message}</div> </g:if>
			
			<h1> Editando Planta </h1>
			
			<g:form url = "[resource:maestroPlantaInstance, action:'update']" method = "PUT">
				
				<g:hiddenField name = "version" value = "${maestroPlantaInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>