<%@ page import = "Maestros.MaestroPlanta" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Planta ${planta.descripcion} </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>				
				<li> <g:link class = "list" action = "index"> Listado de Plantas </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Planta </g:link> </li>
			</ul>
			
		</div>
		
		<div class = "content scaffold-show" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Planta ${planta.descripcion} </h1>
			
			<ol class = "property-list maestroPlanta">
			
				<li class = "fieldcontain"> <span class = "property-label"> Cliente </span> <span class = "property-value"> ${planta?.cliente} </span> </li>
			
				<li class = "fieldcontain"> <span class = "property-label"> Descripción </span> <span class = "property-value"> ${planta.descripcion} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Código Copec </span> <span class = "property-value"> ${planta.codigoCopec} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Región </span> <span class = "property-value"> ${planta?.region} </span> </li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Activa </span> <span class = "property-value"> <g:formatBoolean boolean = "${planta?.activa}" true = "Si" false = "No" /> </span> </li>
			
			</ol>
			
			<g:form resource = "${planta}" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${planta}"> Editar </g:link>
					
					<g:if test = "${planta.activa}"> 
						
						<input class = "delete" id = "eliminar" value = "Desactivar" type = "submit" >
					
						<script>
	
							$(document).ready(function(){ $('#eliminar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de desactivar la Planta?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#eliminarFinal').trigger('click'); }); } });  }); });
				
						</script>
						
						<div hidden = ""> <input id = "eliminarFinal" type = "submit" name = "accion" value = "Desactivar"> </div>
					
					</g:if>
					
					<g:else>
						
						<input class = "add" id = "activar" value = "Activar" type = "submit" >
						
						<script>
							
							$(document).ready(function(){ $('#activar').on('click', function(e){ e.preventDefault(); bootbox.confirm("¿Esta seguro de activar la Planta?", 
							function(confirmed) { if(confirmed == true) { $(document).ready(function(){$('#activarFinal').trigger('click'); }); } });  }); });
						
						</script>
						
						<div hidden = ""> <input id = "activarFinal" type = "submit" name = "accion" value = "Activar"> </div>
					
					</g:else>
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>