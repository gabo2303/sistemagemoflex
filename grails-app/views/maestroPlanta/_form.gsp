<%@ page import = "Maestros.MaestroPlanta" %>

<div class = "fieldcontain">
	
	<label for = "cliente"> Cliente <span class = "required-indicator">*</span> </label>
	
	<g:select name = "cliente" from = "${Maestros.MaestroCliente.list()}" optionKey = "id" required = "" value = "${maestroPlantaInstance?.cliente?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "descripcion"> Descripción <span class = "required-indicator">*</span> </label> <g:textField name = "descripcion" required = "" value = "${maestroPlantaInstance?.descripcion}"/>

</div>

<div class = "fieldcontain">
	
	<label for = "region"> Región <span class = "required-indicator">*</span> </label>
	
	<g:select name = "region" from = "${Maestros.MaestroRegion.list(sort:'descripcion', order:'asc')}" optionKey = "id" required = "" value = "${maestroPlantaInstance?.region?.id}"/>

</div>

<div class = "fieldcontain">
	
	<label> Código Copec <span class = "required-indicator">*</span> </label> 
	
	<g:textField name = "codigoCopec" required = "" value = "${maestroPlantaInstance?.codigoCopec}"/>

</div>