<%@ page import = "Maestros.CheckList" %>

<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Listado de Preguntas </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "create" action = "create"> Crear Item </g:link> </li> </ul> </div>
		
		<div class = "content scaffold-list" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Listado de Preguntas </h1> <br/>
			
			<g:form action = "index">
			
				<table style = "margin-left:2.5%">
					
					<thead> <tr> <g:sortableColumn property = "id" title = "Id" /> <th> Tipo de Inspección </th> <th> Clasificación </th> <th> Crítico </th> <th> Bueno </th> <th> Malo </th> <th> Pregunta </th> </tr> </thead>
					
					<tbody>
						
						<tr class = "noclass"> 
							
							<td> </td> <td> <g:select name = "tipoInspeccion" from = "${Maestros.MaestroTipoInspeccion.findAllByActivo(true)}" optionKey = "id" 
							optionValue = "descripcion" noSelection = "['':'Seleccione']" value = "${params.tipoInspeccion}"/> </td>
							<td> <input name = "clasificacion" value = "${params.clasificacion}" size = "20" > </td>
							<td> <g:select name = "critico" from = "${['Si', 'No']}" noSelection = "['':'Seleccione']" value = "${params.critico}"/> </td> 
							<td colspan = "2"> </td> <td> <input name = "pregunta" value = "${params.pregunta}" size = "40" > </td>
							
						</tr>
							
						<g:each in = "${preguntas}" status = "i" var = "pregunta">
							
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
						
								<td> <g:link action = "show" id = "${pregunta.id}"> ${pregunta.id} </g:link> </td> <td> ${pregunta.tipoInspeccion.descripcion} </td> 
								
								<td> ${pregunta.clasificacion.descripcion} </td> <td> <g:if test = "${pregunta.critico == 1}"> Si </g:if> <g:else> No </g:else> </td> <td> ${pregunta.bueno} </td> 
								
								<td> ${pregunta.malo} </td> <td> ${pregunta.pregunta} </td>
							
							</tr>
						
						</g:each>
						
					</tbody>
					
				</table>
				
				<div class = "paginations"> 
					
					<g:submitButton name = "Filtrar" class = "botonBuscador" /> <g:paginate total = "${preguntas.totalCount ?: 0}" params = "${params}"/> 
					
				</div>
			
			</g:form>
			
		</div>
		
	</body>
	
</html>