<%@page import = "Maestros.MaestroTipoInspeccion"%>
<%@ page import = "Maestros.CheckList" %>

<div class = "fieldcontain">

	<label for = "pregunta"> Pregunta <span class = "required-indicator">*</span> </label> <g:textField size = "60" name = "pregunta" required = "" value = "${checkListInstance?.pregunta}"/>
	
</div>

<div class = "fieldcontain">
	
	<label for = "bueno"> Puntaje Bueno <span class = "required-indicator">*</span> </label> <g:field name = "bueno" type = "number" value = "${checkListInstance.bueno}" required = ""/> 

</div>

<div class = "fieldcontain">
	
	<label for = "malo"> Puntaje Malo <span class = "required-indicator">*</span> </label> <g:field name = "malo" type = "number" value = "${checkListInstance.malo}" required = ""/>

</div>

<div class = "fieldcontain">
	
	<label for = "critico"> Item Crítico <span class = "required-indicator">*</span> </label>
	
	<select name = "critico" > <option value = "0"> No </option> <g:if test = "${checkListInstance.critico == 1}"> <option value = "1" selected = "selected"> Si </option> </g:if> <g:else> <option value = "1"> Si </option> </g:else> </select>

</div>

<div class = "fieldcontain">
	
	<label for = "clasificacion"> Clasificación <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" id = "clasificacion" name = "clasificacion.id" value = "${checkListInstance?.clasificacion?.id}"
	from = "${Maestros.MaestroClasificacion.list(sort:'descripcion', order:'asc')}" optionKey = "id" required = "" class = "many-to-one"/>

</div>

<div class = "fieldcontain">
	
	<label for = "cabecera"> Tipo Inspección <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" id = "tipoInspeccion" name = "tipoInspeccion.id" value = "${checkListInstance?.tipoInspeccion?.id}"
	from = "${MaestroTipoInspeccion.findAllByActivo(true, [sort:'descripcion', order:'asc'])}" optionKey = "id" />

</div>