<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Creación de Item </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "list" action = "index"> Listado de Items </g:link> </li> </ul> </div>
		
		<div id = "create-checkList" class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Creación de Item </h1> <br/>
			
			<g:form url = "[resource:checkListInstance, action:'save']" >
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset> 
				
				<fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Crear"/> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>