<%@ page import = "Maestros.CheckList" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> CheckList </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class = "list" action = "index"> Listado de Items </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Item </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "show-checkList" class = "content scaffold-show" role = "main">
						
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Mostrando Pregunta de CheckList </h1> <br/>
			
			<ol class = "property-list checkList">
			
				<li class = "fieldcontain"> <span class = "property-label"> Pregunta </span> <span class = "property-value "> ${checkListInstance.pregunta} </span> </li>	
				
				<li class = "fieldcontain"> <span class = "property-label"> Puntaje Bueno </span> <span class = "property-value"> ${checkListInstance.bueno} </span> </li>	
				
				<li class = "fieldcontain"> <span class = "property-label"> Puntaje Malo </span> <span class = "property-value"> ${checkListInstance.malo} </span> </li>
				
				<li class = "fieldcontain"> 
				
					<span class = "property-label"> Item Crítico </span> 
					
					<span class = "property-value"> <g:if test = "${checkListInstance.critico == 1}"> Si </g:if> <g:else> No </g:else> </span> 
				
				</li>
				
				<li class = "fieldcontain"> <span class = "property-label"> Clasificación </span> <span class = "property-value"> ${checkListInstance?.clasificacion?.descripcion} </span> </li>
					
				<li class = "fieldcontain"> <span class = "property-label"> Tipo de Inspección </span> <span class = "property-value"> ${checkListInstance?.tipoInspeccion.descripcion} </span> </li>
					
			</ol>
			
			<g:form resource = "checkListInstance" action = "delete" method = "DELETE">
				
				<fieldset class = "buttons">
					
					<g:link class = "edit" action = "edit" resource = "${checkListInstance}"> Editar </g:link>
					
					<g:actionSubmit class = "delete" action = "delete" value = "Eliminar" onclick = "return confirm('¿Esta Seguro?');" />
					
				</fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>