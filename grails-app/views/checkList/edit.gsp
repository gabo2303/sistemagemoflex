<%@ page import = "Maestros.CheckList" %>

<!DOCTYPE html>

<html>
	
	<head> <meta name = "layout" content = "main"> <title> Editando Item </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation">
			
			<ul>
				<li> <g:link class = "list" action = "index"> Listado de Items </g:link> </li>
				<li> <g:link class = "create" action = "create"> Crear Item </g:link> </li>
			</ul>
			
		</div>
		
		<div id = "edit-checkList" class = "content scaffold-edit" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if>
			
			<h1> Editando Item </h1> <br/>
			
			<g:form resource = "${checkListInstance}" action = "update" method = "PUT" >
				
				<g:hiddenField name = "version" value = "${checkListInstance?.version}" />
				
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
					
				<fieldset class="buttons"> <g:actionSubmit class = "save" action = "update" value = "Actualizar"/> </fieldset>
			
			</g:form>
			
		</div>
		
	</body>
	
</html>