<%@ page contentType = "text/html;charset=UTF-8" %>

<html>
	
	<head>
		
		<meta http-equiv = "Content-Type" content = "text/html; charset=ISO-8859-1"/> <meta name = "layout" content = "main"/>

		<link rel = "stylesheet" href = "/SistemaGemoflex/css/formlimpio.css" type = "text/css"> <tooltip:resources/>

		<title> Consulta Críticos / Malos </title>
	
	</head>

	<body>
  		
  		<br/> 	<g:if test = "${flash.warning}"> 
  			
  			<div class = "errors" style = "width: 90%; margin-left:5%; font-size: 13px"> ${flash.warning} </div> 
  		
  		</g:if>
		
		<div class = "content scaffold-list">	
			
			<h1> Consulta por Items Malos / Críticos </h1> <br/>
						
			<g:form name = "tablaUsuario" controller = "consultaCriticos" action = "consulta">
				
				<table style = "margin-left: 2.5%; width:95%" class = "buttons">
				
					<tr class = "noclass"> 
						
						<td style = "width: 120px"> Tipo de Inspección: </td> 
						
						<g:if test = "${params.tipoInspeccion == null || params.tipoInspeccion == ''}"> 
						
							<td> <g:select from = "${tipoInspeccion}" optionValue = "descripcion" name = "tipoInspeccion" style = "width:200px"
							noSelection = "['':'Todos los Tipos']" /> </td>
							
						</g:if>
						
						<g:else>
						
							<td> <g:select from = "${tipoInspeccion}" optionValue = "descripcion" name = "tipoInspeccion" style = "width:200px"
							noSelection = "['':'Todos los Tipos']" value = "${Maestros.MaestroTipoInspeccion.get(params.tipoInspeccion.toInteger())}"/> </td>
							
						</g:else>
						
						<td> Planta: </td> 
						<td> <g:select from = "${plantas.descripcion}" name = "plantas" noSelection = "['':'Todas las Plantas']" value = "${params.plantas}"/> </td> 
						
						<td> Transportista: </td> 
						<td> <g:select from = "${transportistas.descripcion}" name = "transportistas" noSelection = "['':'Todos los Transportistas']" value = "${params.transportistas}" style = "width:200px"/> </td>
											
						<td> Período: </td>
						<td> <g:datePicker name = "periodo" precision = "month" value = "${params.periodo}"/> </td>												
					</tr>

				</table>
				
				<fieldset class = "buttons" style = "margin-top: 20px; width:10%; margin-left:45%"> 
					
					<g:actionSubmit action = "consulta" class = "edit" value = "Consultar"/> 
					
				</fieldset>
				
			</g:form>
			
		</div>
		
		<g:if test = "${listadoTotal}">
			
			<g:form url = "[controller:'consultaCriticos', action:'consulta', params:[plantas:params.plantas, transportistas:params.transportistas, tipoInspeccion:params.tipoInspeccion]]">
			
				<table style = "margin-left: 2.5%; margin-top:2%;">
					 		
					<thead>
					
						<tr>
							<th> N° Camión </th> <th> Item </th> <th> Planta </th> <th> Transportista </th> <th> Fecha Inspección </th> 
							
							<th> Repetición </th> <th> Comentario </th> <th> Evaluación </th> <th> Tipo Inspección / Filtrar </th>											
						</tr>
						
					</thead>
				
					<tbody>
						
						<tr class = "noclass">
							
							<td> <input name = "numero" value = "${params.numero}" placeholder = "Ingrese número exacto."/> </td> 
							
							<td style = "display:none"> <g:datePicker hidden = "" name = "periodo" value = "${params.periodo}" /> </td>
							
							<td colspan = "6"> <input name = "item" value = "${params.item}"/> </td>					
							
							<td> <g:select name = "evaluacion" from = "${['Criticos', 'Malos']}" noSelection = "['':'Todos']" value = "${params.evaluacion}"/> </td>
							
							<td> <g:submitButton class = "botonBuscador" name = "Filtrar"/> </td>
							
						</tr>
																	
						<g:each in = "${listado}" status = "i" var = "inspeccion">
						
							<tr class = "${(i % 2) == 0 ? 'even' : 'odd'}">
								
								<td> ${inspeccion.idCabecera.numero} </td>
																
								<td> ${inspeccion.pregunta} </td>
															
								<td> ${inspeccion.idCabecera.planta} </td>
																	
								<td> ${inspeccion.idCabecera.transportista} </td>
								
								<td> <g:formatDate date = "${inspeccion.idCabecera.fechaInspeccion}" format = "dd-MM-yyyy"/> </td>
								
								<td> ${inspeccion.criticoRepeticion} </td>
								
								<td> <tooltip:tip value = "${inspeccion.observacion ?: 'No hay observaciones'}"> ${inspeccion.observacion} </tooltip:tip> </td> 
																								
								<g:if test = "${inspeccion.critico == 1}"> <td> Crítico </td> </g:if> <g:else> <td> Malo </td> </g:else>
								
								<td> ${Maestros.MaestroTipoInspeccion.get(inspeccion.idCabecera.idInspeccion).descripcion.drop(15)} </td>	
																						
							</tr>
							
						</g:each>
					
					</tbody>
				
				</table>
				
				<div class = "paginations"> 
					
					<sec:ifAnyGranted roles = "ROLE_SUPERADMINISTRADOR, ROLE_ADMINISTRADOR, ROLE_SUPERVISOR, ROLE_CLIENTE, ROLE_TRANSPORTISTA">
						
						<input type = "submit" class = "descargarExcel" value = "Descargar Excel" id = "excel" style = "margin-right:2%" name = "descargar">
						
					</sec:ifAnyGranted> 
					
					<g:paginate total = "${listadoTotal ?: 0}" params = "${params}"/>
					
				</div>
				
			</g:form>
			
		</g:if>
		
	</body>

</html>