<%@ page import = "Maestros.CorreoTransportista" %>

<div class = "fieldcontain">
	
	<label for = "email"> Email <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "correo" name = "email.id" from = "${Maestros.MaestroCorreo.list(sort:'correo', order:'asc')}" optionKey = "id" required = "" value = "${correoTransportistaInstance?.email?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "planta"> Planta <span class="required-indicator">*</span> </label>
	
	<g:select name = "planta.id" from = "${Maestros.MaestroPlanta.list(sort:'descripcion', order:'asc')}" optionKey = "id" required = "" value = "${correoTransportistaInstance?.planta?.id}" />

</div>

<div class = "fieldcontain">

	<label for = "tipoInspeccion"> Tipo de Inspección <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" name = "tipoInspeccion.id" from = "${Maestros.MaestroTipoInspeccion.findAllByActivo(true).sort { it.descripcion }}" 
	optionKey = "id" required = "" value = "${correoTransportistaInstance?.tipoInspeccion?.id}" />

</div>

<div class = "fieldcontain">
	
	<label for = "transportista"> Transportista <span class = "required-indicator">*</span> </label>
	
	<g:select optionValue = "descripcion" name = "transportista.id" from = "${Maestros.MaestroTransportista.findAllByActivo(true).sort { it.descripcion }}" 
	optionKey = "id" required = "" value = "${correoTransportistaInstance?.transportista?.id}" />

</div>