<!DOCTYPE html>

<html>

	<head> <meta name = "layout" content = "main"> <title> Correo Transportista </title> </head>
	
	<body>
	
		<div class = "nav" role = "navigation"> <ul> <li> <g:link class = "list" action = "index"> Listado Correo Transportista </g:link> </li> </ul> </div>
		
		<div id = "create-correoTransportista" class = "content scaffold-create" role = "main">
			
			<br/> <g:if test = "${flash.message}"> <div class = "message" role = "status"> ${flash.message} </div> </g:if> 
			
			<h1> Crear Correo Transportista </h1> <br/>
			
			<g:form resource = "${correoTransportistaInstance}" action = "save" >
			
				<fieldset class = "form"> <g:render template = "form"/> </fieldset>
				
				<fieldset class = "buttons"> <g:submitButton name = "create" class = "save" value = "Crear" /> </fieldset>
				
			</g:form>
			
		</div>
		
	</body>
	
</html>