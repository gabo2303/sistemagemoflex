package sistemagemoflex

import grails.transaction.Transactional
import java.util.regex.Matcher
import java.util.regex.Pattern

@Transactional
class ValidacionService 
{
     def validarNumeros(String cadena)
	{
		boolean valido
		def pattern = ~/^[0-9]+$/
		
		if(pattern.matcher(cadena).matches())
		{
			valido = true
		}
		else
		{
			valido = false
		}
		return valido
	}
}
