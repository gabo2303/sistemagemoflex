package sistemagemoflex

import grails.transaction.Transactional

@Transactional
class ConsultasService 
{
	def consultaPlanta(usuarioId, transportistaString, idCliente, tInspeccion, descripcionTransportista) 
	{
		def plantaQuery =
		"""
			select descripcion from MaestroPlanta where id in (select planta from UsuarioPlanta where usuario = ${usuarioId}) 
			and id in (select planta from TransportistaPlanta where transportista in (${transportistaString}) 
			and cliente = ${idCliente})    
			and descripcion in (select planta from InspeccionCabecera where totalMalo > 0 and idInspeccion = ${tInspeccion} 
			and transportista in (${descripcionTransportista})) order by descripcion 
		"""
		
		return plantaQuery		
    }	
	
	def arregloString(algo)
	{
		def descripcionParametro = ""

		for (int i = 0; i < algo.size(); i++)
		{
			if (algo[i + 1] == null) { descripcionParametro = descripcionParametro + "'" + algo[i] + "'" }
			
			else { descripcionParametro = descripcionParametro + "'" + algo[i] + "'," }
		}
		
		return descripcionParametro
	}
	
	//Consulta exclusiva para los transportistas que devuelve la planta
	def plantaQuery(usuarioId, transportistaString, clienteId)
	{
		"""
			from MaestroPlanta where id in (select planta from UsuarioPlanta where usuario = ${usuarioId}) 
			and id in (select planta from TransportistaPlanta where transportista.descripcion in (${transportistaString}))
			and cliente = ${clienteId})
		"""
	}
	
	//Consulta exclusiva para los transportistas que devuelve el tipo de inspeccion
	def tipoInspeccionQuery(descripcionTransportista, descripcionPlanta, clienteId)
	{
		"""
			from MaestroTipoInspeccion where id in (select idInspeccion from InspeccionCabecera where estado = 'APROBADO' and 
			transportista in (${descripcionTransportista}) and planta in (${descripcionPlanta}))) and cliente_id = ${clienteId}
		"""
	}
	
	//Consulta que se efect�a una vez obtenidos todos los par�metros para buscar y da run resumen de la cabecera de las inspecciones 
	def resumenCabeceraReporte(planta, transportista, fechaInicial, fechaFinal, idInspeccion)
	{
		"""select id, numero, Date_Format(fechaInspeccion, '%d-%m-%Y'), puntajeTotal, indiceOperacion, totalCriticos, 
		transportista, planta, idInspeccion, nEstacionServicio, marca, trMarca, capacidad from InspeccionCabecera where fechaInspeccion 
		between '${fechaInicial}' and '${fechaFinal}' """ + ((planta != '') ? " and planta in (${planta}) " : "") + 
		""" and estado = 'APROBADO' """ + ((transportista != '') ? " and transportista in (${transportista}) " : "") + 
		""" and idInspeccion in (${idInspeccion}) order by 1 desc"""
	}
	
	//Consulta que se efect�a para dar a redtae el detalle de sus inspecciones
	def inspeccionDetalleRedTae(fechaInicial, fechaFinal, planta)
	{
		"""select d.pregunta, ch.id, c.patente, d.evaluacionItem, d.evaluacionItemBm, c.planta, c.nEstacionServicio, 
		Date_Format(c.fechaInspeccion, '%d-%m-%Y'), c.marca, c.trMarca, c.capacidad from InspeccionDetalle d, CheckList ch, InspeccionCabecera c 
		where d.idCabecera in (select id from InspeccionCabecera where fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' and 
		idInspeccion = 6 """ + ((planta != '') ? " and planta in (${planta})" : "") + 
		""" and estado = 'APROBADO') and d.idCabecera = c.id and ch.id = (select id from CheckList where pregunta = d.pregunta and tipoInspeccion = 6)"""
	}
	
	def consultaHistoricaTransportista(planta, transportista, fechaInicial, fechaFinal, idInspeccion)
	{
		"""
			select id, fechaInspeccion, numero, patente, planta, transportista, totalCriticos, idInspeccion, estado from
			InspeccionCabecera where estado = 'APROBADO' and planta in (${planta}) and transportista in
			(${transportista}) and idInspeccion = ${idInspeccion} and fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' order by id desc
		"""	
	}
}