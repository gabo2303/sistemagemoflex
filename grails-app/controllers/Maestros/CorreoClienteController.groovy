package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CorreoClienteController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def planta = MaestroPlanta.get(params.planta)
		def cliente = MaestroCliente.get(params.cliente)
		def correo = MaestroCorreo.get(params.correo)
		def tipoInspeccion = MaestroTipoInspeccion.get(params.tipoInspeccion)
		
		def correosClientes = CorreoCliente.createCriteria().list(params)
		{
			if(cliente) { eq('cliente', cliente) }
			if(planta) { eq('planta', planta) }
			if(correo) { eq('email', correo) }
			if(tipoInspeccion) { eq('tipoInspeccion', tipoInspeccion) }
		}
		
		if(params.planta == '') { params.planta = null }
		if(params.correo == '') { params.correo = null }
		
		if(!correosClientes) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[correosClientes:correosClientes, params:params]	     
    }
 
    def show(CorreoCliente correoClienteInstance) { respond correoClienteInstance }

    def create() { respond new CorreoCliente(params) }

    @Transactional
    def save(CorreoCliente correoCliente) 
	{
        correoCliente.save flush:true, failOnError:true

        flash.message = "La asociacion se ha creado exitosamente."
        redirect correoCliente
    }

    def edit(CorreoCliente correoClienteInstance) {
        respond correoClienteInstance
    }

    @Transactional
    def update(CorreoCliente correoCliente) 
	{
        correoCliente.save flush:true

        flash.message = "La asociacion se ha actualizado exitosamente."
        redirect correoCliente
    }

    @Transactional
    def delete(CorreoCliente correoClienteInstance) 
	{
        if (correoClienteInstance == null) {
            notFound()
            return
        }

        correoClienteInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CorreoCliente.label', default: 'CorreoCliente'), correoClienteInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}