package Maestros

import java.util.concurrent.Phaser.QNode;

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder;

import com.sun.xml.internal.bind.v2.runtime.reflect.Accessor.GetterOnlyReflection;

import seguridad.Usuario;
import sistemagemoflex.ConsultasService

import jxl.Workbook
import jxl.write.Label
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook
import jxl.write.Number

class TransportistaConsultaController 
{
	ConsultasService consultasService
	
    //METODOS CONSULTA POR HISTORICO TRANSPORTISTA
	def index() 
	{ 
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()//Se Obtiene el permiso del usuario
		def name = auth.getName() //Obtengo el username
		def usuarioId = Usuario.findByUsername(name).id //Obtengo el id del usuario
		def cliente = Usuario.findByUsername(name).cliente //Obtengo el nombre del cliente
		def clienteId = cliente.id //Obtengo el id del cliente
		
		//TRANSPORTISTA
		def transportistas = MaestroTransportista.executeQuery("""select mt.descripcion from MaestroTransportista as mt, 
															   RepTransportista as rt, Usuario as u where rt.transportista = 
															   mt.id and rt.usuario = u.id and u.username = ?""", [name])
		
		def descripcionTransportista = consultasService.arregloString(transportistas)
		
		//PLANTAS
		def planta = MaestroPlanta.executeQuery(consultasService.plantaQuery(usuarioId, descripcionTransportista, clienteId))		
		def descripcionPlanta = consultasService.arregloString(planta)
		
		//TIPO DE INSPECCION
		def tipoInspeccion = MaestroTipoInspeccion.executeQuery(consultasService.tipoInspeccionQuery(descripcionTransportista, descripcionPlanta, clienteId))
				
		//RENDER
		render(view:"index", model:[transportistas:transportistas, descripcionTransportista:descripcionTransportista, permiso:permiso, 
			   name:name, cliente:cliente, tipoInspeccion:tipoInspeccion.sort { it }, planta:planta.sort { it.descripcion }, descripcionPlanta:descripcionPlanta])			
	}
	
	
	//M�todo consulta el historial de la flota del transportista por fecha y de una o todas las plantas	
	def consultaTransportista()
	{
		def idInspeccion = params.nInspeccion.toInteger()
		def fechaInicial = g.formatDate(date: params.fechaInicial, format: 'yyyy-MM-dd')
		def fechaFinal = g.formatDate(date: params.fechaFinal, format: 'yyyy-MM-dd')
		def plantasTotales = params.plantasTotales
		def cabecera
		def plantaSeleccionada = params.planta		
		def transportista = params.transportista
		
		if(plantaSeleccionada != '')
		{
			cabecera = InspeccionCabecera.executeQuery(consultasService.consultaHistoricaTransportista("'${plantaSeleccionada}'", 
													   transportista, fechaInicial, fechaFinal, idInspeccion))
		}
		else
		{				
			cabecera = InspeccionCabecera.executeQuery(consultasService.consultaHistoricaTransportista(plantasTotales, 
													   transportista, fechaInicial, fechaFinal, idInspeccion))
		}	
		
		render template:'consultaHistoricaTransportista', model:[cabecera:cabecera]		
	}
	
	//METODOS CONSULTA POR ITEM CRITICO TRANSPORTISTA
	def transportistaConsultaItem()
	{
		//VARIABLES
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()
		def name = auth.getName() //Obtengo el username
		def usuarioId = Usuario.findByUsername(name).id //Obtengo el id del usuario
		def cliente = Usuario.findByUsername(name).cliente //Obtengo el nombre del cliente
		def clienteId = cliente.id //Obtengo el id del cliente
		
		//TRANSPORTISTA
		def transportistas = MaestroTransportista.executeQuery("""select mt.descripcion from MaestroTransportista as mt, 
															   RepTransportista as rt, Usuario as u where rt.transportista = 
															   mt.id and rt.usuario = u.id and u.username = ?""", [name])
		
		def descripcionTransportista = consultasService.arregloString(transportistas)
		
		//PLANTA			
		def planta = MaestroPlanta.executeQuery(consultasService.plantaQuery(usuarioId, descripcionTransportista, clienteId))
		def descripcionPlanta = consultasService.arregloString(planta)
		
		//TIPO DE INSPECCION	
		def tipoInspeccion = MaestroTipoInspeccion.executeQuery(consultasService.tipoInspeccionQuery(descripcionTransportista, descripcionPlanta, clienteId))
		
		model:[transportistas:descripcionTransportista, tipoInspeccion:tipoInspeccion, cliente:cliente, plantas:descripcionPlanta]		
	}
	
	def listaItem()
	{
		//VARIABLES
		def tInspeccion = params.id.toInteger()		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()
		def name = auth.getName() //Obtengo el username
		def usuarioId = Usuario.findByUsername(name).id //Obtengo el id del usuario
		def cliente = Usuario.findByUsername(name).cliente //Obtengo el nombre del cliente
		def clienteId = cliente.id //Obtengo el id del cliente
		
		//Transportistas
		def transportistas = params.transportistas
		
		//Plantas
		def plantas = params.plantas
		def plantasTotales = params.plantas
		
		//N�mero	
		def numero = InspeccionCabecera.executeQuery("""select numero from InspeccionCabecera where idInspeccion = ${tInspeccion} 
													 and estado = 'APROBADO' and planta in (${plantas}) and transportista in 
													 (${transportistas}) and totalMalo > 0 group by numero order by 1""") 
		
		//ITEM A DESPLEGAR
		def itemsQuery = 
		"""
			select pregunta from InspeccionDetalle where idCabecera in (select id from InspeccionCabecera where idInspeccion = 
			${tInspeccion} and planta in (${plantas}) and transportista in (${transportistas})) and evaluacionItemBm = 'Malo'  
			group by pregunta order by 1
		"""
				
		def items = InspeccionDetalle.executeQuery(itemsQuery)
		
		plantas = plantas.substring(1, plantas.length() - 1).split("','").toList()
		
		//RENDER				
		render (template:'verItems', model:[items:items, tInspeccion:tInspeccion, plantas:plantas.sort { it }, numero:numero, transportistas:transportistas, plantasTotales:plantasTotales])
	}
	
		
	//METODO QUE ENTREGA LOS ITEMS MALOS
	def listadoPorItems()
	{
		//VARIABLES
		def inspeccion = params.nInspeccion.toInteger()
		def fechaInicial = g.formatDate(date: params.fechaInicial, format: 'yyyy-MM-dd')
		def fechaFinal = g.formatDate(date: params.fechaFinal, format: 'yyyy-MM-dd')
		def plantaSeleccionada = params.planta
		def plantas = params.plantas
		def item = params.items
		def listadoItemsPorFalla
		def numeroTanque =  params.tanque
		
		def query
		def consultapor
		def transportistas = params.transportistas
				
		//GRAN CONSULTA QUE DESPLIEGA LOS ITEMS
		if (numeroTanque == "")
		{
			if(item == "")
			{
				//IF ELSE NUMERO TANQUE NO, ITEM NO, PLANTA SI/NO
				if (plantaSeleccionada == "")
				{					
					query = """ select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
								d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
								c.idInspeccion = ${inspeccion} and c.planta in (${plantas}) and c.transportista in (${transportistas}) 
								and  c.id = d.idCabecera and  c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' 
								and d.evaluacionItemBm = 'malo' and c.estado = 'APROBADO' order by c.fechaInspeccion desc """			
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query)
					
					consultapor = "Consulta por todos los Tanques, Items y Plantas"
				}
				else
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
						c.idInspeccion = ${inspeccion} and c.planta = '${plantaSeleccionada}' and c.transportista in (${transportistas}) 
						and  c.id = d.idCabecera and  c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' 
						and d.evaluacionItemBm = 'malo' and c.estado = 'APROBADO' order by c.fechaInspeccion desc """		
					
					listadoItemsPorFalla = InspeccionCabecera.executeQuery(query)
					
					consultapor = "Consulta por todos los Tanques, Items y por Planta ${plantaSeleccionada}"					
				}
			}
			else 
			{
				//IF ELSE NUMERO CAMION NO, ITEM SI, PLANTA SI/NO
				if (plantaSeleccionada == "")
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.pregunta = ?
						and c.idInspeccion = ${inspeccion} and c.planta in (${plantas}) and c.transportista in (${transportistas}) 
						and  c.id = d.idCabecera and  c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' 
						and d.evaluacionItemBm = 'malo' and c.estado = 'APROBADO' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
					
					consultapor = "Consulta por todos los Tanques, Plantas y por Item ${item}"
				}
				else
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, d.critico,
						c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.evaluacionItemBm = 
						'malo' and d.pregunta = ? and c.id = d.idCabecera and c.transportista in (${transportistas}) and c.estado = 
						'APROBADO' and c.planta = '${plantaSeleccionada}' and c.idInspeccion = ${inspeccion} and 
						c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
					
					consultapor = "Consulta por todos los Tanques, por Planta ${plantaSeleccionada} y por Item ${item}"
				}
			}				
		}
		else
		{
			//IF ELSE NUMERO CAMION SI, ITEM SI/NO, PLANTA SI/NO
			if(item == "")
			{
				if (plantaSeleccionada == "")
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
						c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta in (${plantas}) 
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionCabecera.executeQuery(query)
					
					consultapor = "Consulta por Tanque Numero ${numeroTanque}, Todas las Plantas e Items "					
				}
				else
				{
					query = 
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
						c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta = '${plantaSeleccionada}'
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """			
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query)
					
					consultapor = "Consulta por Tanque Numero ${numeroTanque}, Planta ${plantaSeleccionada} y todos los Items "
				}
			}
			else 
			{
				if (plantaSeleccionada == "")
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.pregunta = ? 
						and c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta in (${plantas})
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
					
					consultapor = "Consulta por Tanque Numero ${numeroTanque}, Item ${item} y todas las Plantas"
				}
				else
				{
					query = 
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, 
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.pregunta = ? 
						and c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta = '${plantaSeleccionada}'
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """ 
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
					
					consultapor = "Consulta por Tanque Numero ${numeroTanque},  Item ${item} y la Planta ${plantaSeleccionada}"
				}
			}
		}
		
		//RENDER
		render(template:'listadoPorItems', model:[listadoItemsPorFalla:listadoItemsPorFalla, consultapor:consultapor, inspeccion:inspeccion,
		fechaInicial:fechaInicial, fechaFinal:fechaFinal, plantaSeleccionada:plantaSeleccionada, plantas:plantas, item:item,
		numeroTanque:numeroTanque, transportistas:transportistas])
	}
	
	//METODOS DEL RESUMEN DE INSPECCION
	def resumenInspeccion()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()//Se Obtiene el permiso del usuario
		def name = auth.getName() //Obtengo el username
		def usuarioId = Usuario.findByUsername(name).id //Obtengo el id del usuario
		def cliente = Usuario.findByUsername(name).cliente //Obtengo el nombre del cliente
		def clienteId = cliente.id //Obtengo el id del cliente
		def transportista = []
		
		//TRANSPORTISTA
		def transportistas = MaestroTransportista.executeQuery("""select mt.descripcion from MaestroTransportista as mt, 
															   RepTransportista as rt, Usuario as u where rt.transportista = 
															   mt.id and rt.usuario = u.id and u.username = ?""", [name])
		
		def descripcionTransportista = consultasService.arregloString(transportistas)
		
		//PLANTAS	
		def planta = MaestroPlanta.executeQuery(consultasService.plantaQuery(usuarioId, descripcionTransportista, clienteId))
		def descripcionPlanta = consultasService.arregloString(planta)
		
		//TIPO DE INSPECCION		
		def tipoInspeccion = MaestroTipoInspeccion.executeQuery(consultasService.tipoInspeccionQuery(
			descripcionTransportista, descripcionPlanta, clienteId))
				
		model:[permiso:permiso, name:name, cliente:cliente, tipoInspeccion:tipoInspeccion, planta:planta.sort { it.descripcion }, descripcionTransportista: descripcionTransportista, 
			  descripcionPlanta: descripcionPlanta]
	}
	
	def cabeceraResumen()
	{	
		if(params.inspeccion == "")
		{
			flash.message = "Debe seleccionar una Inspecci�n"
			redirect(action:'resumenInspeccion')
		}
		else
		{ 
			def idInspeccion = params.inspeccion.toInteger()
			def nombreInspeccion = MaestroTipoInspeccion.findById(idInspeccion).descripcion			
			def fechaInicial = g.formatDate(date: params.nfecha1, format: 'yyyy-MM-dd')
			def fechaFinal = g.formatDate(date: params.nfecha2, format: 'yyyy-MM-dd')
			def plantaSeleccionada = params.plantaS
			def promedioPuntaje = 0
			def promedioIO = 0
			def sumaCriticos = 0
			def contador = 0
			
			def cabecera //Variable que guarda la consulta
			
			//If que verifica si selecciono una planta o no
			if(plantaSeleccionada == "")
			{				
				cabecera = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte(params.plantasTotales, 
											 			  params.descripcionTransportista, fechaInicial, fechaFinal, idInspeccion))
			}
			else
			{
				cabecera = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte("'${plantaSeleccionada}'", 
											 			  params.descripcionTransportista, fechaInicial, fechaFinal, idInspeccion))
			}
	
			//If que verifica si se encontraron datos en la consulta o no
			if (cabecera.isEmpty())
			{
				flash.message2 = "No se encontraron datos"
				redirect(action:'resumenInspeccion')
				return true
			}
			else
			{
				def data = []
				def detalle = 0
				
				for(k in 0..cabecera.size - 1)
				{	
					promedioIO = promedioIO + cabecera[k][4]
					promedioPuntaje = promedioPuntaje + cabecera[k][3]
					sumaCriticos = sumaCriticos + cabecera[k][5]	
					contador++	
				}				
				
				promedioIO = Math.round((promedioIO/contador) * 100) / 100
				promedioPuntaje = Math.round((promedioPuntaje/contador) * 100) / 100
				fechaInicial = g.formatDate(date: params.nfecha1, format: 'dd-MM-yyyy')
				fechaFinal = g.formatDate(date: params.nfecha2, format: 'dd-MM-yyyy')
							
				cabecera.each 
				{
					j->
					data.add("date1":fechaInicial, "date2":fechaFinal, "nombreInspeccion":nombreInspeccion, "id":j[0],
							 "numero":j[1],	"fecha_hora_inspeccion":j[2], "puntaje_total":j[3], "transportista":j[6],
							 "critico_detalle":j[5], "indice_operacion":j[4], "planta":j[7], "promedioPuntaje":promedioPuntaje,
							 "promedioIO":promedioIO, "sumaCriticos":sumaCriticos)
				}		
				
				chain(controller:'jasper',action:'index', model:[data:data], params:params)
			}
		}	
	}
	
	//METODOS DE LA ULTIMA INSPECCION
	def ultimaInspeccion()
	{
		//USUARIO
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()//Se Obtiene el permiso del usuario
		def name = auth.getName() //Obtengo el username
		def usuarioId = Usuario.findByUsername(name).id //Obtengo el id del usuario
		def cliente = Usuario.findByUsername(name).cliente //Obtengo el nombre del cliente
		def clienteId = cliente.id //Obtengo el id del cliente
		
		//PLANTAS
		def plantasPrevias = MaestroPlanta.executeQuery("""select descripcion from MaestroPlanta where id in (select planta from 
														UsuarioPlanta where usuario = ${usuarioId})""")

		def descripcionPlantas = consultasService.arregloString(plantasPrevias)
		
		//TRANSPORTISTAS
		def transportistas = MaestroTransportista.executeQuery("""select mt.descripcion from MaestroTransportista as mt, 
															   RepTransportista as rt, Usuario as u where rt.transportista = 
															   mt.id and rt.usuario = u.id and u.username = '${name}'""")
		
		def transportistasFinales = consultasService.arregloString(transportistas)
		
		//TIPO DE INSPECCION
		def tipoInspeccion = MaestroTipoInspeccion.executeQuery("""from MaestroTipoInspeccion where id in (select idInspeccion from InspeccionCabecera 
																where estado = 'APROBADO' and planta in (${descripcionPlantas}) and transportista in (${transportistasFinales}))
																and cliente = ${clienteId}""")
							
		render(view:"ultimaInspeccion", model:[tipoInspeccion:tipoInspeccion, transportistas: transportistasFinales])
	}
	
	def resultadoUltimaInspeccion()
	{		
		def transportista = params.transportista
		def numeroInspeccion = params.numeroInspeccion
		
		def numeroFecha = InspeccionCabecera.executeQuery("""select numero, max(fechaInspeccion), planta from InspeccionCabecera 
														  where estado = 'APROBADO' and transportista in (${transportista}) and 
														  idInspeccion = ${numeroInspeccion} group by numero""") 
				
		render template: 'resultadoUltimaInspeccion', model:[numeroFecha: numeroFecha]
	}
	
	def bajadaExcelPorItem()
	{
		def inspeccion = params.inspeccion.toInteger()
		def fechaInicial = g.formatDate(date: params.fechaInicial, format: 'yyyy-MM-dd')
		def fechaFinal = g.formatDate(date: params.fechaFinal, format: 'yyyy-MM-dd')
		def plantaSeleccionada = (params.plantaSeleccionada == null) ? "" : params.plantaSeleccionada
		def plantas = params.plantas
		def item = (params.item == null) ? "" : params.item
		def numeroTanque =  (params.numeroTanque == null) ? "" : params.numeroTanque
		def transportistas = params.transportistas
		
		def listadoItemsPorFalla
		def query	
		
		//GRAN CONSULTA QUE DESPLIEGA LOS ITEMS
		if (numeroTanque == "")
		{
			if(item == "")
			{
				//IF ELSE NUMERO TANQUE NO, ITEM NO, PLANTA SI/NO
				if (plantaSeleccionada == "")
				{
					query = """ select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
								d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
								c.idInspeccion = ${inspeccion} and c.planta in (${plantas}) and c.transportista in (${transportistas}) 
								and  c.id = d.idCabecera and  c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' 
								and d.evaluacionItemBm = 'malo' and c.estado = 'APROBADO' order by c.fechaInspeccion desc """			
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query)
				}
				else
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
						c.idInspeccion = ${inspeccion} and c.planta = '${plantaSeleccionada}' and c.transportista in (${transportistas}) 
						and  c.id = d.idCabecera and  c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' 
						and d.evaluacionItemBm = 'malo' and c.estado = 'APROBADO' order by c.fechaInspeccion desc """		
					
					listadoItemsPorFalla = InspeccionCabecera.executeQuery(query)
				}
			}
			else
			{
				//IF ELSE NUMERO CAMION NO, ITEM SI, PLANTA SI/NO
				if (plantaSeleccionada == "")
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.pregunta = ?
						and c.idInspeccion = ${inspeccion} and c.planta in (${plantas}) and c.transportista in (${transportistas}) 
						and  c.id = d.idCabecera and  c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' 
						and d.evaluacionItemBm = 'malo' and c.estado = 'APROBADO' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
				}
				else
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, d.critico,
						c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.evaluacionItemBm = 
						'malo' and d.pregunta = ? and c.id = d.idCabecera and c.transportista in (${transportistas}) and c.estado = 
						'APROBADO' and c.planta = '${plantaSeleccionada}' and c.idInspeccion = ${inspeccion} and 
						c.fechaInspeccion between '${fechaInicial}' and '${fechaFinal}' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
				}
			}
		}
		else
		{
			//IF ELSE NUMERO CAMION SI, ITEM SI/NO, PLANTA SI/NO
			if(item == "")
			{
				if (plantaSeleccionada == "")
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
						c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta in (${plantas}) 
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionCabecera.executeQuery(query)
				}
				else
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where 
						c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta = '${plantaSeleccionada}'
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """			
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query)
				}
			}
			else
			{
				if (plantaSeleccionada == "")
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.pregunta = ? 
						and c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta in (${plantas})
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
				}
				else
				{
					query =
					""" select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista,
						d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d where d.pregunta = ? 
						and c.numero = ${numeroTanque} and c.idInspeccion = ${inspeccion} and c.planta = '${plantaSeleccionada}'
						and c.transportista in (${transportistas}) and c.id = d.idCabecera and c.fechaInspeccion 
						between '${fechaInicial}' and '${fechaFinal}' and d.evaluacionItemBm = 'malo' and 
						c.estado = 'APROBADO' order by c.fechaInspeccion desc """ 
					
					listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [item])
				}
			}
		}
		
		String nombre = "Listado_Por_Items.xls"
		response.setContentType('application/vnd.ms-excel')
		response.setHeader('Content-Disposition', 'Attachment;Filename=' + nombre)
		WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream)
		
		WritableSheet sheet1 = workbook.createSheet(g.formatDate(date:new Date(), format:'dd-MM-yyyy'), 0)
		
		sheet1.setColumnView(0, 15)
		sheet1.setColumnView(1, 45)
		sheet1.setColumnView(2, 45)
		sheet1.setColumnView(3, 15)
		sheet1.setColumnView(4, 15)
		sheet1.setColumnView(5, 45)
		sheet1.setColumnView(6, 15)
		
		sheet1.addCell(new Label(0, 0, "Fecha Inspeccion"))
		sheet1.addCell(new Label(1, 0, "Item"))
		sheet1.addCell(new Label(2, 0, "Observacion"))
		sheet1.addCell(new Label(3, 0, "Planta/Localidad"))
		sheet1.addCell(new Label(4, 0, "Numero Tanque"))
		sheet1.addCell(new Label(5, 0, "Transportista"))
		sheet1.addCell(new Label(6, 0, "Evaluacion"))
		
		listadoItemsPorFalla.eachWithIndex
		{
			detalle, i ->
			
			sheet1.addCell(new Label(0, 2 + i, g.formatDate(date:detalle[0], format:'dd-MM-yyyy')))
			sheet1.addCell(new Label(1, 2 + i, detalle[1]))
			sheet1.addCell(new Label(2, 2 + i, detalle[2]))
			sheet1.addCell(new Label(3, 2 + i, detalle[3]))
			sheet1.addCell(new Label(4, 2 + i, detalle[4]))
			sheet1.addCell(new Label(5, 2 + i, detalle[5]))
			
			if (detalle[6] == 0) { sheet1.addCell(new Label(6, 2 + i, 'Malo')) } else { sheet1.addCell(new Label(6, 2 + i, 'Critico')) }
		}
		
		workbook.write()
		workbook.close()
		
		return
	}
}