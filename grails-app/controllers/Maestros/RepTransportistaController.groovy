package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional
import seguridad.*

@Transactional(readOnly = true)
class RepTransportistaController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) 
	{
		params.max = 10
		
		def usuario = Usuario.get(params.usuario)
		def transportista = MaestroTransportista.get(params.transportista)
		
		def usuariosTransportistas = RepTransportista.createCriteria().list(params)
		{
			if(usuario) { eq('usuario', usuario) }
			if(transportista) { eq('transportista', transportista) }	
		}
		
		if(!usuariosTransportistas) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[usuariosTransportistas:usuariosTransportistas, params:params]
    }

    def show(RepTransportista asociacion) { model:[asociacion:asociacion] }

    def create() 
	{ 
		def cliente = MaestroCliente.get(params.cliente) 
		def transportistas = MaestroTransportista.findAllByClienteAndActivo(cliente, true).sort { it.descripcion } 
		def usuarios = Usuario.findAllByAccountLockedAndCliente(false, cliente).sort { it.username }
		
		respond new RepTransportista(params), model:[cliente:cliente, usuarios:usuarios, transportistas:transportistas] 
	}

    @Transactional
    def save(RepTransportista asociacion) 
	{
		if(RepTransportista.findByUsuarioAndTransportista(asociacion.usuario, asociacion.transportista))
		{
			flash.warning = "La asociaci\u00F3n que intenta ingresar ya se encuentra registrada en el Sistema. Favor revisar."
			redirect action:'create'
			return
		}

		asociacion.save flush:true, failOnError:true
		
		flash.message = "La asociaci\u00F3n se ha creado exitosamente."
		redirect asociacion
    }
	
    def edit(RepTransportista repTransportistaInstance)
	{		 
		def idcliente = repTransportistaInstance?.usuario?.cliente?.id
		def cliente = MaestroCliente.get(idcliente)
		def transportistaList = cliente?.transportista.sort { it.descripcion }
		
		respond repTransportistaInstance, model:[transportistaList:transportistaList]
    }
	
    @Transactional
    def update(RepTransportista asociacion) 
	{
        asociacion.save flush:true

        flash.message = "La asociaci\u00F3n se ha actualizado exitosamente."
        redirect asociacion
    }
	
    @Transactional
    def delete(RepTransportista asociacion) 
	{
        asociacion.delete flush:true
		
        flash.message = "La asociaci\u00F3n se ha eliminado exitosamente."
        redirect action:"index", method:"GET"
    }
}