package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CheckListController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def index(Integer max) 
	{		
        params.max = 10
		
		def tipoInspeccion = MaestroTipoInspeccion.get(params.tipoInspeccion)
		def clasificacion = MaestroClasificacion.findAllByDescripcionIlike('%' + params.clasificacion + '%')
		
		def preguntas = CheckList.createCriteria().list(params)
		{
			if(tipoInspeccion) { eq('tipoInspeccion', tipoInspeccion) }
			if(clasificacion) { 'in'('clasificacion', clasificacion) }
			if(params.pregunta) { ilike('pregunta', '%' + params.pregunta + '%') }
			if(params.critico == 'Si') { eq('critico', 1) } else if(params.critico == 'No') { eq('critico', 0) }
		}
		
        model:[preguntas:preguntas, params:params]
    }
	
    def show(CheckList checkListInstance) { respond checkListInstance }

    def create() { respond new CheckList(params) }

    @Transactional
    def save(CheckList checkList) 
	{        		
        checkList.save flush:true, failOnError:true

        flash.message = "La pregunta se ha creado exitosamente."
        redirect checkList
	}

    def edit(CheckList checkListInstance) { respond checkListInstance }

    @Transactional
    def update(CheckList checkList) 
	{        
        checkList.save flush:true
			
        flash.message = "La pregunta se ha actualizado exitosamente."
        redirect checkList        
	}

    @Transactional
    def delete(CheckList checkListInstance) 
	{
        checkListInstance.delete flush:true

        flash.message = "La pregunta se ha eliminado exitosamente."
        redirect action:"index", method:"GET"
	}
	
	@Transactional
	def copiarCorreos()
	{
		def nuevaInspeccion = MaestroTipoInspeccion.get(17)
		def correosCliente = CorreoCliente.findAllByClienteAndTipoInspeccion(MaestroCliente.get(1), MaestroTipoInspeccion.get(3))
		def correosTransportista = CorreoTransportista.findAllByTipoInspeccion(MaestroTipoInspeccion.get(3))
		
		println "CORREOS: $correosCliente"
		println "CORREOS2: $correosTransportista"
		
		correosCliente.each
		{
			dato ->
			
			new CorreoCliente(cliente:dato.cliente, tipoInspeccion:nuevaInspeccion, planta:dato.planta, email:dato.email).save failOnError:true, flush:true
		}	
		
		correosTransportista.each
		{
			dato ->
			
			new CorreoTransportista(transportista:dato.transportista, planta:dato.planta, tipoInspeccion:nuevaInspeccion, email:dato.email).save failOnError:true, flush:true
		}	
		println "LISTO"
	}
}