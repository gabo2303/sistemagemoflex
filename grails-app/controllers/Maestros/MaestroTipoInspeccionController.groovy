package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MaestroTipoInspeccionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	//Direcciona al list
    def index(Integer max) 
	{
		params.max = 10
		
		def tiposInspeccion = MaestroTipoInspeccion.createCriteria().list(params) {  }
		
		model:[tiposInspeccion:tiposInspeccion]
        
    }
	
    def show(MaestroTipoInspeccion maestroTipoInspeccionInstance) { respond maestroTipoInspeccionInstance }

    def create() { respond new MaestroTipoInspeccion(params) }

    @Transactional
    def save(MaestroTipoInspeccion tipoInspeccion) 
	{        
        tipoInspeccion.save flush:true

        flash.message = "El Tipo de Inspecci\u00F3n se ha creado exitosamente."
        redirect tipoInspeccion
    }

    def edit(MaestroTipoInspeccion maestroTipoInspeccionInstance) { respond maestroTipoInspeccionInstance }

    @Transactional
    def update(MaestroTipoInspeccion tipoInspeccion) 
	{        
        tipoInspeccion.save flush:true

        flash.message = "El Tipo de Inspecci\u00F3n se ha actualizado exitosamente."
        redirect tipoInspeccion       
    }

    @Transactional
    def delete(MaestroTipoInspeccion tipoInspeccion) 
	{
		tipoInspeccion.activo = !tipoInspeccion.activo
		
		if(params.accion == 'Activar') { flash.message = "El Tipo de Inspecci\u00F3n se ha Activado exitosamente." } else { flash.message = "El Tipo de Inspecci\u00F3n se ha Desactivado exitosamente." }
		
		redirect tipoInspeccion
    }
}