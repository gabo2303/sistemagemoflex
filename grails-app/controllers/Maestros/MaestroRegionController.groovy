package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MaestroRegionController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def regiones = MaestroRegion.createCriteria().list(params) { if(params.descripcion) { ilike('descripcion', '%' + params.descripcion + '%') } }
		
        model:[regiones:regiones]
    }

    def show(MaestroRegion maestroRegionInstance) { respond maestroRegionInstance }

    def create() { respond new MaestroRegion(params) }

    @Transactional
    def save(MaestroRegion maestroRegionInstance) 
	{
        if (maestroRegionInstance.hasErrors()) { respond maestroRegionInstance.errors, view:'create'; return }

        maestroRegionInstance.save flush:true

        request.withFormat 
		{
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'maestroRegion.label', default: 'MaestroRegion'), maestroRegionInstance.id])
                redirect maestroRegionInstance
            }
            '*' { respond maestroRegionInstance, [status: CREATED] }
        }
    }

    def edit(MaestroRegion maestroRegionInstance) { respond maestroRegionInstance }

    @Transactional
    def update(MaestroRegion maestroRegionInstance) {
        if (maestroRegionInstance == null) {
            notFound()
            return
        }

        if (maestroRegionInstance.hasErrors()) {
            respond maestroRegionInstance.errors, view:'edit'
            return
        }

        maestroRegionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MaestroRegion.label', default: 'MaestroRegion'), maestroRegionInstance.id])
                redirect maestroRegionInstance
            }
            '*'{ respond maestroRegionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MaestroRegion maestroRegionInstance) {

        if (maestroRegionInstance == null) {
            notFound()
            return
        }

        maestroRegionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MaestroRegion.label', default: 'MaestroRegion'), maestroRegionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}