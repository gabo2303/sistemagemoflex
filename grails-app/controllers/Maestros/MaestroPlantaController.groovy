package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional
import jxl.Workbook
import jxl.write.Label
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook

@Transactional(readOnly = true)
class MaestroPlantaController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def region = MaestroRegion.get(params.region)
		def cliente = MaestroCliente.get(params.cliente)
		
		def plantas = MaestroPlanta.createCriteria().list(params)
		{
			if(region) { eq('region', region) }
			if(cliente) { eq('cliente', cliente) }
			if(params.descripcion) { ilike('descripcion', '%' + params.descripcion + '%') }
		}
		
		if(!plantas) { flash.warning = "No se han encontrado resultados para la b\u00FAsqueda realizada." } 
		
		else { flash.message = "Se ha(n) encontrado ${plantas.totalCount} resultado(s) para la b\u00FAsqueda realizada." }
		
		model:[plantas:plantas]   
    }
	
    def show(MaestroPlanta planta) { model:[planta:planta] }

    def create() { respond new MaestroPlanta(params) }

    @Transactional
    def save(MaestroPlanta planta) 
	{
		if(MaestroPlanta.findByDescripcion(planta.descripcion.trim()))
		{
			flash.warning = "La descripci\u00F3n ingresada para la Planta ya se encuentra registrada en el Sistema. Favor revisar."
			forward action:'create'
			return
		}
		
        planta.save flush:true, failOnError:true

        flash.message = "La Planta se ha creado exitosamente."
        redirect planta
    }

    def edit(MaestroPlanta maestroPlantaInstance) { respond maestroPlantaInstance }

    @Transactional
    def update(MaestroPlanta planta) 
	{
        planta.save flush:true, failOnError:true

        flash.message = "La Planta se ha actualizado exitosamente."
        redirect planta
    }
	
    @Transactional
    def delete(MaestroPlanta planta) 
	{
        planta.activa = !planta.activa

		if(planta.activa) { flash.message = "La Planta se ha activado exitosamente." } else { flash.message = "La Planta se ha desactivado exitosamente." }
		
        redirect planta
    }
	
	def descargarExcel()
	{
		response.setContentType('application/vnd.ms-excel')
		response.setHeader('Content-Disposition', 'Attachment;Filename=' + 'Plantas.xls')
		WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream)
		
		WritableSheet sheet1 = workbook.createSheet("Plantas", 0)
		
		sheet1.setColumnView(0, 10)
		sheet1.setColumnView(1, 20)
		sheet1.setColumnView(2, 20)
		
		sheet1.addCell(new Label(0, 0, "Id"))
		sheet1.addCell(new Label(1, 0, "Descripcion"))
		sheet1.addCell(new Label(2, 0, "Region"))
		
		MaestroPlanta.createCriteria().list 
		{
			cliente { eq("razonSocial", "Copec") } 
			eq("activa", true) 
		}
		.eachWithIndex
		{
			registro, i ->
			
			sheet1.addCell(new Label(0, i + 2, registro.id.toString()))
			sheet1.addCell(new Label(1, i + 2, registro.descripcion))
			sheet1.addCell(new Label(2, i + 2, registro.region.descripcion))
		}
		
		workbook.write()
		workbook.close()
	}
}