package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MaestroClasificacionController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def clasificaciones = MaestroClasificacion.createCriteria().list(params) { }
		
		model:[clasificaciones:clasificaciones]
    }
	
    def show(MaestroClasificacion clasificacion) { model:[clasificacion:clasificacion] }

    def create() { respond new MaestroClasificacion(params) }

    @Transactional
    def save(MaestroClasificacion clasificacion) 
	{   
        clasificacion.save flush:true

        flash.message = "La Clasificaci\u00F3n se ha creado exitosamente."
        redirect clasificacion
    }

    def edit(MaestroClasificacion maestroClasificacionInstance) { respond maestroClasificacionInstance }

    @Transactional
    def update(MaestroClasificacion clasificacion) 
	{        
        clasificacion.save flush:true

        flash.message = "La Clasificaci\u00F3n se ha actualizado exitosamente."
        redirect clasificacion
    }

    @Transactional
    def delete(MaestroClasificacion clasificacion) 
	{
        clasificacion.delete flush:true

        flash.message = "La Clasificaci\u00F3n se ha eliminado exitosamente."
        redirect action:"index", method:"GET"
    }
}