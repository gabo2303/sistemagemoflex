package Maestros

import static org.springframework.http.HttpStatus.*
import seguridad.Usuario;
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsuarioPlantaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) 
	{
		params.max = 10
		
		def usuario = Usuario.get(params.usuario)
		def planta = MaestroPlanta.get(params.planta)
		
		def usuariosPlantas = UsuarioPlanta.createCriteria().list(params)
		{
			if(usuario) { eq('usuario', usuario) }
			if(planta) { eq('planta', planta) }
		}
		
		if(!params.planta) { params.planta = null }
		if(!params.usuario) { params.usuario = null }
		
		if(!usuariosPlantas) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[usuariosPlantas:usuariosPlantas, params:params]	
	}

    def show(UsuarioPlanta usuarioPlantaInstance) { respond usuarioPlantaInstance }

    
	def create() 
	{ 
		def plantas = MaestroPlanta.findAllByCliente(MaestroCliente.get(params.cliente))
		def usuarios = Usuario.findAllByCliente(MaestroCliente.get(params.cliente))
				
		respond new UsuarioPlanta(params), model:[plantas:plantas, usuarios:usuarios] 
	}

    @Transactional
    def save(UsuarioPlanta usuarioPlantaInstance) 
	{
        if (usuarioPlantaInstance.hasErrors()) { respond usuarioPlantaInstance.errors, view:'create'; return }
				
		def consulta = UsuarioPlanta.findByPlantaAndUsuario(usuarioPlantaInstance?.planta, usuarioPlantaInstance?.usuario)
			
		if(consulta)
		{							
			flash.warning = "\u00C9ste usuario ya se ha asignado a esta planta. Favor revisar."
			redirect (action:"create")					
		}
		else
		{
			//Sino se crea el registro
			usuarioPlantaInstance.save flush:true
			
			request.withFormat 
			{
				form multipartForm {
					flash.message = "La asociaci\u00F3n ha sido creada exitosamente."
					redirect usuarioPlantaInstance
				}
				'*' { respond usuarioPlantaInstance, [status: CREATED] }
			}
		}
	}	
		
    def edit(UsuarioPlanta usuarioPlantaInstance) 
	{		
		def plantas = MaestroPlanta.findAllByCliente(usuarioPlantaInstance.usuario.cliente)
		def usuarios = Usuario.findAllByCliente(usuarioPlantaInstance.usuario.cliente)
			
		respond usuarioPlantaInstance, model:[plantas:plantas, usuarios:usuarios]		
    }

    @Transactional
    def update(UsuarioPlanta usuarioPlantaInstance) 
	{
        if (usuarioPlantaInstance.hasErrors()) { respond usuarioPlantaInstance.errors, view:'edit'; return }

        usuarioPlantaInstance.save flush:true

        request.withFormat 
		{
            form multipartForm {
                flash.message = "La asociaci\u00F3n se ha actualizado exitosamente."
                redirect usuarioPlantaInstance
            }
            '*'{ respond usuarioPlantaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UsuarioPlanta usuarioPlantaInstance) 
	{
        usuarioPlantaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UsuarioPlanta.label', default: 'UsuarioPlanta'), usuarioPlantaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}