package Maestros

import grails.transaction.Transactional;
import static org.springframework.http.HttpStatus.*

import java.text.DateFormat
import java.text.SimpleDateFormat

import seguridad.*

import org.grails.plugins.excelimport.*
import org.apache.poi.ss.usermodel.*
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.poi.hssf.usermodel.HSSFHeader
import org.apache.poi.hssf.usermodel.HSSFPrintSetup
import org.apache.poi.hssf.usermodel.HeaderFooter

import jxl.Workbook 
import jxl.write.Label
import jxl.write.Number
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook

class ConsultaCriticosController 
{
	def springSecurityService
	def consultasService
	def excelImportService
	
    def index() { } 
	
	def consulta()
	{			
		def usuario = Usuario.findByUsername(springSecurityService.currentUser)
		
		def tipoInspeccion = MaestroTipoInspeccion.findAllByClienteAndIdNotEqualAndActivo(usuario.cliente, 13, true)
		def plantas = MaestroPlanta.findAllByDescripcionInList(UsuarioPlanta.findAllByUsuario(usuario).planta.descripcion) 
		def transportistas = MaestroTransportista.findAllByDescripcionInList(RepTransportista.findAllByUsuario(usuario).transportista.descripcion) 
		
		def listado = []; def listadoTotal = 0
		
		if(params.periodo) 
		{ 	
			listado = resultado(plantas, transportistas, params)		
			
			if(!listado) { flash.warning = "No hay datos a mostrar." } else { listadoTotal = listado.getTotalCount() }
		}
		
		model:[tipoInspeccion:tipoInspeccion, plantas:plantas.sort { it.descripcion }, transportistas:transportistas.sort { it.descripcion }, listado:listado, listadoTotal:listadoTotal, params:params]
	}
	
	def resultado(plantas, transportistas, params)
	{	
		def tipoInspeccion
		
		if(params.plantas != '') { plantas = [params.plantas] } else { plantas = plantas.descripcion }
		
		if(params.transportistas != '') { transportistas = [params.transportistas] } else { transportistas = transportistas.descripcion }
				
		if(params.tipoInspeccion != '') { tipoInspeccion = [params.tipoInspeccion.toInteger()] } else { tipoInspeccion = [1, 2, 3, 4, 6, 9, 12] }
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
		
		def fechaInicial = formatter.parse(formatter.format(params.periodo))	
		def fechaFinal = params.periodo
		
		fechaFinal[Calendar.MONTH] = fechaFinal[Calendar.MONTH] + 1 
		fechaFinal = formatter.parse(formatter.format(fechaFinal))
		
		def cabecerasPeriodo = InspeccionCabecera.createCriteria().list
		{
			if(params.numero) { eq('numero', params.numero) } 
			between('fechaInspeccion', fechaInicial, fechaFinal) eq('estado', 'APROBADO') 'in'('idInspeccion', tipoInspeccion) 'in'('transportista', transportistas) 'in'('planta', plantas)
			
			projections { property('id') }			
		}
		
		params.max = 10
		
		def detallesMalosPaginados
			
		if(params.descargar)
		{
			if(cabecerasPeriodo)
			{
				detallesMalosPaginados = InspeccionDetalle.createCriteria().list
				{
					if(params.item) { ilike('pregunta', '%' + params.item + '%') }
					
					if(params.evaluacion == 'Malos') { eq('critico', 0) } else if(params.evaluacion == 'Criticos') { eq('critico', 1) }
					
					eq('evaluacionItemBm', 'Malo')
					
					idCabecera { 'in'('id', cabecerasPeriodo) }
				}
			}
			else { detallesMalosPaginados = [] }
			
			String nombre = "Fallas_Reiterativas.xls"
			response.setContentType('application/vnd.ms-excel')
			response.setHeader('Content-Disposition', 'Attachment;Filename=' + nombre)
			WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream)
			
			WritableSheet sheet1 = workbook.createSheet(g.formatDate(date:new Date(), format:'dd-MM-yyyy'), 0)
			
			sheet1.setColumnView(0, 15)
			sheet1.setColumnView(1, 15)
			sheet1.setColumnView(2, 20)
			sheet1.setColumnView(4, 50)
			sheet1.setColumnView(5, 60)
			
			sheet1.addCell(new Label(0, 0, "Fecha Inspeccion"))
			sheet1.addCell(new Label(1, 0, "Planta"))
			sheet1.addCell(new Label(2, 0, "Transportista"))
			sheet1.addCell(new Label(3, 0, "Equipo"))
			sheet1.addCell(new Label(4, 0, "Item Critico"))
			sheet1.addCell(new Label(5, 0, "Detalle"))
			sheet1.addCell(new Label(6, 0, "Repeticion"))
					
			detallesMalosPaginados.eachWithIndex
			{
				detalle, i ->
				
				sheet1.addCell(new Label(0, 2 + i, g.formatDate(date:detalle.idCabecera.fechaInspeccion, format:'dd-MM-yyyy')))
				sheet1.addCell(new Label(1, 2 + i, detalle.idCabecera.planta))
				sheet1.addCell(new Label(2, 2 + i, detalle.idCabecera.transportista))
				sheet1.addCell(new Label(3, 2 + i, detalle.idCabecera.numero))
				sheet1.addCell(new Label(4, 2 + i, detalle.pregunta))
				sheet1.addCell(new Label(5, 2 + i, detalle.observacion))
				sheet1.addCell(new Number(6, 2 + i, detalle.criticoRepeticion))			
			}
			
			workbook.write()
			workbook.close()
			
			return
		}
		else
		{
			if(cabecerasPeriodo)
			{		
				detallesMalosPaginados = InspeccionDetalle.createCriteria().list(params)
				{
					if(params.item) { ilike('pregunta', '%' + params.item + '%') }
					
					if(params.evaluacion == 'Malos') { eq('critico', 0) } else if(params.evaluacion == 'Criticos') { eq('critico', 1) }			
					
					eq('evaluacionItemBm', 'Malo') 
					
					idCabecera { 'in'('id', cabecerasPeriodo) } 
				}		
			}
			else { detallesMalosPaginados = [] }
			
			params.periodo[Calendar.MONTH] = params.periodo[Calendar.MONTH] - 1
			
			return detallesMalosPaginados
		}
	}
}