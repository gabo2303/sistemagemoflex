package Maestros

import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin;
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder;

class IndexController 
{
	def springSecurityService 
	def consultasService
	
    def index() 
	{		
		def permiso = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString(); def anio; def cliente
		def fechaActual = g.formatDate(date:new Date(), format:'MM-yyyy'); def mesActual = new Date()[Calendar.MONTH] + 1; def mesInicial
		def periodo = g.formatDate(date:params.periodo, format:'MM-yyyy')
		
		if(permiso != "[ROLE_ANONYMOUS]") { cliente = seguridad.Usuario.findByUsername(springSecurityService.currentUser).cliente.razonSocial  }
		
		if((mesActual - 9) < 0) { mesInicial = 11 + (mesActual - 8); anio = new Date()[Calendar.YEAR] - 1 } else { mesInicial = mesActual - 8; anio = new Date()[Calendar.YEAR] }
		
		if((permiso == "[ROLE_SUPERADMINISTRADOR]" || permiso == "[ROLE_CLIENTE]" || permiso == "[ROLE_ADMINISTRADOR]" || permiso == "[ROLE_SUPERVISOR]") && cliente == 'Copec')
		{
			def cantidades = []; def data = []; def mes; def planta; def transportista
					
			cliente = seguridad.Usuario.findByUsername(springSecurityService.currentUser).cliente
			def plantasTotales = MaestroPlanta.findAllByCliente(cliente).descripcion
			def transportistasTotales = MaestroTransportista.findAllByCliente(cliente).razonSocial
			
			if(!params.plantaSeleccionada) { planta = consultasService.arregloString(plantasTotales) } else { planta = "'${params.plantaSeleccionada}'" }
			
			if(!params.transportistaSeleccionado) { transportista = consultasService.arregloString(transportistasTotales) } else { transportista = "'${params.transportistaSeleccionado}'" }
			
			if(!periodo) { periodo = fechaActual }
			
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (1, 2) and planta in (${planta}) 
										  				   and transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
										  
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (3, 15, 16, 17) and planta in (${planta}) and 
								  						   transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
			
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 4 and planta in (${planta}) and 
									  					   transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
									  
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 6 and planta in (${planta}) and 
								 						   transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
								 
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 9 and planta in (${planta}) and 
														   transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
			
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 12 and planta in (${planta}) and 
									    				   transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
										
			cantidades.add(InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 13 and planta in (${planta}) and 
										 				   transportista in (${transportista}) and Date_Format(fechaInspeccion, '%m-%Y') = '${periodo}' """).count { it })
			
			9.times
			{				
				mesInicial += 1; if(mesInicial == 13) { mesInicial = 1; anio += 1 }
					
				def cantidad1 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (3, 15, 16, 17) and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad2 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (1, 2) and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad3 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 4 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad4 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 6 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad5 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 9 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad6 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 12 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
									
				def cantidad7 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 13 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }	
								
				switch(mesInicial)
				{
					case 1: mes = "Enero"
					break
					case 2: mes = "Febrero"
					break
					case 3: mes = "Marzo"
					break
					case 4:	mes = "Abril"
					break
					case 5:	mes = "Mayo"
					break
					case 6:	mes = "Junio"
					break
					case 7:	mes = "Julio"
					break
					case 8:	mes = "Agosto"
					break
					case 9:	mes = "Septiembre"
					break
					case 10: mes = "Octubre"
					break
					case 11: mes = "Noviembre"
					break
					default: mes = "Diciembre"
					break
				}
							
				data.add([mes, cantidad1, cantidad2, cantidad3, cantidad4, cantidad5, cantidad6, cantidad7])
			}
			
			model:[cantidades:cantidades, data:data, plantas:plantasTotales, transportistas:transportistasTotales, params:params, cliente:cliente]
		}
		else if(permiso == "[ROLE_TRANSPORTISTA]" && cliente == 'Copec')
		{
			def indicesEliminar = []; def data = []; def mes; def planta; def transportista; def columnas = [["string","Mes"]]
			
			def usuario = seguridad.Usuario.findByUsername(springSecurityService.currentUser)
			def plantasTotales = UsuarioPlanta.findAllByUsuario(usuario).planta.descripcion
			def transportistasTotales = RepTransportista.findAllByUsuario(usuario)?.transportista.razonSocial
			
			if(!params.plantaSeleccionada) { planta = consultasService.arregloString(plantasTotales) } else { planta = "'${params.plantaSeleccionada}'" }
			
			if(!params.transportistaSeleccionado) { transportista = consultasService.arregloString(transportistasTotales) } else { transportista = "'${params.transportistaSeleccionado}'" }
			
			9.times
			{
				mesInicial += 1; if(mesInicial == 13) { mesInicial = 1; anio += 1 }
				
				def cantidad1 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (3, 15, 16, 17) and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad2 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (1, 2) and extract(month from 
								fechaInspeccion) = '${mesInicial}' and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
				
				def cantidad3 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 4 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad4 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 6 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad5 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 9 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
								
				def cantidad6 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 12 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
										
			    def cantidad7 = InspeccionCabecera.executeQuery("""select id from InspeccionCabecera where estado = 'APROBADO' and idInspeccion = 13 and extract(month from fechaInspeccion) = '${mesInicial}' 
								and extract(year from fechaInspeccion) = '${anio}' and planta in (${planta}) and transportista in (${transportista})""").count { it }
												
				switch(mesInicial)
				{
					case 1: mes = "Enero"
					break
					case 2: mes = "Febrero"
					break
					case 3: mes = "Marzo"
					break
					case 4:	mes = "Abril"
					break
					case 5:	mes = "Mayo"
					break
					case 6:	mes = "Junio"
					break
					case 7:	mes = "Julio"
					break
					case 8:	mes = "Agosto"
					break
					case 9:	mes = "Septiembre"
					break
					case 10: mes = "Octubre"
					break
					case 11: mes = "Noviembre"
					break
					default: mes = "Diciembre"
					break
				}			
								
				data.add([mes, cantidad1, cantidad2, cantidad3, cantidad4, cantidad5, cantidad6, cantidad7])
			}
			
			for(i in 1..7) { if(data.sum{it[i]} == 0) { indicesEliminar.add(i) } }
			
			indicesEliminar.reverse().each { indice -> data.each { arreglo -> arreglo.remove(indice) } }
						
			for (i in 1..7)
			{
				if(!indicesEliminar.contains(i))
				{
					switch(i)
					{
						case 1:	columnas.add(["number","Gr\u00E1fica"])
						break
						case 2: columnas.add(["number","Liviano-Petr\u00F3leo"])
						break
						case 3: columnas.add(["number","Lubricantes Quintero"])
						break
						case 4: columnas.add(["number","Red Tae"])
						break
						case 5: columnas.add(["number","CD Lubricantes"])
						break
						case 6: columnas.add(["number","Gr\u00E1fica Lubricantes Quintero"])
						break
						default: columnas.add(["number","Veh\u00FAculo en Taller"])
						break
					}
				}
			}
			
			model:[data:data, plantas:plantasTotales, transportistas:transportistasTotales, params:params, columnas:columnas, cliente:usuario.cliente]
		}
		else { render view:'index' }
	}	
}