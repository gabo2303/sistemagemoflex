package Maestros

import java.text.DateFormat
import java.text.SimpleDateFormat

import javax.management.Query
import javax.validation.metadata.ReturnValueDescriptor

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder

import org.apache.commons.io.FileUtils

import seguridad.Usuario

import java.util.LinkedHashMap

import grails.transaction.Transactional
import groovy.sql.Sql

import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.codehaus.groovy.grails.plugins.jasper.JasperService

import transaccionales.EnvioCorreo

@Transactional(readOnly = false)
class InspeccionController 
{
	def springSecurityService
	def jasperService
	def mailService
	
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	//Muestra el listado de los tipos de inspecciones segun el usuario conectado
	def index() 
	{
		def usuario = Usuario.findByUsername(springSecurityService.currentUser?.username)
		def tipoInspeccion = usuario.cliente?.tipoInspeccion.findAll { it.activo }.sort { it.descripcion }
		def planta = UsuarioPlanta.findAllByUsuarioAndPlantaInList(usuario, MaestroPlanta.findAllByCliente(usuario.cliente)).planta
		
		model:[usuario:usuario, tipoInspeccion:tipoInspeccion, planta:planta]
	}
	
	//M�todo que obtiene el listado de los transportistas seg�n la planta seleccionada.
	def transPlanta()
	{		
		def cliente = MaestroCliente.findByRazonSocial(params.cliente)		
		def planta = MaestroPlanta.findByDescripcion(params.planta)
		
		//Genera la lista de los transportistas que se encuentran en la planta seleccionada.
		def transportistas = TransportistaPlanta.findAllByTransportistaInListAndPlanta(MaestroTransportista.findAllByCliente(cliente), planta)?.transportista
		
		//Genera la lista de las estaciones de servicio segun la planta seleccionada.
		def estaciones = EstacionServicioRedTae.findAllByLocalidad(planta)		

		render(template:'listTransportistas', model:[estaciones:estaciones, transportistas:transportistas, nombrePlanta:params.planta, tipoInspeccion:params.tipoInspeccion])
	}

	@Transactional
	//Metodo que genera las listas de camion, Tracto y tanque segun el transportista seleccionado.
	def transportista()
	{
		def cliente = Usuario.findByUsername(springSecurityService.currentUser).cliente 
		def transportista = MaestroTransportista.get(params.transportista)
		def planta = MaestroPlanta.findByDescripcion(params.nombrePlanta)
		
		def camiones = MaestroCamion.findAllByTransportistaAndPlanta(transportista, planta)
		
		render template:"listCamiones", model:[camiones:camiones, transport:transportista, cliente:cliente]
	}

	//Metodo que obtiene la lista de las preguntas segun el tipo de inspeccion
	@Transactional
	def items() 
	{		
		def tipoInspeccion = MaestroTipoInspeccion.get(params.id)
		 	
		//Listado de los usuarios
		def preguntas = CheckList.findAllByTipoInspeccion(tipoInspeccion)
		
		render(template:"listItems", model:[tipoInspeccion:tipoInspeccion, preguntas:preguntas, contador:preguntas.size()])
	}

	//Guarda las Inspecciones
	@Transactional
	def save()
	{	
		def verificador = false; def ultimaInspeccion; def cabecera; def cantidadCritico = 0; def totalMalo = 0; def puntajeTotal = 0
						
		def camionTanque = MaestroCamion.get(params.camionTanque)
		def tanque = MaestroCamion.get(params.tanque)
		def tracto = MaestroCamion.get(params.tracto)
		
		if(Usuario.findByUsername(springSecurityService.currentUser).cliente.razonSocial == 'Lipigas') { params.estado = 'APROBADO' }
		
		if (camionTanque) 
		{ 						
			cabecera = new InspeccionCabecera
			(
				usuario:params.usuario, planta:params.planta, comentario:params.comentarios, estado:params.estado, nEstacionServicio:params.eServicio, 
				conductor:params.nombreConductor, fechaInspeccion:params.fecha as Date, idInspeccion:params.nInspeccion, transportista:MaestroTransportista.get(params.transporte).descripcion,
				transportistaObjeto:MaestroTransportista.get(params.transporte), plantaObjeto:MaestroPlanta.findByDescripcion(params.planta),
											  
				cEstanque:"${camionTanque.numero} | ${camionTanque.patente} | ${camionTanque.marca} | ${camionTanque.capacidad}",
				numero:camionTanque.numero, patente:camionTanque.patente, marca:camionTanque.marca, capacidad:camionTanque.capacidad, tracto:'No Aplica', 
				tanque:'No Aplica', trNumero:'No Aplica', trMarca:'No Aplica', trPatente:'No Aplica', 
						   
				productoCarga:params.pCarga, tipoCarguio:params.tCarguio, succionPropia:params.sPropia, cargaOtroMedio:params.cOMedio, 
				fabricanteMeter:params.fMeter, modeloMeter:params.mMeter, serieMeter:params.sMeter, ticketPrinterMeter:params.tPrintMeter
			).save()
		}
		else
		{
			cabecera = new InspeccionCabecera
			(
				usuario:params.usuario, planta:params.planta, comentario:params.comentarios, estado:params.estado, nEstacionServicio:params.eServicio, 
				conductor:params.nombreConductor, fechaInspeccion:params.fecha as Date, idInspeccion:params.nInspeccion, transportista:MaestroTransportista.get(params.transporte).descripcion,
				transportistaObjeto:MaestroTransportista.get(params.transporte), plantaObjeto:MaestroPlanta.findByDescripcion(params.planta),
					   
				cEstanque:'No aplica', numero:tanque.numero, patente:tanque.patente, marca:tanque.marca, capacidad:tanque.capacidad, tracto:"${tracto.numero} | ${tracto.patente} | ${tracto.marca}",
				tanque:"${tanque.numero} | ${tanque.patente} | ${tanque.marca} | ${tanque.capacidad}", trNumero:tracto.numero, trMarca:tracto.marca, trPatente:tracto.patente,
				
				productoCarga:params.pCarga, tipoCarguio:params.tCarguio, succionPropia:params.sPropia, cargaOtroMedio:params.cOMedio, 
				fabricanteMeter:params.fMeter, modeloMeter:params.mMeter, serieMeter:params.sMeter, ticketPrinterMeter:params.tPrintMeter
			).save()	
		}
		
		if(cabecera.idInspeccion != 13 && cabecera.idInspeccion != 7 && cabecera.idInspeccion != 8 && cabecera.idInspeccion != 10 && cabecera.idInspeccion != 11  && cabecera.idInspeccion != 14)
		{
			ultimaInspeccion = InspeccionCabecera.createCriteria().get
			{
				eq('idInspeccion', cabecera.idInspeccion) eq('numero', cabecera.numero) eq('estado', 'APROBADO') 
				
				projections { max('fechaInspeccion') }
			}
			
			if(ultimaInspeccion)
			{
				ultimaInspeccion = InspeccionCabecera.createCriteria().get
				{
					eq('idInspeccion', cabecera.idInspeccion) eq('numero', cabecera.numero) 
					eq('estado', 'APROBADO') eq('fechaInspeccion', ultimaInspeccion)
				}
				
				if(ultimaInspeccion) { verificador = true }
			}	
			else { verificador = false }
		}
		
		cabecera.validate()

		if(cabecera.hasErrors()) { flash.warning = "Error al guardar los datos." }
		
		def checklist = CheckList.findAllByTipoInspeccion(MaestroTipoInspeccion.get(cabecera.idInspeccion)) 
		
		checklist.each
		{				
			InspeccionDetalle detalle = new InspeccionDetalle()
			detalle.pregunta = it.pregunta
			
			def repeticion = 0
			
			if(params."p${it.id}" == 'Malo') 
			{ 				
				if(it.critico == 1) { cantidadCritico += 1 }				
				repeticion = 1 
				totalMalo += 1
				puntajeTotal += it.malo
				detalle.evaluacionItem = it.malo
			}
			else 
			{ 
				detalle.evaluacionItem = it.bueno
				puntajeTotal += it.bueno 
			}		
				
			if(verificador && repeticion == 1) 
			{	
				def pregunta = it.pregunta

				def detallePrevio = InspeccionDetalle.createCriteria().get
				{
					eq('idCabecera', ultimaInspeccion)
					eq('pregunta', pregunta)
					eq('evaluacionItemBm', 'Malo')					
				} 
			
				if(detallePrevio) { repeticion += detallePrevio.criticoRepeticion }
			}
			
			detalle.idCabecera = cabecera			
			detalle.evaluacionItemBm = params."p${it.id}" 
			detalle.critico = it.critico
			detalle.observacion = params."o${it.id}"
			detalle.criticoRepeticion = repeticion
			detalle.bueno = it.bueno
			detalle.malo = it.malo
			detalle.puntajeItemBueno = it.bueno
				
			detalle.validate()

			if(!detalle.hasErrors()) { detalle.save(flush:true) } else { flash.warning = "Ha ocurrido un error." }
		}
		
		cabecera.totalCriticos = cantidadCritico
		cabecera.totalItemsEvaluados = checklist.size
		cabecera.totalBueno = checklist.size - totalMalo
		cabecera.totalMalo = totalMalo	
		cabecera.puntajeTotal = puntajeTotal
		
		if(cabecera.idInspeccion == 1) { cabecera.indiceOperacion = Math.round(((puntajeTotal * 100) / 5925) * 100) / 100 } //Math.round((0.017622 * puntajeTotal) * 100) / 100 }		
		else if(cabecera.idInspeccion == 2) { cabecera.indiceOperacion = Math.round(((puntajeTotal * 100) / 4850) * 100) / 100 } //Math.round((0.021742 * puntajeTotal) * 100) / 100 }
		else if(cabecera.idInspeccion == 3) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 52) / 2.37) * 100) / 100 }
		else if(cabecera.idInspeccion == 4) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 78) / 4.3) * 100) / 100 }
		else if(cabecera.idInspeccion == 5) { cabecera.indiceOperacion = Math.round(((1.55039 * 0.01) * puntajeTotal) * 100) / 100 }
		else if(cabecera.idInspeccion == 6) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 61) / 3.14) * 100) / 100}
		else if(cabecera.idInspeccion == 9) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 37) / 1.58) * 100) / 100 }
		else if(cabecera.idInspeccion == 12) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 54) / 2.87) * 100) / 100 }
		else if(cabecera.idInspeccion == 15 || cabecera.idInspeccion == 16) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 54) / 2.27) * 100) / 100 }
		else if(cabecera.idInspeccion == 17) { cabecera.indiceOperacion = Math.round((100 - (puntajeTotal - 56) / 2.31) * 100) / 100 }
		else if(cabecera.idInspeccion == 18) { cabecera.indiceOperacion = Math.round(((puntajeTotal * 100) / 4700) * 100) / 100 }
		else { cabecera.indiceOperacion = 0 }
				
		flash.message = "El Informe fue guardado exitosamente."	
		
		redirect action:'index'		
	}
	
	@Transactional
	//Funci�n que sirve para generar los reportes en distintos formatos
	def reporte()
	{	
		def boolTransp = false;			def boolCliente = false;		params._format = 'XLS'		
		
		def cabecera = InspeccionCabecera.findById(params.cabecera as Integer)
		params.nombreArchivo = "${cabecera.numero} - ${cabecera.planta} - [${g.formatDate(date:cabecera.fechaInspeccion, format: 'dd-MM-yyyy')}] - ${Maestros.MaestroTipoInspeccion.findById(cabecera.idInspeccion).descripcion}" 
		params.transportista = cabecera.transportista;		params.numero = cabecera.numero;		params.tipoInspeccion = cabecera.idInspeccion
		def tipoInspeccion = MaestroTipoInspeccion.get(cabecera.idInspeccion).descripcion
		
		//Real Path Imagen sirve para mostrar logo gemoflex en el reporte
		def realPath = servletContext.getRealPath("images")
		params.realPath = realPath
		
		def data = []; 			def dataFotos = [];		def item = [];		def puntaje = [];		def evaluacion = []; 	
		def observacion = [];	def critico = [];		def imagen = [];	def imagen2 = [];		def puntajeItemBueno = []

		//Guarda todos los datos encontrados id cabecera, de la tabla inspeccion detalle
		def detalle = InspeccionDetalle.findAllByIdCabecera(cabecera)
		
		def foto = Image.findAllByIdCabecera(cabecera)//Busca y guarda todas las imagenes con el id cabecera seleccionado
		def contFoto = foto.size() //Contador para recorrer las imagenes
		
		if(cabecera.enviado == false) { cabecera.enviado = true; params.enviado = false } else { params.enviado = true }
		
		cabecera.save(flush:true)
		
		//Recorre la lista detalle y asigna a cada arreglo un valor
		detalle.each 
		{
			j->

			item << j.pregunta
			puntaje << j.evaluacionItem
			evaluacion << j.evaluacionItemBm
			observacion << j.observacion
			puntajeItemBueno << j.puntajeItemBueno
			critico << j.critico
		}

		//Recorre la lista foto y asgina los valores al arreglo fotos
		def contador = 0
		foto.each { f-> if(contador % 2 == 0) { imagen<<f.foto } else { imagen2<<f.foto }; contador++ }
		
		if(!detalle.isEmpty())
		{
			//For que sirve para recorrer los arreglos
			for(i in 0..detalle.size - 1)
			{
				def mapaValores = 
				[
					"usuario":cabecera.usuario, "tipo_inspeccion":tipoInspeccion, "planta":cabecera.planta, "tracto":cabecera.tracto, "tanque":cabecera.tanque, 
					"fecha_hora_inspeccion":cabecera.fechaInspeccion, "transportista":cabecera.transportista, 
					"c_estanque":cabecera.cEstanque, "n_estacion_servicio":cabecera.nEstacionServicio, "conductor":cabecera.conductor,
					"pregunta":item.get(i), "evaluacion_item":puntaje.get(i), "evaluacion_item_bm":evaluacion.get(i), "puntaje_item_bueno":puntajeItemBueno.get(i),
					"observacion":observacion.get(i), "critico":critico.get(i), "id":cabecera.id
				]
	
				data.add(mapaValores) //Se llena el arreglo data con los valores mapaValores.
			}
		}
		else
		{
			def mapaValores =
			[
				"usuario":cabecera.usuario, "tipo_inspeccion":tipoInspeccion, "planta":cabecera.planta, "tracto":cabecera.tracto, "tanque":cabecera.tanque,
				"fecha_hora_inspeccion":cabecera.fechaInspeccion, "transportista":cabecera.transportista,
				"c_estanque":cabecera.cEstanque, "n_estacion_servicio":cabecera.nEstacionServicio, "conductor":cabecera.conductor,
				"pregunta":0, "evaluacion_item":0, "evaluacion_item_bm":0, "observacion":"Sin observaciones.", "critico":0, "id":cabecera.id
			]
			
			data.add(mapaValores)
		}
		
		if(imagen.size > 0)
		{	
			//for que recorre el arreglo fotos
			for(x in 0..imagen.size - 1)
			{
				def mpValores =	[ "imagen":imagen.get(x) ]
			
				if(x == 0) { mpValores << ["inicio":0] }
				
				if(x < imagen2.size) { mpValores << ["imagen2":imagen2.get(x)] }
				
				data.add(mpValores)//Se llenan el arreglo data con los valores de mpValores				
			}
			
			def mapValores =
			[
				"id":cabecera.id,
				"comentario":cabecera.comentario,
				"total_bueno":cabecera.totalBueno,
				"total_malo":cabecera.totalMalo,
				"critico_detalle":cabecera.totalCriticos.toInteger(),
				"total_items_evaluados":cabecera.totalItemsEvaluados,
				"puntaje_total":cabecera.puntajeTotal,
				"indice_operacion":cabecera.indiceOperacion,
				"p_carga":cabecera.productoCarga,
				"t_carguio":cabecera.tipoCarguio,
				"s_propia":cabecera.succionPropia,
				"c_o_medio":cabecera.cargaOtroMedio,
				"f_meter":cabecera.fabricanteMeter,
				"m_meter":cabecera.modeloMeter,
				"s_meter":cabecera.serieMeter,
				"t_print_meter":cabecera.ticketPrinterMeter,
				"idInspeccion":cabecera.idInspeccion
			]
			
			data.add(mapValores)
		}
		else
		{
			def mpValores =
			[
				"id":cabecera.id,
				"comentario":cabecera.comentario,
				"total_bueno":cabecera.totalBueno,
				"total_malo":cabecera.totalMalo,
				"critico_detalle":cabecera.totalCriticos.toInteger(),
				"total_items_evaluados":cabecera.totalItemsEvaluados,
				"puntaje_total":cabecera.puntajeTotal,
				"indice_operacion":cabecera.indiceOperacion,
				"p_carga":cabecera.productoCarga,
				"t_carguio":cabecera.tipoCarguio,
				"s_propia":cabecera.succionPropia,
				"c_o_medio":cabecera.cargaOtroMedio,
				"f_meter":cabecera.fabricanteMeter,
				"m_meter":cabecera.modeloMeter,
				"s_meter":cabecera.serieMeter,
				"t_print_meter":cabecera.ticketPrinterMeter,
				"idInspeccion":cabecera.idInspeccion
			]
			
			data.add(mpValores)//Se llenan el arreglo data con los valores de mpValores				
		}		
		
		if(params.correoFinal != null) 
		{ 
			params.correoFinal.substring(1, params.correoFinal.length() - 1).split(',').each 
			{ 
				
				correo -> if(CorreoTransportista.findByEmail(MaestroCorreo.findByCorreo(correo.trim())) != null) { boolTransp = true }
						  else if(CorreoCliente.findByEmail(MaestroCorreo.findByCorreo(correo.trim())) != null) { boolCliente = true }
						  println correo
						  println boolTransp
						  println boolCliente
			} 
		}
		
		if((!params.enviado && boolCliente && boolTransp) || params.enviado) 
		{ 
			params.nombreInspeccion = Maestros.MaestroTipoInspeccion.findById(cabecera.idInspeccion).descripcion
			params.estacionServicio = cabecera.nEstacionServicio ?: 'No Aplica'
			
			def report = jasperService.buildReportDefinition(params, request.getLocale(), [data:data])
			
			if(!params.enviado)
			{
				log.info("INICIA EL PROCESO DE ENVIO DE CORREO")
				
				FileUtils.writeByteArrayToFile(new File("/web-app/${params.nombreArchivo}.xls"), jasperService.generateReport(report).toByteArray())
				
				def correosFinales = []
				
				params.correoFinal.substring(1, params.correoFinal.length() - 1).split(',').each { correo -> correosFinales.add(correo) }
						
				correosFinales.add("globalpointcliente@gmail.com")
				
				//Servicio que realiza el envio de correo con archivo adjunto
				mailService.sendMail
				{
					multipart true
					to correosFinales //"gabriel.230396@gmail.com" 
					subject "${params.nombreInspeccion} Camion ${params.numero}"
					html   "Estimado(a) ${params.transportista}, le informamos que se ha realizado la inspeccion al camion con los siguientes datos ${params.nombreArchivo}"
					attachBytes "${params.nombreArchivo}.xls", "application/vnd.ms-excel",new File("/web-app/${params.nombreArchivo}.xls").readBytes()
				}
				
				log.info("FINALIZA EL ENVIO DE CORREO")
				
				new EnvioCorreo(fechaEnvio:new Date(), tipoInspeccion:cabecera.idInspeccion, numeroCamion:cabecera.numero).save flush:true, failOnError:true
				
				new File("/web-app/${params.nombreArchivo}.xls").delete()
				
				flash.message = "Correo Enviado exitosamente."
			}
			
			response.setHeader("Content-disposition", "attachment; filename=\"" + (report.parameters._name ?: report.name) +  "\"." + report.fileFormat.extension)
			response.contentType = report.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << report.contentStream.toByteArray()
			response.outputStream.close()
			
			log.info("FINALIZA LA DESCARGA DEL ARCHIVO")
			
			render view:"show", model: [tipoInspeccion:cabecera.idInspeccion, id:cabecera.id]
			//chain(controller:'jasper', action:'index', model:[data:data], params:params) 
		}
		else if(!params.enviado && boolCliente && !boolTransp) 
		{ 
			cabecera.enviado = false;	cabecera.save()
			
			flash.warning = "Favor asociar el correo del Transportista para enviar el correo."
			
			redirect(controller:'inspeccion', action:'show', params:[tipoInspeccion:cabecera.idInspeccion, id:cabecera.id]) 
		}	
		else if(!params.enviado && !boolCliente && boolTransp) 
		{ 
			cabecera.enviado = false;	cabecera.save()
			
			flash.warning = "Favor asociar el correo del Cliente para enviar el correo."
			
			redirect(controller:'inspeccion', action:'show', params:[tipoInspeccion:cabecera.idInspeccion, id:cabecera.id])
		}
		else if(!params.enviado && !boolCliente && !boolTransp) 
		{ 
			cabecera.enviado = false;	cabecera.save()
			
			flash.warning = "Favor asociar los correos del Cliente y del Transportista para poder enviar el correo."
			
			redirect(controller:'inspeccion', action:'show', params:[tipoInspeccion:cabecera.idInspeccion, id:cabecera.id])
		}
	}
	
	@Transactional
	def show()
	{	
		def descripcion = MaestroTipoInspeccion.findById(params.tipoInspeccion as Integer).descripcion
		def cabecera = InspeccionCabecera.get(params.id)		
		def detalle = InspeccionDetalle.findAllByIdCabecera(cabecera)
		def fotos = Image.findAllByIdCabecera(cabecera)		
		
		if(params.enviado == 'false') { cabecera.enviado = false; 	cabecera.save() }
				
		def planta = MaestroPlanta.findByDescripcion(cabecera.planta) 	
		def transportista = MaestroTransportista.findByDescripcion(cabecera.transportista)		
		def cliente = MaestroTipoInspeccion.findById(cabecera.idInspeccion).cliente
		
		def correoTransportista = CorreoTransportista.executeQuery("""select email from CorreoTransportista where planta_id = 
								  ${planta.id} and transportista_id = ${transportista.id} and tipo_inspeccion_id = 
								  ${cabecera.idInspeccion}""").correo
									  
		def correoCliente = CorreoCliente.executeQuery("""select email from CorreoCliente where planta_id = ${planta.id} and
						    tipo_inspeccion_id = ${cabecera.idInspeccion} and cliente_id = ${cliente.id}""").correo
				
		model:[detalle:detalle, fotos:fotos, descripcion:descripcion, cabecera:cabecera, correoCliente:correoCliente, correoTransportista:correoTransportista]
	}
	
	def edit()
	{
		def descripcion = MaestroTipoInspeccion.findById(params.tipoInspeccion as Integer).descripcion		
		def folio = InspeccionCabecera.get(params.id)
		def det = InspeccionDetalle.findAllByIdCabecera(folio)
		def fotos = Image.findAllByIdCabecera(folio).iterator()

		render(view:'edit', model:[det:det, fotos:fotos, descripcion:descripcion, folio:folio])
	}
	
	@Transactional
	def update()
	{
		def cantidadCriticos = 0; def valorItems = 0; def totalBueno = 0; def totalMalo = 0; def verificador = false; def ultimaInspeccion
		
		def cabecera = InspeccionCabecera.get(params.folio as Long)
		
		if(cabecera.idInspeccion == 6)
		{
			cabecera.productoCarga = params.productoCarga;		cabecera.tipoCarguio = params.tipoCarguio
			cabecera.succionPropia = params.succionPropia;		cabecera.cargaOtroMedio = params.cargaOtroMedio
			cabecera.fabricanteMeter = params.fabricanteMeter;	cabecera.modeloMeter = params.modeloMeter
			cabecera.serieMeter = params.serieMeter;			cabecera.ticketPrinterMeter = params.ticketPrinterMeter
		}
		
		def detalle = InspeccionDetalle.findAllByIdCabecera(cabecera)
		
		if(cabecera.idInspeccion != 13 && cabecera.idInspeccion != 7 && cabecera.idInspeccion != 8 && cabecera.idInspeccion != 10 && cabecera.idInspeccion != 11 && cabecera.idInspeccion != 14)
		{
			if(params.estado != 'NULO')
			{
				ultimaInspeccion = InspeccionCabecera.createCriteria().get
				{
					eq('idInspeccion', cabecera.idInspeccion) eq('numero', cabecera.numero) eq('estado', 'APROBADO')
					
					projections { max('fechaInspeccion') }
				}
				
				if(ultimaInspeccion)
				{
					ultimaInspeccion = InspeccionCabecera.createCriteria().get
					{
						eq('idInspeccion', cabecera.idInspeccion) eq('numero', cabecera.numero) 
						eq('estado', 'APROBADO') eq('fechaInspeccion', ultimaInspeccion)
					}
					
					if(ultimaInspeccion) { verificador = true }
				}
				else { verificador = false }
			}			
		}
		
		detalle.each
		{
			var ->
			
			def repeticion = 0
			
			if(params."${var.id}" == '0') 
			{ 
				var.evaluacionItem = var.malo; var.evaluacionItemBm = "Malo"; valorItems += var.malo; totalMalo += 1; repeticion = 1
				
				if(var.critico == 1) { cantidadCriticos++ }
				
				if(verificador && repeticion == 1)
				{
					def detalleFlash = InspeccionDetalle.createCriteria().get
					{
						eq('idCabecera', ultimaInspeccion) eq('pregunta', var.pregunta) eq('evaluacionItemBm', 'Malo')			
					}
					
					if(detalleFlash) { repeticion += detalleFlash.criticoRepeticion }
				}
			}
			
			if(params."${var.id}" == '1')
			{
				var.evaluacionItem = var.bueno; var.evaluacionItemBm = "Bueno"; valorItems += var.bueno; totalBueno += 1				
			}
			
			var.observacion = params."o${var.id}"; var.criticoRepeticion = repeticion	
			
			var.save flush:true
		}
		
		if(cabecera.idInspeccion == 1) { cabecera.indiceOperacion = Math.round(((valorItems * 100) / 5925) * 100) / 100 }
		else if(cabecera.idInspeccion == 2) { cabecera.indiceOperacion = Math.round(((valorItems * 100) / 4850) * 100) / 100 }
		else if(cabecera.idInspeccion == 3) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 52) / 2.37) * 100) / 100 }
		else if(cabecera.idInspeccion == 4) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 78) / 4.3) * 100) / 100 }
		else if(cabecera.idInspeccion == 5) { cabecera.indiceOperacion = Math.round(((1.55039 * 0.01) * valorItems) * 100) / 100 }
		else if(cabecera.idInspeccion == 6) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 61) / 3.14) * 100) / 100}
		else if(cabecera.idInspeccion == 9) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 37) / 1.58) * 100) / 100 }
		else if(cabecera.idInspeccion == 12) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 54) / 2.87) * 100) / 100 }
		else if(cabecera.idInspeccion == 15 || cabecera.idInspeccion == 16) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 54) / 2.27) * 100) / 100 }
		else if(cabecera.idInspeccion == 17) { cabecera.indiceOperacion = Math.round((100 - (valorItems - 56) / 2.31) * 100) / 100 }
		else if(cabecera.idInspeccion == 18) { cabecera.indiceOperacion = Math.round(((valorItems * 100) / 4700) * 100) / 100 } 
		else { cabecera.indiceOperacion = 0 }
		
		if(cabecera.estado != params.estado && params.estado == 'APROBADO') { cabecera.fechaAprobacion = new Date() }
		
		cabecera.totalMalo = totalMalo;				cabecera.totalBueno = totalBueno;			cabecera.puntajeTotal = valorItems;			cabecera.totalCriticos = cantidadCriticos		
		cabecera.fechaInspeccion = params.fecha;	cabecera.conductor = params.chofer;			cabecera.estado = params.estado;			cabecera.comentario = params.comentario
				
		cabecera.save flush:true
		
		def descripcion = MaestroTipoInspeccion.findById(cabecera.idInspeccion).descripcion
		
		def fotos = Image.findAllByIdCabecera(cabecera)
		
		fotos.each { var -> if(params."i${var.id}") { Image.get(var.id).idCabecera.discard(); Image.get(var.id).delete(flush:true) } }
		
		fotos = Image.findAllByIdCabecera(cabecera);
		
		def planta = MaestroPlanta.findByDescripcion(cabecera.planta)
		def transportista = MaestroTransportista.findByDescripcion(cabecera.transportista)
		def tipoInspeccion = MaestroTipoInspeccion.findById(cabecera.idInspeccion)
		
		def correoTransportista = CorreoTransportista.createCriteria().list() { eq('planta', planta) eq('transportista', transportista) eq('tipoInspeccion', tipoInspeccion) }?.email?.correo
		
		def correoCliente = CorreoCliente.createCriteria().list() { eq('planta', planta) eq('tipoInspeccion', tipoInspeccion) eq('cliente', tipoInspeccion.cliente) }?.email?.correo
		 
		def role = Usuario.findByUsername(cabecera.usuario).getAuthorities()
		
		redirect action:'show', params:[id:cabecera.id, tipoInspeccion:cabecera.idInspeccion, role:role, estado:cabecera.estado]
	}
		
	//Genera la lista de las inspecciones para luego poder mostrarlas
	@Transactional
	def verInspeccion()
	{	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()
		def name = auth.getName()
		def idCliente = Usuario.findByUsername(name).cliente
		
		if(params.nuevoMax != null) { params.max = Integer.parseInt(params.nuevoMax) } else { params.max = 10 }
			
		def transportista = MaestroTransportista.findAllByDescripcionIlike('%' + params.transportista + '%')
		def planta = MaestroPlanta.findAllByDescripcionIlike('%' + params.descripcion + '%')
		def listaEncontrados
		def tipoInspeccion = MaestroTipoInspeccion.findAllByCliente(idCliente)
		
		if(permiso.toString() == "[ROLE_INSPECTOR]")
		{	
			listaEncontrados = InspeccionCabecera.createCriteria().list(params)
			{
				and
				{
					ilike('usuario', '%' + name + '%')
					if(params.folio != null && params.folio != '') { eq('id', params.folio as Long) }
					if(params.fecha != null && params.fecha != '') { eq('fechaInspeccion', Date.parse("yyyy-MM-dd", params.fecha)) }
					if(params.planta != null) { ilike('planta', '%' + params.planta + '%') }
					if(params.transportista != null) { ilike('transportista', '%' + params.transportista + '%') }
					if(params.numero != null) { ilike('numero', '%' + params.numero + '%') }					
					if(params.estado != null) { ilike('estado', '%' + params.estado + '%') }
					
					if(params.tipoInspeccion != '' && params.tipoInspeccion != null) 
					{ 
						def idInspeccion = MaestroTipoInspeccion.findByDescripcionIlike('%' + params.tipoInspeccion + '%')
						
						if(idInspeccion != null) { eq('idInspeccion', idInspeccion.id.toInteger()) }
					}
				}
				
				order 'id', 'desc'
			}
		}
		else
		{
			listaEncontrados = InspeccionCabecera.createCriteria().list(params)
			{				
				if(params.folio) { eq('id', params.folio as Long) }					
				if(params.fecha) { eq('fechaInspeccion', Date.parse("yyyy-MM-dd", params.fecha)) }					
				if(params.planta) { ilike('planta', '%' + params.planta + '%') }
				if(params.transportista) { ilike('transportista', '%' + params.transportista + '%') }
				if(params.numero) { ilike('numero', '%' + params.numero + '%') }
				if(params.usuario) { ilike('usuario', '%' + params.usuario + '%') }
				if(params.estado) { ilike('estado', '%' + params.estado + '%') }
				
				if(params.tipoInspeccion)
				{
					def idInspeccion = []
					
					MaestroTipoInspeccion.findAllByDescripcionIlike('%' + params.tipoInspeccion + '%').id.each { var -> idInspeccion.add(var.toInteger()) }
					
					if(idInspeccion) { 'in'('idInspeccion', idInspeccion) }
				}					
				else 
				{ 
					def idInspeccion = []
					MaestroTipoInspeccion.findAllByCliente(MaestroCliente.findByRazonSocial(idCliente)).id.each { var -> idInspeccion.add(var.toInteger()) }
					'in'('idInspeccion', idInspeccion) 
				}
				
				order 'id', 'desc'
			}				
		}

		render view:'verInspeccion', model:[listaEncontrados:listaEncontrados, permiso:permiso, listaEncontradosTotal:listaEncontrados.getTotalCount(), tipoInspeccion:tipoInspeccion, idCliente:idCliente]
	}
	
	//Permite mostrar la imagen guardada en la base de datos
	def showImage = 
	{
		def imagen = Image.get(params.id )
		response.outputStream << imagen.foto
		response.outputStream.flush()
	}
	
	@Transactional
	def anularInforme()
	{
		def cabecera = InspeccionCabecera.get(params.cabecera)
		cabecera.estado = 'NULO'
		
		flash.message = "La inspecci\u00F3n se ha anulado exitosamente."
		redirect action:'verInspeccion'
	}
}