package Maestros

import sun.security.krb5.internal.crypto.CksumType;
import groovy.sql.Sql
import static org.springframework.http.HttpStatus.*
import grails.converters.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MaestroClienteController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def clientes = MaestroCliente.createCriteria().list(params) {  }
        
		model:[clientes:clientes]
    }
	
    def show(MaestroCliente cliente) { model:[cliente:cliente] }

    def create() { respond new MaestroCliente(params) }

    @Transactional
    def save(MaestroCliente maestroClienteInstance) 
	{
        maestroClienteInstance.save flush:true

        flash.message = "El Cliente se ha actualizado exitosamente."
        redirect maestroClienteInstance
    }

    def edit(MaestroCliente maestroClienteInstance) { respond maestroClienteInstance }

    @Transactional
    def update(MaestroCliente cliente) 
	{
        cliente.save flush:true

        flash.message = "El Cliente se ha actualizado exitosamente."
		redirect cliente
    }

    @Transactional
    def delete(MaestroCliente cliente) 
	{
        cliente.activo = !cliente.activo
		
		if(cliente.activo) { flash.message = "El Cliente se ha activado exitosamente." } else { flash.message = "El Cliente se ha desactivado exitosamente." }
		
		redirect cliente
	}
}