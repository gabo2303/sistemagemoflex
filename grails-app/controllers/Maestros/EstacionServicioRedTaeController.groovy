package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EstacionServicioRedTaeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def localidad = MaestroPlanta.get(params.localidad)
		
		def estaciones = EstacionServicioRedTae.createCriteria().list(params)
		{
			if(params.codigoEstacion) { ilike('codigoEstacion', '%' + params.codigoEstacion + '%') }
			if(localidad) { eq('localidad', localidad) }
		}
		
		if(params.localidad == '') { params.localidad = null }
		
		if(!estaciones) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[estaciones:estaciones, params:params]        
    }

    def show(EstacionServicioRedTae estacionServicioRedTaeInstance) { respond estacionServicioRedTaeInstance }

    def create() { respond new EstacionServicioRedTae(params) }

    @Transactional
    def save(EstacionServicioRedTae estacionServicioRedTaeInstance) 
	{
        if (estacionServicioRedTaeInstance.hasErrors()) {  
            respond estacionServicioRedTaeInstance.errors, view:'create'
            return
        }

        estacionServicioRedTaeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'estacionServicioRedTae.label', default: 'EstacionServicioRedTae'), estacionServicioRedTaeInstance.id])
                redirect estacionServicioRedTaeInstance
            }
            '*' { respond estacionServicioRedTaeInstance, [status: CREATED] }
        }
    }

    def edit(EstacionServicioRedTae estacionServicioRedTaeInstance) { respond estacionServicioRedTaeInstance }

    @Transactional
    def update(EstacionServicioRedTae estacionServicioRedTaeInstance) 
	{
		if (estacionServicioRedTaeInstance.hasErrors()) {
            respond estacionServicioRedTaeInstance.errors, view:'edit'
            return
        }

        estacionServicioRedTaeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EstacionServicioRedTae.label', default: 'EstacionServicioRedTae'), estacionServicioRedTaeInstance.id])
                redirect estacionServicioRedTaeInstance
            }
            '*'{ respond estacionServicioRedTaeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EstacionServicioRedTae estacionServicioRedTaeInstance) 
	{
        estacionServicioRedTaeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EstacionServicioRedTae.label', default: 'EstacionServicioRedTae'), estacionServicioRedTaeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}
