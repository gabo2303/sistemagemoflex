package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional
import seguridad.Usuario

@Transactional(readOnly = true)
class MaestroCamionController 
{
	def springSecurityService
	def validacionService
	
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
    def index(Integer max) 
	{
		params.max = 10
				
		def transportista = MaestroTransportista.get(params.transportista)
		def planta = MaestroPlanta.get(params.planta)
					
		def camiones = MaestroCamion.createCriteria().list(params)
		{
			if (params.cliente) { 'in'('cliente', MaestroCliente.get(params.cliente)) }
			if (params.activo == 'Verdadero') {	eq('activo', true) } else if (params.activo == 'Falso') { eq('activo', false) }
			if (transportista) { 'in'('transportista', transportista) }
			if (planta) { 'in'('planta', planta) }
			if (params.numero) { ilike('numero', '%' + params.numero + '%') }
			if (params.patente) { ilike('patente', '%' + params.patente + '%') }	
		}
		
		if(!params.transportista) { params.transportista = null }
		if(!params.planta) { params.planta = null }
		
		if(!camiones) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[camiones:camiones, params:params]	     
    }

    def show(MaestroCamion camion) { model:[camion:camion] }

    def create() 
	{ 
		def cliente = Usuario.findByUsername(springSecurityService.currentUser).cliente
		def plantas = MaestroPlanta.findAllByCliente(cliente)
		def transportistas = MaestroTransportista.findAllByCliente(cliente)
		
		respond new MaestroCamion(params), model:[cliente:cliente, plantas:plantas, transportistas:transportistas] 
	}

    @Transactional
    def save(MaestroCamion camion) 
	{
		if(MaestroCamion.findByPatenteIlike('%' + params.patente + '%'))
		{
			flash.warning = "La patente que intenta ingresar ya se encuentra asociada a otro cami\u00F3n. Favor revisar."
			forward action:'create'
			return
		}
		
        camion.save flush:true, failOnError:true
		
        flash.message = "El camion " + camion.id + " ha sido creado exitosamente."
        redirect camion
    }

    def edit(MaestroCamion maestroCamionInstance) 
	{
		def cliente = Usuario.findByUsername(springSecurityService.currentUser).cliente
		def plantas = MaestroPlanta.findAllByCliente(cliente)
		def transportistas = MaestroTransportista.findAllByCliente(cliente)
		
        respond maestroCamionInstance, model:[cliente:cliente, plantas:plantas, transportistas:transportistas]
    }

    @Transactional
    def update()
	{
		def camion = MaestroCamion.get(params.id)
		
		if(MaestroCamion.findByPatenteIlikeAndIdNotEqual('%' + params.patente + '%', camion.id))
		{
			flash.warning = "La patente que intenta ingresar ya se encuentra asociada a otro cami\u00F3n. Favor revisar."
			forward action:'edit'
			return
		}	
		
		camion.planta = MaestroPlanta.get(params.planta)
		camion.transportista = MaestroTransportista.get(params.transportista)
		camion.patente = params.patente
		camion.marca = params.marca
		camion.numero = params.numero
		camion.tipoCamion = params.tipoCamion
		camion.capacidad = params.capacidad
		
        camion.save flush:true, failOnError:true
        
		flash.message = "El cami\u00F3n " + camion.id + " ha sido actualizado exitosamente."
        redirect camion
    }
	
	@Transactional
	def delete(MaestroCamion camion)
	{
		camion.activo = !camion.activo
		
		if(params.accion == 'Activar') { flash.message = "El Cami\u00F3n se ha Activado exitosamente." } else { flash.message = "El Cami\u00F3n se ha Desactivado exitosamente." }
		
		redirect camion
	}
}