package Maestros

import static org.springframework.http.HttpStatus.*

import java.awt.event.ItemEvent;
import java.security.Security;

import javax.lang.model.element.VariableElement;

import jxl.Workbook
import jxl.write.Label
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook
import jxl.write.Number

import org.apache.commons.codec.language.bm.Languages.SomeLanguages;
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.*

import seguridad.Usuario
import grails.transaction.Transactional
import grails.converters.JSON
import groovy.sql.Sql

@Transactional(readOnly = true)
class ConsultasController 
{
	def consultasService
	def springSecurityService
	def dataSource
	
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	//Acci�n que los datos inciales de la consulta resumen de RedTae
	 def index() 
	 { 		
		 def usuario = Usuario.findByUsername(springSecurityService.currentUser)		
		 def permiso = usuario.getAuthorities()	 
		
		 //Consultas que devuelven el listado de plantas asociadas a un usuario pasando por una serie de filtros
		 def plantasPrevias = InspeccionCabecera.executeQuery("""select planta from InspeccionCabecera where idInspeccion = 6 and estado = 'APROBADO'""")	
		
		 plantasPrevias = consultasService.arregloString(plantasPrevias)	
		
		 def planta = MaestroPlanta.executeQuery("""select descripcion from MaestroPlanta where descripcion in (${plantasPrevias})and id in (select planta from UsuarioPlanta where usuario = ${usuario.id})""")
		
		 def plantaString = consultasService.arregloString(planta)
		
		 model:[permiso:permiso, name:usuario.username, cliente:usuario.cliente, planta:planta.sort { it }, plantaString:plantaString]
	 }
	
	 def consultaResumenRedTae() { render(template: "consultaResumenRedTae", model:[params:params]) } 
	 
	//Funci�n que sirve para exportar el detalle de las inspecciones a csv
	def reporteCsv()
	{	
		def fechaInicial = params.fechaInicial 				//Variable que guarda la fecha inicial
		def fechaFinal = params.fechaFinal 					//Variable que guarda la fecha final
		def plantaSeleccionada = params.plantaSeleccionada	//Variable que guarda la planta seleccionada		
		def plantaString = params.plantaString 				//Variable que guarda las plantas totales
		
		def detalle	//Variable que guarda la consulta del detalle de la inspecci�n
		
		if(plantaSeleccionada == "") 
		{ 	
			//Consulta por todas las plantas
			detalle = InspeccionDetalle.executeQuery(consultasService.inspeccionDetalleRedTae(fechaInicial, fechaFinal, ''))
		}
		else
		{
			//Consulta por una planta seleccionada
			detalle = InspeccionDetalle.executeQuery(consultasService.inspeccionDetalleRedTae(fechaInicial, fechaFinal, "'${plantaSeleccionada}'"))
		}
		
		//Variable que guarda el tama�o de la lista segun la consulta detalle
		def cont = detalle.size() 
		
		//Se verifica si se encontraron datos o no en la consulta		
		if (cont == 0)
		{
			flash.message = "No se encontraron datos"
			redirect(action:'index')
			return true
		}	
		else
		{		
			def data = []
							
			detalle.each 
			{
				var->
				
				data.add("pregunta":var[0], "idPregunta":var[1], "patente":var[2], "evaluacion_item":var[3], "evaluacion_item_bm":var[4], 
				"planta":var[5], "Numero_Estacion":var[6], "fecha":var[7], "marca":var[8] ?: var[9], "capacidad":var[10])	 
			}
						
			chain(controller:'jasper',action:'index', model:[data:data], params:params)
		}	
				
	}
	
	//METODOS QUE CONFORMAN LA CONSULTA RESUMEN DE INSPECCION
	def resumenInspeccion()
	{
		def usuario = Usuario.findByUsername(springSecurityService.currentUser)		
		def permiso = usuario.getAuthorities()
		
		flash.message = "Seleccione las inspecciones a consultar."
								
		model:[permiso:permiso, usuarioId:usuario.id, cliente:usuario.cliente]
	}
	
	def resumenInspeccionPlanta()
	{
		def inspecciones = []
		
		if(params.multi)
		{
			if(params.liviano) { inspecciones.add(MaestroTipoInspeccion.get(1).id) }
			if(params.petroleo) { inspecciones.add(MaestroTipoInspeccion.get(2).id) }
			if(params.grafica) { inspecciones.add(MaestroTipoInspeccion.get(3).id) }
			if(params.lubricante) { inspecciones.add(MaestroTipoInspeccion.get(4).id) }
			if(params.graficaLubricante) { inspecciones.add(MaestroTipoInspeccion.get(9).id) }
			if(params.redtae) { inspecciones.add(MaestroTipoInspeccion.get(6).id) }
			if(params.cdLubricante) { inspecciones.add(MaestroTipoInspeccion.get(12).id) }
			if(params.taller) { inspecciones.add(MaestroTipoInspeccion.get(13).id) }
			if(params.grafica2017) { inspecciones.add(MaestroTipoInspeccion.get(15).id) }
			if(params.graficaFuel) { inspecciones.add(MaestroTipoInspeccion.get(16).id) }
			if(params.graficaMinero) { inspecciones.add(MaestroTipoInspeccion.get(17).id) }
			if(params.aditivoAdblue) { inspecciones.add(MaestroTipoInspeccion.get(18).id) }
		}
		else { inspecciones.add(MaestroTipoInspeccion.findAllByDescripcion(params.id).id) }
		
		def usuarioId = params.usuarioId
		inspecciones = consultasService.arregloString(inspecciones)
		
		def plantaInformes = InspeccionCabecera.executeQuery("""select distinct planta from InspeccionCabecera where estado = 'APROBADO' and idInspeccion in (${inspecciones}))""")
		
		def plantaInformesString = consultasService.arregloString(plantaInformes)

		def planta = MaestroPlanta.executeQuery("""select descripcion from MaestroPlanta where id in (select planta from UsuarioPlanta where usuario = ${usuarioId}) and descripcion in 
												(${plantaInformesString})""")
		
		def plantaSinCero = MaestroPlanta.executeQuery("""select descripcion from MaestroPlanta where id in (select planta from UsuarioPlanta where usuario = ${usuarioId}) and descripcion in 
													   (${plantaInformesString})""")
		
		plantaSinCero = consultasService.arregloString(plantaSinCero)
		
		planta = planta.sort { it }
		planta.add(0, "Todas Las Plantas")
		
		render(template:'resumenInspeccionPlanta', model:[plantaSinCero:plantaSinCero, planta:planta, inspecciones:inspecciones])
	}
	
	def resumenInspeccionTransportista()
	{
		def plantaSeleccionada = params.plantaSeleccionada 
		def plantaSinCero = params.plantaSinCero
		def inspecciones = params.inspecciones
		def transportista 
				
		if(plantaSeleccionada == "Todas Las Plantas")
		{
			transportista = InspeccionCabecera.executeQuery("""select transportista from InspeccionCabecera where idInspeccion in (${inspecciones}) and planta in (${plantaSinCero})
															and estado = 'APROBADO' group by transportista""")	
		}
		else
		{
			transportista = InspeccionCabecera.executeQuery("""select transportista from InspeccionCabecera where idInspeccion in  (${inspecciones}) and planta = '${plantaSeleccionada}'
															and estado = 'APROBADO' group by transportista""")
		}
		
		def transportistaSinCero = consultasService.arregloString(transportista)
		
		transportista.add(0, "Todos Los Transportistas")
		
		render template: 'resumenInspeccionTransportista', model:[inspecciones:inspecciones, plantaSeleccionada:plantaSeleccionada, plantaSinCero:plantaSinCero, transportista:transportista, 
																 transportistaSinCero: transportistaSinCero] 
	}
	
	def resumenInspeccionFechas()
	{		
		def inspecciones = params.inspeccionesTransportista
		def plantaSinCero = params.plantaSinCero
		def plantaSeleccionada = params.plantaSeleccionada
		def transportista = params.transportista
		def transportistaSinCero = params.transportistaSinCero		
		
		render template:'resumenInspeccionFechas', model:[inspecciones:inspecciones, plantaSeleccionada:plantaSeleccionada, plantaSinCero:plantaSinCero, transportista:transportista, 
														   transportistaSinCero: transportistaSinCero]
	}
	
	def resumenInspeccionPreReporte()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication()
		def permiso = auth.getAuthorities()//Devuelve el permiso del usuario
		def inspecciones = params.inspeccionesFecha
		def plantaSeleccionada = params.plantaSeleccionadaFecha
		def plantaSinCero = params.plantaSinCeroFecha
		def transportistaSeleccionado = params.transportistaSeleccionado
		def transportistaSinCero = params.transportistaSinCeroFecha
		def fechaInicial = params.fechaInicial
		def fechaFinal = params.fechaFinal
		
		render template:'resumenInspeccionReporte', model:[inspecciones:inspecciones, plantaSeleccionada: plantaSeleccionada, plantaSinCero: plantaSinCero, transportistaSeleccionado: transportistaSeleccionado,
														   transportistaSinCero: transportistaSinCero, fechaInicial: fechaInicial, fechaFinal: fechaFinal, permiso:permiso]
	}
	
	//M�todo que permite exportar el resumen de la cabecera 
	def resumenInspeccionReporte()
	{		
		def fechaInicial = params.fechaInicial
		def fechaFinal = params.fechaFinal		
		def plantaSeleccionada = params.plantaSeleccionadaReporte
		def plantasTotales = params.plantaSinCeroReporte
		def inspecciones = MaestroTipoInspeccion.executeQuery("from MaestroTipoInspeccion where id in (${params.inspeccionesReporte})")
		def inspeccionesId = inspecciones.id.toString().substring(1, inspecciones.toString().length() - 1)		
		def inspeccionesDescripcion = []
		
		inspecciones.descripcion.each { inspeccionesDescripcion.add(it.drop(15)) } 
		inspeccionesDescripcion = inspeccionesDescripcion.toString().substring(1, inspeccionesDescripcion.toString().length() - 1)
		
		def transportistaSeleccionado = params.transportistaSeleccionadoReporte
		def transportistasTotales = params.transportistaSinCeroReporte
		
		def detalle		
		def promedioIO = 0
		def promedioPuntaje = 0			
		def contador = 0
		def sumaCriticos = 0
				
		if(plantaSeleccionada == "Todas Las Plantas")
		{
			if(transportistaSeleccionado == "Todos Los Transportistas")
			{
				detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte('', '', fechaInicial, fechaFinal, inspeccionesId))
			}
			else { detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte('', "'${transportistaSeleccionado}'", fechaInicial, fechaFinal, inspeccionesId)) }
		}
		else
		{
			if(transportistaSeleccionado == "Todos Los Transportistas")
			{
				detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte("'${plantaSeleccionada}'", '', fechaInicial, fechaFinal, inspeccionesId))				
			}
			else { detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte("'${plantaSeleccionada}'", "'${transportistaSeleccionado}'", fechaInicial, fechaFinal, inspeccionesId)) }
		}
		
		//If que verifica si se encontraron datos en la consulta o no			
		if (detalle.isEmpty())
		{
			flash.message2 = "No se encontraron datos"
			
			redirect(action:'resumenInspeccion')
			return true
		}	
		else
		{
			def data = []
			def cabecera = 0
			
			for(k in 0..detalle.size - 1)
			{	
				promedioIO = promedioIO + detalle[k][4]
				promedioPuntaje = promedioPuntaje + detalle[k][3]
				sumaCriticos = sumaCriticos + detalle[k][5]	
				contador++	
			}				
			
			promedioIO = Math.round((promedioIO/contador) * 100) / 100
			promedioPuntaje = Math.round((promedioPuntaje/contador) * 100) / 100
						
			detalle.each 
			{
				i ->	
				
				def descripcion = MaestroTipoInspeccion.get(i[8]).descripcion
				descripcion = descripcion.drop(15)
				println "I: ${i[9]}"
				data.add("date1":fechaInicial, "date2":fechaFinal, "nombreInspeccion":inspeccionesDescripcion, "tipoInspeccion":descripcion, 
						 "id":i[0], "numero":i[1], "fecha_hora_inspeccion":i[2], "puntaje_total":i[3], "transportista":i[6], "critico_detalle":i[5], 
						 "indice_operacion":i[4], "planta":i[7], "promedioIO":promedioIO.toDouble(), "promedios_puntajes":promedioPuntaje.toDouble(), 
						 "sumaCriticos":sumaCriticos, "estacion_servicio": (i[9] ?: "No Aplica"), "marca": (i[10] ?: i[11]), "capacidad": i[12])							
			}

			chain(controller:'jasper', action:'index', model:[data:data], params:params)
		}	
	}
	
	//Metodo que muestra el listado de las inspecciones segun el usuario conectado
	def consultarPorItem()
	{
		def cliente = Usuario.findByUsername(springSecurityService.currentUser).cliente				
				
		model:[tipoInspeccion:cliente.tipoInspeccion, cliente:cliente]		
	}
	
	//M�todo que entrega las inspecciones que tienen falla en algun item seleccionado	
	def listadoPorItems()
	{
		def itemsTotales;  def plantasTotales;  def tanquesTotales;  def transportistasTotales;  def query;	 def listadoItemsPorFalla;  def listadoItemsPorFallaTotal;  def plantas = [];  def tanques = []   		
		
		def inspecciones = [] 
		def fecha1 = params.nfecha1
		def fecha2 = params.nfecha2		
		def critico = 1		
		def cliente = Usuario.findByUsername(springSecurityService.currentUser).cliente
		
		if(!params.inspecciones) { MaestroTipoInspeccion.findAllByClienteAndActivo(cliente, true).each { inspeccion -> if(params."${inspeccion.descripcion.drop(15)}") { inspecciones.add(inspeccion) } } }
		
		else { inspecciones = MaestroTipoInspeccion.findAllByIdInListAndActivo(params.inspecciones[1..-2].split(',').toList(), true) }
				
		if(!inspecciones) 
		{ 
			flash.warning = "Debe seleccionar a lo menos un Tipo de Inspecci\u00F3n."
			redirect action:'consultarPorItem' 
			return 
		}
		
		if(!params.item) { itemsTotales = "%%%" } else { itemsTotales = '%' + params.item + '%' }
				
		if(!params.planta) { plantasTotales = consultasService.arregloString(MaestroPlanta.findAllByCliente(cliente).descripcion) } else { plantasTotales = "'${params.planta}'" }
		if(!params.tanque) { tanquesTotales = consultasService.arregloString(MaestroCamion.findAllByCliente(cliente).numero) } else { tanquesTotales = "'${params.tanque}'" }
		
		if(!params.transportista) { transportistasTotales = consultasService.arregloString(MaestroTransportista.findAllByCliente(cliente).descripcion) } 
		else { transportistasTotales = consultasService.arregloString(MaestroTransportista.findAllByDescripcionIlikeAndCliente('%' + params.transportista + '%', cliente)) }
		
		if(!itemsTotales || !transportistasTotales)
		{
			flash.warning = "No existe ninguna Pregunta de CheckList o Transportista con la especificaci\u00F3n ingresada."
			render view:'consultarPorItem', model:[inspecciones:inspecciones.id, fecha1:fecha1, fecha2:fecha2, params:params, plantas:plantas, tanques:tanques]
			return
		}
				
		if(!params.paginacion) { params.max = 10 } else { params.max = params.paginacion } 
		
		if(params.critico == 'Critico') { critico = '1' } else if(params.critico == 'Malo') { critico = '0' } else if(params.critico ==  'Todos') { critico = "'1', '0'" }
		
		if(!params.offset) { params.offset = 0 }
		
		query =
		"""
			select c.fechaInspeccion, d.pregunta, d.observacion, c.planta, c.numero, c.transportista, d.critico, c.idInspeccion, c.estado, c.id from InspeccionCabecera c, InspeccionDetalle d 
			where c.idInspeccion in (${consultasService.arregloString(inspecciones.id)}) and c.numero in (${tanquesTotales}) and d.pregunta like ? and c.planta in (${plantasTotales}) 
			and c.transportista in (${transportistasTotales}) and d.critico in (${critico}) and c.id = d.idCabecera and c.fechaInspeccion between ? and ? and d.evaluacionItemBm = 'malo' 
			and c.estado = 'APROBADO' order by c.fechaInspeccion desc
		"""
		
		listadoItemsPorFalla = InspeccionDetalle.executeQuery(query, [itemsTotales, fecha1, fecha2], [max:params.max, offset:params.offset])
		listadoItemsPorFallaTotal = InspeccionDetalle.executeQuery(query, [itemsTotales, fecha1, fecha2])
		listadoItemsPorFallaTotal.each { plantas.add(it[3]); tanques.add(it[4]) }
				
		if(!listadoItemsPorFalla) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda ingresada." }
			
		render view:'consultarPorItem', model:[listadoItemsPorFalla:listadoItemsPorFalla, listadoItemsPorFallaTotal:listadoItemsPorFallaTotal, inspecciones:inspecciones.id, fecha1:fecha1, fecha2:fecha2, params:params, plantas:plantas.sort { it }, tanques:tanques.sort { it }]
	}
	
	def consultaReiterativos()
	{
		def cabecera 
		def detalles

		if(params.numero)
		{
			cabecera = InspeccionCabecera.createCriteria().list { eq('numero', params.numero) eq('idInspeccion', params.tipoInspeccion as Integer) eq('estado', 'APROBADO') }.max { it.fechaInspeccion }
			
			detalles = InspeccionDetalle.createCriteria().list
			{
				eq('idCabecera', cabecera)
				ge('criticoRepeticion', 2)
			}
			
			if(!detalles) { flash.warning = "No se han encontrado criticos repetitivos para el camion y el tipo de inspeccion ingresado." }		
		}
		
		model:[detalles:detalles]
	}
	
	def webService()
	{
		def sql = new Sql(dataSource)
		
		def resultados = sql.rows("""SELECT * FROM inspeccion_cabecera WHERE fecha_inspeccion = ? AND estado = 'APROBADO'""", [new Date().clearTime()])
				
		sql.close()
		
		def res = []
				
		resultados.each
		{
			resultado ->
			
			res.add('id':resultado.id, 'fecha':new Date().clearTime(), 'tipoInspeccion':MaestroTipoInspeccion.get(resultado.id_inspeccion as Long).descripcion, 'idInspeccion':resultado.id_inspeccion,
					'numero': resultado.numero, 'planta':resultado.planta, 'transportista':resultado.transportista, 'puntajeTotal':resultado.puntaje_total, 'indiceOperacion':resultado.indice_operacion,
					'totalItemsEvaluados':resultado.total_items_evaluados, 'totalBueno':resultado.total_bueno, 'totalMalo':resultado.total_malo, 'totalCriticos':resultado.total_criticos)
		}
		
		render res as JSON
	}
	
	def bajadaExcelResumenInspeccion()
	{
		def fechaInicial = params.fechaInicial
		def fechaFinal = params.fechaFinal
		def plantaSeleccionada = params.plantaSeleccionadaReporte
		def plantasTotales = params.plantaSinCeroReporte
		def inspecciones = MaestroTipoInspeccion.executeQuery("from MaestroTipoInspeccion where id in (${params.inspeccionesReporte})")
		def inspeccionesId = inspecciones.id.toString().substring(1, inspecciones.toString().length() - 1)
		def inspeccionesDescripcion = []
		inspecciones.descripcion.each { inspeccionesDescripcion.add(it.drop(15)) }
		inspeccionesDescripcion = inspeccionesDescripcion.toString().substring(1, inspeccionesDescripcion.toString().length() - 1)
		
		def transportistaSeleccionado = params.transportistaSeleccionadoReporte
		def transportistasTotales = params.transportistaSinCeroReporte
		
		def detalle
		def promedioIO = 0
		def promedioPuntaje = 0
		def contador = 0
		def sumaCriticos = 0
				
		if(plantaSeleccionada == "Todas Las Plantas")
		{
			if(transportistaSeleccionado == "Todos Los Transportistas")
			{
				detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte('', '', fechaInicial, fechaFinal, inspeccionesId))
			}
			else { detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte('', "'${transportistaSeleccionado}'", fechaInicial, fechaFinal, inspeccionesId)) }
		}
		else
		{
			if(transportistaSeleccionado == "Todos Los Transportistas")
			{
				detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte("'${plantaSeleccionada}'", '', fechaInicial, fechaFinal, inspeccionesId))
			}
			else { detalle = InspeccionCabecera.executeQuery(consultasService.resumenCabeceraReporte("'${plantaSeleccionada}'", "'${transportistaSeleccionado}'", fechaInicial, fechaFinal, inspeccionesId)) }
		}
		
		//If que verifica si se encontraron datos en la consulta o no
		if (detalle.isEmpty())
		{
			flash.message2 = "No se encontraron datos"
			
			redirect(action:'resumenInspeccion')
			return true
		}
		
		String nombre = "Resumen_Inspecciones.xls"
		response.setContentType('application/vnd.ms-excel')
		response.setHeader('Content-Disposition', 'Attachment;Filename=' + nombre)
		WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream)
		
		WritableSheet sheet1 = workbook.createSheet("Resumen de Inspecciones", 0)
		
		sheet1.setColumnView(0, 35)
		sheet1.setColumnView(1, 25)
		sheet1.setColumnView(2, 14)
		sheet1.setColumnView(3, 30)
		sheet1.setColumnView(4, 15)
		sheet1.setColumnView(5, 15)
		sheet1.setColumnView(6, 25)
		sheet1.setColumnView(7, 25)
		sheet1.setColumnView(8, 25)
		sheet1.setColumnView(9, 25)
		sheet1.setColumnView(10, 20)
		sheet1.setColumnView(11, 20)
		
		sheet1.addCell(new Label(0, 0, "Resumen de Informes"))
		sheet1.addCell(new Label(0, 2, inspeccionesDescripcion))
		sheet1.addCell(new Label(0, 4, "Fecha Inicial: " + fechaInicial))
		sheet1.addCell(new Label(1, 4, "Fecha Final: " + fechaFinal))
		
		sheet1.addCell(new Label(0, 6, "Tipo de Inspeccion"))
		sheet1.addCell(new Label(1, 6, "Numero Folio"))
		sheet1.addCell(new Label(2, 6, "Numero Tanque"))
		sheet1.addCell(new Label(3, 6, "Fecha Inspeccion"))
		sheet1.addCell(new Label(4, 6, "Puntaje"))
		sheet1.addCell(new Label(5, 6, "I.O%"))
		sheet1.addCell(new Label(6, 6, "Criticos"))
		sheet1.addCell(new Label(7, 6, "Transportista"))
		sheet1.addCell(new Label(8, 6, "Planta"))
		sheet1.addCell(new Label(9, 6, "Estacion de Servicio"))
		sheet1.addCell(new Label(10, 6, "Marca"))
		sheet1.addCell(new Label(11, 6, "Capacidad"))
				
		def data = []
		def cabecera = 0
		
		for(k in 0..detalle.size - 1)
		{
			promedioIO = promedioIO + detalle[k][4]
			promedioPuntaje = promedioPuntaje + detalle[k][3]
			sumaCriticos = sumaCriticos + detalle[k][5]
			contador++
		}
			
		promedioIO = Math.round((promedioIO/contador) * 100) / 100
		promedioPuntaje = Math.round((promedioPuntaje/contador) * 100) / 100
					
		detalle.eachWithIndex
		{
			det, i ->
			
			def descripcion = MaestroTipoInspeccion.get(det[8]).descripcion
			descripcion = descripcion.drop(15)
			
			sheet1.addCell(new Label(0, 7 + i, descripcion))
			sheet1.addCell(new Label(1, 7 + i, det[0].toString()))
			sheet1.addCell(new Label(2, 7 + i, det[1]))
			sheet1.addCell(new Label(3, 7 + i, det[2]))
			sheet1.addCell(new Label(4, 7 + i, det[3].toString()))
			sheet1.addCell(new Label(5, 7 + i, det[4].toString()))
			sheet1.addCell(new Label(6, 7 + i, det[5].toString()))
			sheet1.addCell(new Label(7, 7 + i, det[6]))
			sheet1.addCell(new Label(8, 7 + i, det[7]))
			sheet1.addCell(new Label(9, 7 + i, det[9] ? det[9] : 'No Aplica'))
			sheet1.addCell(new Label(10, 7 + i, det[10] ?: det[11]))
			sheet1.addCell(new Label(11, 7 + i, det[12]))
		}
		
		sheet1.addCell(new Label(0, detalle.size + 8, "Resumen Total"))
		sheet1.addCell(new Label(0, detalle.size + 10, "Promedio I.O: " + promedioIO.toString()))
		sheet1.addCell(new Label(0, detalle.size + 11, "Promedio Puntaje: " + promedioPuntaje.toString()))
		sheet1.addCell(new Label(0, detalle.size + 12, "Total Criticos: " + sumaCriticos))
		
		workbook.write()
		workbook.close()
	}
}