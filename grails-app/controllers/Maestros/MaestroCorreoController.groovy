package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MaestroCorreoController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def correos = MaestroCorreo.createCriteria().list(params) 
		{ 
			if(params.correo) { ilike('correo', '%' + params.correo + '%') }
			if(params.activo == 'Si') { eq('activo', true) } else if(params.activo == 'No') { eq('activo', false) }
		}
		
		if(!correos) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[correos:correos]     
    }
	
    def show(MaestroCorreo correo) { model:[correo:correo] }

    def create() { respond new MaestroCorreo(params) }

    @Transactional
    def save(MaestroCorreo correo)
	{
        if(MaestroCorreo.findByCorreo(correo.correo.trim()))
		{
			flash.warning = "El Correo ingresado ya se encuentra en la Base de Datos. Favor revisar."
			forward action:'create'
			return
		}
		
		correo.save flush:true, failOnError:true

        flash.message = "El Correo se ha creado exitosamente."
		redirect correo
    }

    def edit(MaestroCorreo maestroCorreoInstance) { respond maestroCorreoInstance }

    @Transactional
    def update(MaestroCorreo correo) 
	{        
		correo.save flush:true

        flash.message = "El Correo ha sido actualizado exitosamente."
        redirect correo
    }

    @Transactional
    def delete(MaestroCorreo correo) 
	{
        correo.activo = !correo.activo

		if(correo.activo) { flash.message = "El Correo se ha activado exitosamente." } else { flash.message = "El Correo se ha desactivado exitosamente." }
		
        redirect correo
    }
}