 package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CorreoTransportistaController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def planta = MaestroPlanta.get(params.planta)
		def transportista = MaestroTransportista.get(params.transportista)
		def correo = MaestroCorreo.get(params.correo)
		def tipoInspeccion = MaestroTipoInspeccion.get(params.tipoInspeccion)
		
		def correosTransportistas = CorreoTransportista.createCriteria().list(params)
		{
			if(transportista) { eq('transportista', transportista) }
			if(planta) { eq('planta', planta) }
			if(correo) { eq('email', correo) }
			if(tipoInspeccion) { eq('tipoInspeccion', tipoInspeccion) }
		}
		
		if(params.planta == '') { params.planta = null }
		if(params.correo == '') { params.correo = null }
		if(params.transportista == '') { params.transportista = null }
		
		if(!correosTransportistas) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[correosTransportistas:correosTransportistas, params:params]	     
    }
	
    def show(CorreoTransportista asociacion) { model:[asociacion:asociacion] }

    def create() { respond new CorreoTransportista(params) }

    @Transactional
    def save(CorreoTransportista asociacion) 
	{
        asociacion.save flush:true

        flash.message = "La asociaci\u00F3n Correo-Transportista se ha creado exitosamente."
        redirect asociacion
    }
	
    def edit(CorreoTransportista correoTransportistaInstance) { respond correoTransportistaInstance }

    @Transactional
    def update(CorreoTransportista asociacion) 
	{
        asociacion.save flush:true
		
        flash.message = "La asociaci\u00F3n Correo-Transportista se ha actualizado exitosamente."
        redirect asociacion
    }
	
    @Transactional
    def delete(CorreoTransportista asociacion) 
	{
        asociacion.delete flush:true
		
        flash.message = "La asociaci\u00F3n Correo-Transportista se ha eliminado exitosamente."
        redirect action:'index', method:'GET'
    }
}