package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional
import seguridad.Permiso
import seguridad.UsuarioPermiso
import jxl.Workbook
import jxl.write.Label
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook

@Transactional(readOnly = true)
class MaestroTransportistaController 
{
	def springSecurityService
	
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	@Transactional
	def index(Integer max) 
	{
		params.max = 10
		
		def transportistas = MaestroTransportista.createCriteria().list(params)
		{
			if (params.cliente) { def cliente = MaestroCliente.get(params.cliente); 'in'('cliente', cliente) }
			if (params.activo == 'Verdadero') {	eq('activo', true) } else if (params.activo == 'Falso') { eq('activo', false) }
			if (params.rut) { ilike('rut', '%' + params.rut + '%') }
			if (params.razonSocial) { ilike('razonSocial', '%' + params.razonSocial + '%') }
			if (params.descrpicion) { ilike('descripcion', '%' + params.descripcion + '%') }
		}
		
		if(!transportistas) { flash.warning = "No se han encontrado resultados para la b\u00FAsqueda realizada." }
		
		else { flash.message = "Se ha(n) encontrado ${transportistas.totalCount} resultado(s) para la b\u00FAsqueda realizada." }
		
		model:[transportistas:transportistas]	     
    }

    def show(MaestroTransportista transportista) { model:[transportista:transportista] }

    def create() { respond new MaestroTransportista(params) }

    @Transactional
    def save(MaestroTransportista transportista) 
	{		
		if(MaestroTransportista.findByRazonSocial(transportista.razonSocial.trim()))
		{
			flash.warning = "Ya se encuentra un transportista con la razon social ingresada. Favor revisar."
			forward action:'create'
			return
		}
		
		if(MaestroTransportista.findByRutAndCliente(transportista.rut.trim(), transportista.cliente))
		{
			flash.warning = "Ya se encuentra un transportista con el rut ingresado para el cliente seleccionado. Favor revisar."
			forward action:'create'
			return
		}
		
        transportista.save flush:true, failOnError:true
		
		UsuarioPermiso.createCriteria().list
		{
			permiso { 'in'('authority', ['ROLE_SUPERADMINISTRADOR', 'ROLE_SUPERVISOR', 'ROLE_ADMINISTRADOR']) }
			
			usuario
			{
				cliente { eq('razonSocial', transportista.cliente.razonSocial) }
				eq('accountLocked', false)
			}
		}
		.usuario.each { usuario -> new RepTransportista(usuario:usuario, transportista:transportista).save flush:true, failOnError:true }
		
        flash.message = "El transportista " + transportista.descripcion + " ha sido creado exitosamente."
        redirect transportista
    }
	
    def edit(MaestroTransportista maestroTransportistaInstance) { respond maestroTransportistaInstance }
	
    @Transactional
    def update() 
	{
		def transportista = MaestroTransportista.get(params.transportista)
		
		if(MaestroTransportista.findByRazonSocialAndIdNotEqual(params.razonSocial.trim(), transportista.id))
		{
			flash.warning = "Ya se encuentra un transportista con la razon social ingresada. Favor revisar."
			forward action:'edit', params:[id:transportista.id]
			return
		}
		
		if(MaestroTransportista.findByRutAndIdNotEqual(params.rut.trim(), transportista.id))
		{
			flash.warning = "Ya se encuentra un transportista con el rut ingresado. Favor revisar."
			forward action:'edit', params:[id:transportista.id]
			return
		}
		
		transportista.rut = params.rut
		transportista.razonSocial = params.razonSocial
		transportista.descripcion = params.descripcion
		transportista.cliente = MaestroCliente.get(params.cliente)
		
        transportista.save flush:true, failOnError:true
		
        flash.message = "El transportista " + transportista.descripcion + " ha sido actualizado exitosamente."
        redirect transportista
    }
	
	@Transactional
	def delete(MaestroTransportista transportista)
	{
		transportista.activo = !transportista.activo
		
		if(transportista.activo) { flash.message = "El Transportista se ha activado exitosamente." } else { flash.message = "El Transportista se ha desactivado exitosamente." }
		
		redirect transportista
	} 
	
	def descargarExcel()
	{
		response.setContentType('application/vnd.ms-excel')
		response.setHeader('Content-Disposition', 'Attachment;Filename=' + 'Transportistas_Sin_Rut.xls')
		WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream)
		
		WritableSheet sheet1 = workbook.createSheet("Transportistas_Sin_Rut", 0)
		
		sheet1.setColumnView(0, 20)
		sheet1.setColumnView(1, 10)
		
		sheet1.addCell(new Label(0, 0, "Razon Social"))
		sheet1.addCell(new Label(1, 0, "Rut"))
		
		MaestroTransportista.createCriteria().list
		{
			cliente { eq("razonSocial", "Copec") }
			ilike("rut", "%null%")
			eq("activo", true)
		}
		.eachWithIndex
		{
			registro, i ->
			
			sheet1.addCell(new Label(0, i + 2, registro.razonSocial))
			sheet1.addCell(new Label(1, i + 2, registro.rut))
		}
		
		workbook.write()
		workbook.close()
	}
}