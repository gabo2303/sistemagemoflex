package Maestros

import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TransportistaPlantaController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
		params.max = 10
		
		def planta = MaestroPlanta.get(params.planta)
		def transportista = MaestroTransportista.get(params.transportista)
		
		def transportistasPlantas = TransportistaPlanta.createCriteria().list(params)
		{
			if(planta) { eq('planta', planta) }
			if(transportista) { eq('transportista', transportista) }
		}
		
		if(params.planta == '') { params.planta = null }
		
		if(!transportistasPlantas) { flash.warning = "No se han encontrado resultados con la b\u00FAsqueda realizada." }
		
		model:[transportistasPlantas:transportistasPlantas, params:params]		
    }
	
    def show(TransportistaPlanta asociacion) { model:[asociacion:asociacion] }

    def create() { model:[asociacion:new TransportistaPlanta(params)] }

    @Transactional
    def save(TransportistaPlanta asociacion) 
	{        
		if(TransportistaPlanta.findByTransportistaAndPlanta(asociacion.transportista, asociacion.planta))
		{
			flash.warning = "La asociaci\u00F3n que se intenta ingresar ya se encuentra registrada en el Sistema. Favor revisar."
			forward action:'create'
			return
		}
		
        asociacion.save flush:true, failOnError:true

        flash.message = "La asociaci\u00F3n se ha ingresado exitosamente."
        redirect asociacion
    }

    def edit(TransportistaPlanta transportistaPlantaInstance) { respond transportistaPlantaInstance }

    @Transactional
    def update() 
	{
		def asociacion = TransportistaPlanta.get(params.asociacion)
		def transportista = MaestroTransportista.get(params.transportista)
		def planta = MaestroPlanta.get(params.planta)		
		
        if(TransportistaPlanta.findByTransportistaAndPlantaAndIdNotEqual(transportista, planta, asociacion.id))
		{
			flash.warning = "La asociaci\u00F3n que se intenta ingresar ya se encuentra registrada en el Sistema. Favor revisar."
			forward action:'edit'
			return
		}
		
		asociacion.planta = planta
		asociacion.transportista = transportista
		
        asociacion.save flush:true, failOnError:true

        flash.message = "La asociacion se ha actualizado exitosamente."
        redirect asociacion
    }

    @Transactional
    def delete(TransportistaPlanta asociacion) 
	{
        asociacion.delete flush:true

        flash.message = "La asociacion se ha eliminado exitosamente."
        redirect action:"index", method:"GET"
    }	
}