package Maestros

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ImageController 
{
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def create() 
	{
		def imagenes = Image.findAllByIdCabecera(InspeccionCabecera.get(params.cabecera))
		
		model:[imagenes:imagenes, image:new Image(params), folio:params.cabecera]			
	}

	@Transactional
	def save(Image imagen) 
	{	
		imagen.save flush:true, failOnError:true
		
		flash.message = "La imagen ha sido creada exitosamente."			
		redirect action:'create', params:[cabecera:imagen.idCabecera.id]
	}

	@Transactional
	def delete(Image imageInstance) 
	{
		imageInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [message(code: 'Image.label', default: 'Image'), imageInstance.id])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}
	
	//Metodo que permite mostrar la imagen guardada en la base de datos en un gsp
	def showImage = 
	{
		def imagen = Image.get(params.id )
		response.outputStream << imagen.foto
		response.outputStream.flush()
	}
		
}