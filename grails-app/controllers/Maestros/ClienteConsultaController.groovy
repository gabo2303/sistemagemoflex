package Maestros

import org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import seguridad.Usuario

class ClienteConsultaController 
{
	def consultasService
	def springSecurityService
	
    def index() 
	{	
		params.max = 30
		
		def usuario = Usuario.findByUsername(springSecurityService.currentUser)
		def tipoInspeccion = MaestroTipoInspeccion.get(params.tipoInspeccion)
		def inspeccionesCliente = [] 
		def transportistas
		def transportista = MaestroTransportista.get(params.transportista)
		
		if(params.cliente || 'ROLE_INSPECTOR' in usuario.permisos.permiso.authority) { transportistas = MaestroTransportista.findAllByCliente(usuario.cliente) } 
		
		else { transportistas = RepTransportista.findAllByUsuario(usuario).transportista }
		
		usuario.cliente.tipoInspeccion.each { inspeccionesCliente.add(it.id as Integer) }
		
		def inspecciones 
		
//		if(usuario.permisos.permiso.find { it.authority == 'ROLE_INSPECTOR' })
//		{
//			inspecciones = InspeccionCabecera.createCriteria().list(params)
//			{
//				if(params.fechaInicial && params.fechaFinal) { between('fechaInspeccion', params.fechaInicial, params.fechaFinal) }
//				if(params.numero) { eq('numero', params.numero) } 
//				if(params.planta) { ilike('planta', '%' + params.planta + '%') }
//				if(transportista) { eq('transportista', transportista.descripcion) }
//				if(tipoInspeccion) { eq('idInspeccion', tipoInspeccion.id as Integer) } else { 'in'('idInspeccion', inspeccionesCliente) }
//				
//				eq('usuario', springSecurityService.currentUser.toString())
//				eq('estado', 'APROBADO')
//				
//				order 'id', 'desc'
//			}
//		}
//		else
//		{
			inspecciones = InspeccionCabecera.createCriteria().list(params)
			{
				if(params.fechaInicial && params.fechaFinal) { between('fechaInspeccion', params.fechaInicial, params.fechaFinal) }
				if(params.numero) { eq('numero', params.numero) } 
				if(params.planta) { ilike('planta', '%' + params.planta + '%') }
				if(transportista) { eq('transportista', transportista.descripcion) } else { 'in'('transportista', transportistas.descripcion) }
				if(tipoInspeccion) { eq('idInspeccion', tipoInspeccion.id as Integer) } else { 'in'('idInspeccion', inspeccionesCliente) }
					
				eq('estado', 'APROBADO')
				
				order 'id', 'desc'
			}
//		}		 
			
		if(inspecciones) { flash.message = "Se ha(n) encontrado ${inspecciones.totalCount} resultado(s) para la b\u00FAsqueda realizada." } 
		
		else { flash.warning = "No se han encontrado resultados para la b\u00FAsqueda realizada." } 
		
		model:[inspecciones:inspecciones, usuario:usuario, transportistas:transportistas]			
	}
	
	def ultimaInspeccion()
	{		
		params.max = 50
		
		def usuario = Usuario.findByUsername(springSecurityService.currentUser)
					
		def inspeccionesCliente = []
		
		usuario.cliente.tipoInspeccion.each { inspeccionesCliente.add(it.id as Integer) }
		
		def plantas
		def transportistas
		
		if(params.cliente) 
		{
			plantas = MaestroPlanta.findAllByCliente(usuario.cliente)
			transportistas = MaestroTransportista.findAllByCliente(usuario.cliente)
		} 
		else 
		{			
			plantas = UsuarioPlanta.findAllByUsuario(usuario).planta
			transportistas = RepTransportista.findAllByUsuario(usuario).transportista
		}
		
		def tipoInspeccion = MaestroTipoInspeccion.get(params.tipoInspeccion)
		def transportista = MaestroTransportista.get(params.transportista)		
		def planta = MaestroPlanta.get(params.planta)
				
		def inspecciones = InspeccionCabecera.createCriteria().list(params)
		{			
			eq('estado', 'APROBADO')
			
			if(params.numero) { eq('numero', params.numero) } 
			if(planta) { eq('planta', planta.descripcion) } else { 'in'('planta', plantas.descripcion) }
			if(tipoInspeccion) { eq('idInspeccion', tipoInspeccion.id as Integer) } else { 'in'('idInspeccion', inspeccionesCliente) }
			if(transportista) { eq('transportista', transportista.descripcion) } else { 'in'('transportista', transportistas.descripcion) }
			
			projections 
			{ 
				groupProperty('numero') 
				max('fechaInspeccion') 
				property('planta')
				property('transportista')
				property('idInspeccion')
				property('id')
			}
		}
		
		def inspeccionesTotales = InspeccionCabecera.createCriteria().list
		{
			eq('estado', 'APROBADO')
			
			if(params.numero) { eq('numero', params.numero) } 
			if(planta) { eq('planta', planta.descripcion) } else { 'in'('planta', plantas.descripcion) }
			if(tipoInspeccion) { eq('idInspeccion', tipoInspeccion.id as Integer) } else { 'in'('idInspeccion', inspeccionesCliente) }
			if(transportista) { eq('transportista', transportista.descripcion) } else { 'in'('transportista', transportistas.descripcion) }
			
			projections 
			{ 
				groupProperty('numero') 
				max('fechaInspeccion') 
			}
		}
		.size
		
		if(inspecciones) { flash.message = "Se ha(n) encontrado ${inspeccionesTotales} resultado(s) para ls b\u00FAsqueda realizada." }
		
		else { flash.warning = "No se han encontrado resultados para la b\u00FAsqueda realizada." } 
		
		model:[inspecciones:inspecciones, inspeccionesTotales:inspeccionesTotales, usuario:usuario, plantas:plantas, transportistas:transportistas]
	}
	
	def contadorMensual()
	{
		def cliente = MaestroCliente.findById(1)
		  
		def tipoInspeccion = MaestroTipoInspeccion.createCriteria().list{ and { eq('cliente', cliente) } not { 'in'('id',[1 as Long,2 as Long]) } }.descripcion
		tipoInspeccion.add(0, "Inspecciones Tipo Liviano y Petroleo-Combustible")
				
		render view:'contadorMensual', model:[tipoInspeccion:tipoInspeccion, cliente:cliente]
	}
	
	def resultadoContadorMensual()
	{
		def transportista 
		
		if(params.transportista != '') { transportista = params.transportista } 
		
		else 
		{ 
			transportista = consultasService.arregloString(MaestroTransportista.findAllByCliente(MaestroCliente.findById(1)).descripcion)
			transportista = transportista.substring(1, transportista.length() - 1) 
		}
		
		def consulta = InspeccionCabecera.executeQuery("""select transportista, planta, count(id) from InspeccionCabecera
					   where Date_Format(fecha_inspeccion, '%m') = ${params.fecha_month.toInteger()} and 
					   Date_Format(fecha_inspeccion, '%Y') = ${params.fecha_year.toInteger()} and estado = 'APROBADO' and 
					   id_inspeccion in (1) and transportista in ('${transportista}') group by transportista, planta""")		
		
		render template:'resultadoContadorMensual', model:[consulta:consulta]
	}
}