package transaccionales

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import jxl.Workbook
import jxl.write.Label
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook
import Maestros.MaestroTipoInspeccion

@Transactional(readOnly = true)
class EnvioCorreoController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() 
	{
        def correosEnviados = EnvioCorreo.findAllByFechaEnvioBetween(params.fechaInicio, params.fechaFinal)
		
		if(correosEnviados)
		{
			String nombre = "Correos_Enviados.xls"
			response.setContentType('application/vnd.ms-excel')
			response.setHeader('Content-Disposition', 'Attachment;Filename=' + nombre)
			WritableWorkbook workbook = Workbook.createWorkbook(response.outputStream)
			
			WritableSheet sheet1 = workbook.createSheet("Hoja1", 0)
			
			sheet1.setColumnView(0, 20)
			sheet1.setColumnView(1, 20)
			sheet1.setColumnView(2, 30)
			
			sheet1.addCell(new Label(0, 0, "Fecha Envio"))
			sheet1.addCell(new Label(1, 0, "Numero Camion"))
			sheet1.addCell(new Label(2, 0, "Tipo de Inspeccion"))
					
			correosEnviados.eachWithIndex
			{
				correo, i ->
				
				def tipoIns = MaestroTipoInspeccion.get(correo.tipoInspeccion).descripcion
				
				sheet1.addCell(new Label(0, 2 + i, g.formatDate(date:correo.fechaEnvio, format:'dd-MM-yyyy')))
				sheet1.addCell(new Label(1, 2 + i, correo.numeroCamion))
				sheet1.addCell(new Label(2, 2 + i, tipoIns))				
			}
			
			workbook.write()
			workbook.close()
			
			flash.message = "Se ha(n) encontrado ${correosEnviados.size} dato(s)."
		}
		else { flash.warning = "No se han encontrado datos para los parametros seleccionados." }
    }
}