package transaccionales

import grails.converters.JSON
import groovy.sql.Sql
import grails.transaction.Transactional

class ServicioWebController 
{
	def dataSource
	
	@Transactional
    def index() 
	{	
		if(params.usuario == 'copec' && params.password == 'cop1324')
		{	
			def sql = new Sql(dataSource)
			
			def fechaInicial = new Date().clearTime()
			
			fechaInicial[Calendar.DAY_OF_MONTH] -= 1
			
			def fechaFinal = new Date().clearTime()
			
			fechaFinal[Calendar.DAY_OF_MONTH] += 1
			
			def resultados = sql.rows("""SELECT id, fecha_inspeccion, (SELECT descripcion FROM maestro_tipo_inspeccion WHERE id = id_inspeccion) as tipo_inspeccion,
									  id_inspeccion, numero, (SELECT IFNULL(rut, 'Sin Rut') FROM maestro_transportista WHERE id = transportista_objeto_id) 
									  as rut_transportista, (SELECT razon_social FROM maestro_transportista WHERE id = transportista_objeto_id) as 
									  descripcion_transportista, puntaje_total, indice_operacion, total_bueno, total_malo, total_criticos
									  FROM inspeccion_cabecera WHERE estado = 'APROBADO' AND fecha_aprobacion BETWEEN ? AND ? AND id_inspeccion IN 
									  (1, 2, 3, 4, 9, 12, 15, 16, 17) order by id desc""", [fechaInicial, fechaFinal])
						
			def resultadoFinal = []
			
			resultados.each
			{
				resultado -> 
				
				def cabeceraDetalle = [:]
				
				cabeceraDetalle['cabecera'] = resultado
				cabeceraDetalle['detalles'] = sql.rows("""SELECT id, pregunta, critico_repeticion, observacion FROM inspeccion_detalle WHERE 
													   id_cabecera_id = ? AND evaluacion_item_bm = 'Malo'""", [resultado.id])
				
				resultadoFinal.add(cabeceraDetalle)
			}
			
			sql.close()
			
			render resultadoFinal as JSON
		}
		else { render "Ingrese Usuario y Contrasena correctos." }
	}
}