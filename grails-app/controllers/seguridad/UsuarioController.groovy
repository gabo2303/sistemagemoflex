package seguridad

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import Maestros.MaestroCliente

@Transactional(readOnly = true)
class UsuarioController 
{
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
        params.max = 10
		
		def cliente = MaestroCliente.get(params.cliente)
		
		def usuarios = Usuario.createCriteria().list(params)
		{
			if(params.nombre) { ilike('nombre', '%' + params.nombre + '%') }
			if(params.apellido) { ilike('apellido', '%' + params.apellido + '%') }
			if(params.username) { ilike('username', '%' + params.username + '%') }
			if(cliente) { eq('cliente', cliente) }
			if(params.activo == 'Si') { eq('accountLocked', false) } else if(params.activo == 'No') { eq('accountLocked', true) }
		}
		
        model:[usuarios:usuarios]
    }

    def show(Usuario usuario) { model:[usuario:usuario] }

    def create() { respond new Usuario(params) }

    @Transactional
    def save(Usuario usuario) 
	{
        usuario.save flush:true, failOnError:true

        flash.message = "El Usuario se ha guardado exitosamente."
        redirect usuario
    }

    def edit(Usuario usuario) { respond usuario }

    @Transactional
    def update(Usuario usuario) 
	{
        usuario.save flush:true, failOnError:true

        flash.message = "El Usuario se ha actualizado exitosamente."
        redirect usuario
    }

    @Transactional
    def delete(Usuario usuario) 
	{
        usuario.accountLocked = !usuario.accountLocked

		if(usuario.accountLocked) { flash.message = "El usuario se ha desactivado exitosamente." } else { flash.message = "El usuario se ha activado exitosamente." }
		
        redirect usuario
    }
}