package seguridad

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UsuarioPermisoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) 
	{
        params.max = 10
        
		def asociaciones = UsuarioPermiso.createCriteria().list(params) 
		{
			if(params.usuario) { usuario { ilike('username', '%' + params.usuario + '%') } }
			if(params.permiso) { permiso { ilike('authority', '%' + params.permiso + '%') } }
		}
		
		model:[asociaciones:asociaciones]
    }

    def show(UsuarioPermiso asociacion) { model:[asociacion:asociacion] }

    def create() { model:[asociacion:new UsuarioPermiso(params)] }

    @Transactional
    def save(UsuarioPermiso asociacion) 
	{        
        asociacion.save flush:true, failOnError:true

        flash.message = "La asociacion se ha creado exitosamente."
        redirect action:"index", method:"GET"
    }

    @Transactional
    def delete(UsuarioPermiso asociacion) 
	{        
		asociacion.delete flush:true, failOnError:true

        flash.message = "La asociacion se ha eliminado exitosamente."
        redirect action:"index", method:"GET"
    }
}