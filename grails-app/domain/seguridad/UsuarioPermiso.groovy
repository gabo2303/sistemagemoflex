package seguridad

import org.apache.commons.lang.builder.HashCodeBuilder

class UsuarioPermiso implements Serializable 
{
	private static final long serialVersionUID = 1

	Usuario usuario
	Permiso permiso

	boolean equals(other) 
	{
		if (!(other instanceof UsuarioPermiso)) { return false }

		other.usuario?.id == usuario?.id &&
		other.permiso?.id == permiso?.id
	}

	int hashCode() 
	{
		def builder = new HashCodeBuilder()
		if (usuario) builder.append(usuario.id)
		if (permiso) builder.append(permiso.id)
		builder.toHashCode()
	}

	static UsuarioPermiso get(long usuarioId, long permisoId) 
	{
		UsuarioPermiso.where {
			usuario == Usuario.load(usuarioId) &&
			permiso == Permiso.load(permisoId)
		}.get()
	}

	static boolean exists(long usuarioId, long permisoId) 
	{
		UsuarioPermiso.where {
			usuario == Usuario.load(usuarioId) &&
			permiso == Permiso.load(permisoId)
		}.count() > 0
	}

	static UsuarioPermiso create(Usuario usuario, Permiso permiso, boolean flush = false) {
		def instance = new UsuarioPermiso(usuario: usuario, permiso: permiso)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(Usuario u, Permiso r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = UsuarioPermiso.where {
			usuario == Usuario.load(u.id) &&
			permiso == Permiso.load(r.id)
		}.deleteAll()

		if (flush) { UsuarioPermiso.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(Usuario u, boolean flush = false) {
		if (u == null) return

		UsuarioPermiso.where {
			usuario == Usuario.load(u.id)
		}.deleteAll()

		if (flush) { UsuarioPermiso.withSession { it.flush() } }
	}

	static void removeAll(Permiso r, boolean flush = false) {
		if (r == null) return

		UsuarioPermiso.where {
			permiso == Permiso.load(r.id)
		}.deleteAll()

		if (flush) { UsuarioPermiso.withSession { it.flush() } }
	}

	static constraints = 
	{
		permiso validator: { Permiso r, UsuarioPermiso ur ->
			if (ur.usuario == null) return
			boolean existing = false
			UsuarioPermiso.withNewSession {
				existing = UsuarioPermiso.exists(ur.usuario.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['permiso', 'usuario']
		version false
	}
}