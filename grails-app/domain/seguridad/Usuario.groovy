package seguridad

import Maestros.MaestroCliente;
import Maestros.MaestroPlanta;

class Usuario 
{
	transient springSecurityService

	String username
	String password
	boolean estado = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	
	String nombre
	String apellido	
	
	static belongsTo = [cliente:MaestroCliente]
	
	static hasMany = [permisos:UsuarioPermiso]
		
	def cliente() { cliente?.razonSocial }
	
	static transients = ['springSecurityService']
	
	static constraints = 
	{
		nombre (blank:false,size:1..150)
		apellido(blank:false,size:1..150)		
		username (blank: false, unique: true)
		password (blank: false)		
	}

	static mapping = 
	{
		password column: '`password`'
		datasource 'DEFAULT'		
	}

	Set<Permiso> getAuthorities() { UsuarioPermiso.findAllByUsuario(this).collect { it.permiso } }

	def beforeInsert() { encodePassword() }

	def beforeUpdate() { if (isDirty('password')) { encodePassword() } }

	protected void encodePassword() { password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password }
	
	String toString() { return "$username" }
}