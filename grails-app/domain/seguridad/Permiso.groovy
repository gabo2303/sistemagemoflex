package seguridad

class Permiso 
{
	String authority

	static mapping = 
	{
		cache true
		datasource 'DEFAULT'		
	}

	static hasMany = [usuariosPermiso:UsuarioPermiso]
	
	static constraints = { authority blank: false, unique: true }
	
	String toString() { return "$authority" }
}