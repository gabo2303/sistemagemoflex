package Maestros

class TransportistaPlanta {
	
	MaestroPlanta planta
	MaestroTransportista transportista
	
	def planta(){
		planta.descripcion
	}
	
	def transportista(){
		transportista.razonSocial
	}
	
    static constraints = {
		planta (nullable:false)
		transportista(nullable:false)		
    }
	
	static mapping = {
		datasource 'DEFAULT'		
	}		
}


