package Maestros

class CorreoTransportista 
{
	MaestroTipoInspeccion tipoInspeccion
	MaestroPlanta planta
	MaestroTransportista transportista
	MaestroCorreo email
	
	def tipoInspeccion() { tipoInspeccion.descripcion }
	
	def email() { email.correo }
	
	def planta() { planta.descripcion }
	
	def transportista() { transportista.razonSocial }
	
    static constraints = {  }
}