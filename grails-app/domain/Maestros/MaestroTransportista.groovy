package Maestros

class MaestroTransportista 
{		
	String rut
	String razonSocial
	String descripcion
	Boolean activo = true
	
    static belongsTo = [cliente:MaestroCliente]
	
    static constraints = 
	{
		rut nullable:true, blank:false, size:1..20
		razonSocial  nullable:false, blank:false, size:1..100
		descripcion nullable:false, blank:false
	}	
	
	def cliente() { cliente?.toString() }	
	
	String toString() { return "${razonSocial}" }
	
	static mapping = { datasource 'DEFAULT' }	
}