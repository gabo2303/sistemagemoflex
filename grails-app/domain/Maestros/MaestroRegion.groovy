package Maestros

class MaestroRegion 
{	
	String descripcion    
		
	String toString() { return "$descripcion" }
	
	static constraints = { descripcion(nullable:false, blank:false) }
	
	static mapping = { datasource 'DEFAULT' }
}