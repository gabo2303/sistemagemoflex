package Maestros

class MaestroClasificacion 
{		
	String descripcion

    static constraints = { descripcion (nullable:false, blank:false, size:1..100,unique: true) }
}