package Maestros

import seguridad.Usuario

class RepTransportista 
{	
	Usuario usuario
	MaestroTransportista transportista
	
	def usuario() { usuario?.username }
	
	def transportista() { transportista?.razonSocial }
	

    static constraints = 
	{
		usuario nullable:false
		transportista nullable:false  
    }
	
	static mapping = 
	{
		datasource 'DEFAULT'
		version false
	}
}