package Maestros

import seguridad.Usuario

class MaestroCliente 
{
	String rut
	String razonSocial
	Boolean activo = true
	
	String toString() { return "$razonSocial" }
	
	static hasMany = [user:Usuario, planta:MaestroPlanta, transportista:MaestroTransportista, tipoInspeccion:MaestroTipoInspeccion]
	
    static constraints = 
	{
		activo nullable:true, blank:false
		rut size:1..13
		razonSocial nullable:false, blank:false, size :1..150		
    }
	
	static mapping = { datasource 'DEFAULT' }	
}