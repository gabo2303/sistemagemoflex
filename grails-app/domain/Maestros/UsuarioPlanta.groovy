package Maestros
import org.apache.commons.lang.builder.HashCodeBuilder
import seguridad.Usuario

class UsuarioPlanta 
{	
	MaestroPlanta planta	
	Usuario usuario 
	
	static constraints = 
	{
		usuario nullable:false
		planta nullable:false		
    }
	
	def planta() { planta?.toString() }
	
	def usuario() { usuario?.toString() }
	
	static mapping = {	
		version false		
		datasource 'DEFAULT'		
	}
}