package Maestros

class MaestroCamion 
{
	MaestroCliente cliente
	MaestroTransportista transportista
	MaestroPlanta planta
	String patente
	String numero
	String marca
	String tipoCamion
	String capacidad
	Boolean activo = true
	
	def cliente() { cliente?.razonSocial }
		
	def transportista() { transportista?.descripcion }
	
	def planta() { planta?.descripcion }
    
	static constraints = 
	{
		cliente(nullable:false)
		planta(nullable:false)
		transportista(nullable:false)	
		patente(nullable:false,size:1..50, unique: true) 
		marca(nullable:false,size:1..50)
		numero(nullable:false,size:1..20)
		tipoCamion(nullable:false,size:1..20)
		capacidad (nullable:false,blank:true,size:1..50)
	}
	
	static mapping = { datasource 'DEFAULT' }	
}