package Maestros

class InspeccionDetalle 
{
	Integer criticoRepeticion
	Integer evaluacionItem
	Integer puntajeItemBueno
	String evaluacionItemBm	
	String pregunta
	String observacion
	
	Integer critico
	Integer bueno
	Integer malo
	
	//InspeccionCabecera idCabecera.
	static belongsTo = [idCabecera:InspeccionCabecera]
	
	def id() { idCabecera.id }
		
	def usuario() { idCabecera.usuario }
	
    static constraints = 
	{
		puntajeItemBueno(nullable:true)
		pregunta(nullable:false, blank:false, size:1..255)		
		observacion(nullable:true, blank:true, size:1..255)
    }
}