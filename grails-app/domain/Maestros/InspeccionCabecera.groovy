package Maestros

class InspeccionCabecera 
{	
	String  usuario
	String 	planta
	Integer puntajeTotal
	Double indiceOperacion
	Integer totalItemsEvaluados
	Integer totalBueno
	Integer totalMalo
	Integer totalCriticos
	Date fechaInspeccion
	Date fechaAprobacion
	String estado
	String comentario 
	String conductor
	String transportista
	String cEstanque
	String tanque
	String tracto
	String nEstacionServicio
	Integer idInspeccion
	Boolean enviado = false
	MaestroTransportista transportistaObjeto
	MaestroPlanta plantaObjeto
	
	// Datos Tracto	
	String trNumero 
	String trPatente 
	String trMarca
	
	//Datos Tanque y Camion Tanque
	String numero 
	String patente 
	String marca 
	String capacidad
	String productoCarga
	String tipoCarguio
	String succionPropia
	String cargaOtroMedio
	String fabricanteMeter
	String modeloMeter
	String serieMeter
	String ticketPrinterMeter
	
	static hasMany = [detalle:InspeccionDetalle, foto:Image]
	
	static mapping = 
	{
		detalle lazy:false
		foto lazy:false
	}
	
    static constraints = 
	{
		fechaAprobacion nullable:true
		transportistaObjeto nullable:true
		plantaObjeto nullable:true
		nEstacionServicio nullable:true
		puntajeTotal nullable:true
		indiceOperacion nullable:true
		totalItemsEvaluados nullable:true
		totalBueno nullable:true
		totalMalo nullable:true
		idInspeccion nullable:true
		cEstanque nullable:true
		tracto nullable:true
		tanque nullable:true
		trNumero nullable:true
		trPatente nullable:true
		trMarca nullable:true
		numero nullable:true
		patente nullable:true
		marca nullable:true
		capacidad nullable:true	
		totalCriticos nullable:true
		fechaInspeccion nullable:true 
		comentario nullable:true
		puntajeTotal nullable:true
		totalMalo nullable:true
		totalItemsEvaluados nullable:true
		totalCriticos nullable:true
		
		productoCarga nullable:true
		tipoCarguio nullable:true
		succionPropia nullable:true
		cargaOtroMedio nullable:true
		fabricanteMeter nullable:true
		modeloMeter nullable:true
		serieMeter nullable:true
		ticketPrinterMeter nullable:true
    }
}