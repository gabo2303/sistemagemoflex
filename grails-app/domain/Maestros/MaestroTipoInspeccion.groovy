package Maestros

class MaestroTipoInspeccion 
{	
	String descripcion
	String bloqueable = "0"
	Boolean activo = true
	
	static belongsTo = [cliente:MaestroCliente]
	
	def cliente() { cliente?.toString() }
	
    static constraints = 
	{
		descripcion(nullable:false, blank:false)
		bloqueable(nullable:false, blank:false, size:1..10)		
    }
		
	String toString() { return "$id" }
	
	static mapping = { datasource 'DEFAULT' }
}