package Maestros

class EstacionServicioRedTae 
{	
	String codigoEstacion
	MaestroPlanta localidad
	
	String toString() { return "${codigoEstacion}" }
	
	static constraints = { codigoEstacion(unique:true) }
}
