package Maestros

class CorreoCliente 
{	
	MaestroCliente cliente
	MaestroTipoInspeccion tipoInspeccion
	MaestroPlanta planta
	MaestroCorreo email
	
	def cliente() { cliente.razonSocial }
	
	def email() { email.correo }
	
	def tipoInspeccion() { tipoInspeccion.descripcion }
	
	def planta() { planta.descripcion }

    static constraints = { }
}
