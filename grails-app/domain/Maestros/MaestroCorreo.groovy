package Maestros

class MaestroCorreo 
{
	String correo
	Boolean activo = true
	
	String toString() { return "${correo}" }
	
    static constraints = 
	{ 
		correo unique: true
		activo nullable:true 
	}	
}