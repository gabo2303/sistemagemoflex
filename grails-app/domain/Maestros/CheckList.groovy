package Maestros

class CheckList 
{
	MaestroTipoInspeccion tipoInspeccion
	MaestroClasificacion clasificacion
	
	String pregunta
	Integer bueno
	Integer malo
	Integer critico
	
	def clasificacion() { clasificacion?.descripcion }
	
	def tipo() { tipoInspeccion?.descripcion }
		
    static constraints = 
	{
		tipoInspeccion (nullable:false)
		clasificacion(nullable:false)
		pregunta(nullable:false, blank:false, size:1..255)		
    }
	
	static mapping = { datasource 'DEFAULT' }
}