package Maestros

import seguridad.Usuario;

class MaestroPlanta 
{	
	MaestroRegion region	
	String descripcion
	Boolean activa = true
	String codigoCopec
	
	String toString() { return "$descripcion" }
	
	static belongsTo = [cliente: MaestroCliente]
    
	static constraints = 
	{
		codigoCopec nullable:true
		region nullable:false	
		descripcion nullable:false, blank:false
		activa nullable:true, blank:false
    }
	
	static mapping = { datasource 'DEFAULT' }
}