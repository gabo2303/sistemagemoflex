package Maestros

import spock.lang.*

@TestFor(TransportistaPlantaController)
@Mock(TransportistaPlanta)
class TransportistaPlantaControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.transportistaPlantaInstanceList
            model.transportistaPlantaInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.transportistaPlantaInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def transportistaPlanta = new TransportistaPlanta()
            transportistaPlanta.validate()
            controller.save(transportistaPlanta)

        then:"The create view is rendered again with the correct model"
            model.transportistaPlantaInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            transportistaPlanta = new TransportistaPlanta(params)

            controller.save(transportistaPlanta)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/transportistaPlanta/show/1'
            controller.flash.message != null
            TransportistaPlanta.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def transportistaPlanta = new TransportistaPlanta(params)
            controller.show(transportistaPlanta)

        then:"A model is populated containing the domain instance"
            model.transportistaPlantaInstance == transportistaPlanta
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def transportistaPlanta = new TransportistaPlanta(params)
            controller.edit(transportistaPlanta)

        then:"A model is populated containing the domain instance"
            model.transportistaPlantaInstance == transportistaPlanta
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/transportistaPlanta/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def transportistaPlanta = new TransportistaPlanta()
            transportistaPlanta.validate()
            controller.update(transportistaPlanta)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.transportistaPlantaInstance == transportistaPlanta

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            transportistaPlanta = new TransportistaPlanta(params).save(flush: true)
            controller.update(transportistaPlanta)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/transportistaPlanta/show/$transportistaPlanta.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/transportistaPlanta/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def transportistaPlanta = new TransportistaPlanta(params).save(flush: true)

        then:"It exists"
            TransportistaPlanta.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(transportistaPlanta)

        then:"The instance is deleted"
            TransportistaPlanta.count() == 0
            response.redirectedUrl == '/transportistaPlanta/index'
            flash.message != null
    }
}
