package Maestros

import spock.lang.*

@TestFor(CheckListController)
@Mock(CheckList)
class CheckListControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.checkListInstanceList
            model.checkListInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.checkListInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def checkList = new CheckList()
            checkList.validate()
            controller.save(checkList)

        then:"The create view is rendered again with the correct model"
            model.checkListInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            checkList = new CheckList(params)

            controller.save(checkList)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/checkList/show/1'
            controller.flash.message != null
            CheckList.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def checkList = new CheckList(params)
            controller.show(checkList)

        then:"A model is populated containing the domain instance"
            model.checkListInstance == checkList
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def checkList = new CheckList(params)
            controller.edit(checkList)

        then:"A model is populated containing the domain instance"
            model.checkListInstance == checkList
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/checkList/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def checkList = new CheckList()
            checkList.validate()
            controller.update(checkList)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.checkListInstance == checkList

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            checkList = new CheckList(params).save(flush: true)
            controller.update(checkList)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/checkList/show/$checkList.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/checkList/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def checkList = new CheckList(params).save(flush: true)

        then:"It exists"
            CheckList.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(checkList)

        then:"The instance is deleted"
            CheckList.count() == 0
            response.redirectedUrl == '/checkList/index'
            flash.message != null
    }
}
