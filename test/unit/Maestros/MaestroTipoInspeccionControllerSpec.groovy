package Maestros

import spock.lang.*

@TestFor(MaestroTipoInspeccionController)
@Mock(MaestroTipoInspeccion)
class MaestroTipoInspeccionControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.maestroTipoInspeccionInstanceList
            model.maestroTipoInspeccionInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.maestroTipoInspeccionInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def maestroTipoInspeccion = new MaestroTipoInspeccion()
            maestroTipoInspeccion.validate()
            controller.save(maestroTipoInspeccion)

        then:"The create view is rendered again with the correct model"
            model.maestroTipoInspeccionInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            maestroTipoInspeccion = new MaestroTipoInspeccion(params)

            controller.save(maestroTipoInspeccion)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/maestroTipoInspeccion/show/1'
            controller.flash.message != null
            MaestroTipoInspeccion.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def maestroTipoInspeccion = new MaestroTipoInspeccion(params)
            controller.show(maestroTipoInspeccion)

        then:"A model is populated containing the domain instance"
            model.maestroTipoInspeccionInstance == maestroTipoInspeccion
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def maestroTipoInspeccion = new MaestroTipoInspeccion(params)
            controller.edit(maestroTipoInspeccion)

        then:"A model is populated containing the domain instance"
            model.maestroTipoInspeccionInstance == maestroTipoInspeccion
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/maestroTipoInspeccion/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def maestroTipoInspeccion = new MaestroTipoInspeccion()
            maestroTipoInspeccion.validate()
            controller.update(maestroTipoInspeccion)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.maestroTipoInspeccionInstance == maestroTipoInspeccion

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            maestroTipoInspeccion = new MaestroTipoInspeccion(params).save(flush: true)
            controller.update(maestroTipoInspeccion)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/maestroTipoInspeccion/show/$maestroTipoInspeccion.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/maestroTipoInspeccion/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def maestroTipoInspeccion = new MaestroTipoInspeccion(params).save(flush: true)

        then:"It exists"
            MaestroTipoInspeccion.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(maestroTipoInspeccion)

        then:"The instance is deleted"
            MaestroTipoInspeccion.count() == 0
            response.redirectedUrl == '/maestroTipoInspeccion/index'
            flash.message != null
    }
}
