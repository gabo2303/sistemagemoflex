

/* Copyright 2006-2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package org.codehaus.groovy.grails.plugins.jasper

import javax.servlet.http.HttpSession
import Maestros.*
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.j2ee.servlets.ImageServlet

import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.taskdefs.SendEmail;

class JasperController
{
	JasperService jasperService
	
	@Deprecated
	def index =
	{
		def testModel = this.getProperties().containsKey('chainModel') ? chainModel : null
		addImagesURIIfHTMLReport(params, request.contextPath)
		JasperReportDef report = jasperService.buildReportDefinition(params, request.getLocale(), testModel)
		addJasperPrinterToSession(request.getSession(), report.jasperPrinter)
			
		if(params.enviado == 'false')
		{
			try
			{
				//Metodo que genera y exporta el archivo al pc del usuario
				generateResponse(report)
				//Metodo que genera el archivo en distintos formatos y permite enviarlo por correo
				generateResponse(FileUtils.writeByteArrayToFile(new File("C:/Workspace/SistemaGemoflex/web-app/reporte.xls"),
								 report.contentStream.toByteArray()))
							
			}
			catch(Exception e)
			{
				log.info("Enviando Correo")// Log que muestra que se esta realizando el envio del correo
				sendMail()//Llamada que se realiza al metodo sendMail envio de correo
				
				def file = new File("C:/Workspace/SistemaGemoflex/web-app/reporte.xls").delete()
				log.info("Correo Enviado")
				
				params.enviado = 'true'
				
				flash.message = "Correo enviado."
				
				if(params.enviado == 'false')
				{
					flash.warning = "Error en el envio del correo."
				
					chain(controller: 'inspeccion', action:'show', params:[tipoInspeccion:params.tipoInspeccion, id:params.cabecera as Integer,
																		   enviado:false])
				}
			}
		}
		else
		{
			generateResponse(report)
		}
	}

	/**
	 * Generate a html response.
	 */
	def generateResponse = {reportDef ->
		if (!reportDef.fileFormat.inline && !reportDef.parameters._inline) {
			response.setHeader("Content-disposition", "attachment; filename=\""+ (reportDef.parameters._name ?: reportDef.name) +  "\"." + reportDef.fileFormat.extension)
			response.contentType = reportDef.fileFormat.mimeTyp
			response.characterEncoding = "UTF-8"
			response.outputStream << reportDef.contentStream.toByteArray()
			response.outputStream.close()
		} else {
			render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8')
		}
	}

	private void addJasperPrinterToSession(HttpSession session, JasperPrint jasperPrinter) {
		session[ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE] = jasperPrinter
	}

	private void addImagesURIIfHTMLReport(Map parameters, String contextPath) {
		if (JasperExportFormat.HTML_FORMAT == JasperExportFormat.determineFileFormat(parameters._format)) {
			parameters.IMAGES_URI = "${contextPath}/reports/image?image="
		}
	}
}