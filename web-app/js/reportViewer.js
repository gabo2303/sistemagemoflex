//Función que muestra los radio buttons y habilita los input text para poder realizar modificaciones

function mostrarTd(numeroInspeccion)
{
	document.getElementById('bueno').style.display = 'table-cell';
	document.getElementById('malo').style.display = 'table-cell';
	document.getElementById('btnGuardar').style.display = 'table-cell';	
	document.getElementById('Comentarios').disabled = false;

	if(numeroInspeccion == '6')
	{
		document.getElementById('pCarga').disabled = false;
		document.getElementById('tCarguio').disabled = false;
		document.getElementById('sPropia').disabled = false;
		document.getElementById('cOMedio').disabled = false;
		document.getElementById('fMeter').disabled = false;
		document.getElementById('mMeter').disabled = false;
		document.getElementById('sMeter').disabled = false;
		document.getElementById('tPrinterMeter').disabled = false;
	}

	document.getElementById('chofer').disabled = false;
	
	var bueno = document.getElementsByClassName('bueno'), i;
	var malo = document.getElementsByClassName('malo'), i;
	var observacion = document.getElementsByClassName('observa'), i;
	var eliminar = document.getElementsByClassName('eliminar'), i;

	for (var i = 0; i < bueno.length; i ++) 
	{
	   	bueno[i].style.display = 'table-cell';
	   	malo[i].style.display = 'table-cell';
	   	observacion[i].disabled = false;
	}	

	for (var i = 0; i < eliminar.length; i ++) 
	{
	   	eliminar[i].style.display = 'table-cell';	
	}	
}

//Función que permite mostrar en rojo las filas cuando el ítem es crítico y esta marcado como malo

function cambioColor(critico, buma, pregunta, ev, evM, ob, b, m, idTotal, vaS)
{	
	var criticoV = document.getElementById(critico).value;
	var bumaV = document.getElementById(buma).value;

	if (bumaV == "Malo" || bumaV == "M")
	{		
		if(criticoV == 1)
		{	
			document.getElementById(pregunta).style.background = 'red';
			document.getElementById(pregunta).style.color = 'white';
			document.getElementById(ev).style.background = 'red';
			document.getElementById(evM).style.background = 'red';
			document.getElementById(ob).style.background = 'red';
			document.getElementById(b).style.background = 'red';
			document.getElementById(m).style.background = 'red';
			document.getElementById(idTotal).style.background = 'red';
			document.getElementById(idTotal).style.color = 'white';
			document.getElementById(vaS).style.background = 'red';
			document.getElementById(vaS).style.color = 'white';
		}
	}
}

//Función que establece los valores de la última tabla al hacer click en los radio buttons cuando se esté en modo edición del informe 

function vervalor(valor, idTotal, inspeccion, critico, malo, bueno, idc, cr, vs)
{
	//Se obtienen los valores de los radio buton
	var elvalor = document.getElementById(valor).value;					
	var x = document.getElementById(idTotal);
	var r = document.getElementById(cr);
	var z = document.getElementById(vs);
	var criticosFinales = document.getElementById("cantcritico").value;
	
	//Sescuencia de if que suma los críticos y marca en rojo los críticos bloqueables
	if (elvalor == malo && critico == 1 && document.getElementById(valor).checked)
	{
		x.setAttribute("value",elvalor);							
		x.style.backgroundColor = 'red';
		x.style.color = 'white';							
		z.setAttribute("value",'Malo');
		criticosFinales++;						
		document.getElementById(idc).disabled = false;
		document.getElementById(valor).disabled = true; 
	}
	else if(elvalor == bueno && critico == 1 && document.getElementById(valor).checked)
	{							
		x.setAttribute("value", elvalor);							
		x.style.backgroundColor = 'white';
		x.style.color = 'black';
		z.setAttribute("value",'Bueno');
		criticosFinales--;													
		document.getElementById(idc).disabled = false;
		document.getElementById(valor).disabled = true;							
	}
	else if(elvalor == bueno && critico == 0 && document.getElementById(valor).checked)
	{					
		r.setAttribute("value",0);
		z.setAttribute("value",'Bueno');	
		x.setAttribute("value",elvalor);
		x.style.fontWeight = 'normal';
		x.style.backgroundColor = 'white';			
	}
	else if(elvalor == malo && critico == 0 && document.getElementById(valor).checked)
	{						
		r.setAttribute("value",1);		
		z.setAttribute("value",'Malo');					
		x.setAttribute("value",elvalor);
		x.style.fontWeight = 'normal';
		x.style.backgroundColor = 'white';			
	}

	//Función que suma todos los valores del input text
	importe_total = 0
	$(".valor").each(
			function(index, value) 
			{
				importe_total = importe_total + eval($(this).val());
														
			});		
	
	//Se establece la cantidad de criticos
	$("#cantcritico").val(criticosFinales);

	//Muestra el valor de la suma total de los items					
	$("#valorTotal").val(importe_total);

	// Cadena de if que sirve para asignar las fórmulas a cada inspección
	if(inspeccion == 1)
	{								
		$("#IO").val((0.017622*$("#valorTotal").val()).toFixed(2));						
	}
	else if (inspeccion == 2)
	{
		$("#IO").val((0.021742*$("#valorTotal").val()).toFixed(2));	 
	}
	else if (inspeccion == 3)
	{
		$("#IO").val(((100-($("#valorTotal").val()-52)/2.37)).toFixed(2));	
	}
	else if (inspeccion == 4)
	{
		$("#IO").val(((100-($("#valorTotal").val()-78)/4.3)).toFixed(2));	
	}
	else if (inspeccion == 5)
	{								
		$("#IO").val(((1.55039*0.01)*$("#valorTotal").val()).toFixed(2));																
	}
	else if (inspeccion == 6)
	{								
		 $("#IO").val(((100-($("#valorTotal").val()-58)/2.92)).toFixed(2));	
	}
	else if (inspeccion == 7)
	{
		$("#IO").val(0);								
	}
	else if (inspeccion == 8)
	{
		$("#IO").val(0);									
	}
	else if (inspeccion == 9)
	{
		$("#IO").val(((100-($("#valorTotal").val()-37)/1.58)).toFixed(2)); 	
	}
	else if (inspeccion == 12)
	{
		$("#IO").val(((100-($("#valorTotal").val()-54)/2.85)).toFixed(2)); 	
	}

	//Función que permite contar la cantidad de buenos y malos
	$(document).ready(function()
	{
		$('input:radio').click(function()
		{
			var b = $('.bien:checked').length
			var m = $('.mal:checked').length
			
			$("#totalBueno").val(b);
			$("#totalMalo").val(m);
		})
	});				
}


//Función que permite mostrar el detalle del meter 

function mostrarInspeccion()
{
	var x = document.getElementById('numeroInspeccion').value
				
	if(x == "6")
	{	
		$('#pieTae').show();						
	}
	else
	{					
		$('#pieTae').hide();
	}
				
	x.value;
}
